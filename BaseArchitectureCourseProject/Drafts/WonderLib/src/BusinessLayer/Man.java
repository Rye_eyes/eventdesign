/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

import DataAccessLayer.EventMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Inga
 */


/*Данный класс инкапсулирует общие сведения о человеке в системе (клиенте 
или сотруднике (менеджер, администратор). Каждый из людей имеет список 
ассоциированных событий (для клиента - список заказанных событий
у разных сотрудников, для сотрудника - список утверждённых им событий
для разных клиентов). Реализуется не как абстрактный класс в связи с 
удобством применения на уровне DataAccessLayer/ Database паттерна Single
Inheritance Table для хранения информации. Загрузка информации о списке
событий, как не требующаяся по умолчанию, реализуется с применением паттерна
отложенной инициализации Lazy Load. Для ORM-отображения используется
паттерн Foreign Mapping с обратной ссылкой.*/

public class Man extends DomainObject 
{
    private String Surname;
    private String Name;
    private String Patronym;
    private ArrayList<Event> Events;

    
    public Man(Integer Id, String Surname, String Name, String Patronym)
    {
        super(Id);
        
        this.Surname=Surname;
        this.Name=Name;
        this.Patronym=Patronym;
        this.Events=null;
    };
    
    public String getSurname()
    {
        return this.Surname;
    };
    
    public String getName()
    {
        return this.Name;
    };
    
    public String getPatronym()
    {
        return this.Patronym;
    };
    
    public void setSurname(String Surname)
    {
        this.Surname=Surname;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
    
    public void setPatronym(String Patronym)
    {
        this.Patronym=Patronym;
    };
    
    public  ArrayList<Event> getEvents() throws SQLException, ClassNotFoundException
    {
        if (this.Events==null)
        {
            this.Events=new ArrayList<>();
            
            EventMapper EM=EventMapper.getInstance();
            this.Events=EM.getEventsForMan(this);
        };
        
        return this.Events;
    };
    
}
