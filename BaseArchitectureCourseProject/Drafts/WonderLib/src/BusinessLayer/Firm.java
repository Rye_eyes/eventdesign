/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

import DataAccessLayer.RentMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует информацию о фирмах-владельцах рент.*/

public class Firm extends DomainObject
{
    private String Name;
    private ArrayList<Rent> Rents=null;
    
    public Firm(Integer Id, String Name)
    {
        super(Id);
        
        this.Name=Name;
    };
    
    public String getName()
    {
        return this.Name;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
 
    public ArrayList<Rent> getRents() throws SQLException, ClassNotFoundException
    {
        if (this.Rents==null)
        {
            this.Rents=new ArrayList<>();
            
            RentMapper RM=RentMapper.getInstance();
            this.Rents=RM.getRentsForFirm(this);
        };
        
        return this.Rents;
    };
    
}
