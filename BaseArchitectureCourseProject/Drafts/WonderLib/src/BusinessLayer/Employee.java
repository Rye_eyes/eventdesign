/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует сведения о конкретном сотруднике в системе. К общим
сведениям о человеке дополняется роль в системе.*/

public class Employee extends Man
{
    public enum Appointment
    {
        Administrator,
        Manager
    };
    
    private Appointment Rank;
    
    public Employee(Integer Id, String Surname, String Name, String Patronym, Appointment Rank)
    {
        super(Id, Surname, Name, Patronym);
        
        this.Rank=Rank;
    };
    
    public Appointment getRank()
    {
        return this.Rank;
    };
    
    public void setRank(Appointment Rank)
    {
        this.Rank=Rank;
    };
}
