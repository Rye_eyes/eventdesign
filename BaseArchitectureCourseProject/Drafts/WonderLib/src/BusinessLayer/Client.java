/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

/**
 *
 * @author Inga
 */


/*Данный класс инкапсулирует сведения об конкретном обслуженном клиенте 
в системе.*/

public class Client extends Man
{
    public Client(Integer Id, String Surname, String Name, String Patronym)
    {
        super (Id, Surname, Name, Patronym);
    };
    
}
