/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует информацию о времени начала и конца мероприятия. 
Паттерн Object Value (в инфраструктурном слое соответствует паттерну
Embedded Value для таблиц аренд и мероприятий).*/

public class Time 
{
    private final Date BeginT;
    private final Date EndT;
    
    public Time(Date BeginT, Date EndT)
    {
       if (!((BeginT==null)||(EndT==null))) 
       {
           if (BeginT.after(EndT)) throw new IllegalArgumentException();
       }
       
       this.BeginT=BeginT;
       this.EndT=EndT;
    };
    
    @Override
    public boolean equals(Object Other)
    {
        return((Other instanceof Time)&&equals((Time)Other));
    };
    
    public boolean equals(Time Other)
    {
        return((this.BeginT.equals(Other.BeginT))&&(this.EndT.equals(Other.EndT)));
    };

    @Override
    public int hashCode() 
    {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.BeginT);
        hash = 23 * hash + Objects.hashCode(this.EndT);
        return hash;
    };
    
    public Date getBeginT()
    {
        if (this.BeginT==null) return null;
        return new Date(this.BeginT.getTime());
    };
    
    public Date getEndT()
    {
        if (this.EndT==null) return null;
        return new Date(this.EndT.getTime());
    };
    
    public Long getRoundedHourPeriod()
    {
        Long Result=((this.EndT.getTime()-this.BeginT.getTime())/(1000*60*60));
        
        if (getMinuteIntFactoredPeriod()>0) Result++;
        
        return Result;
    }
    
    public Long getHourIntFactoredPeriod()
    {
        return ((this.EndT.getTime()-this.BeginT.getTime())/(1000*60*60));
    };
    
    public Long getMinuteIntFactoredPeriod()
    {
       return (((this.EndT.getTime()-this.BeginT.getTime())/(1000*60))-(this.getHourIntFactoredPeriod()*60)); 
    };
    
    public boolean isCrossedTime(Time T)
    {
        int  EndToBeginComparison, BeginToEndComparison;
        
        BeginToEndComparison= this.BeginT.compareTo(T.EndT);
        EndToBeginComparison=this.EndT.compareTo(T.BeginT);
        
        
        if ((BeginToEndComparison>=0)||(EndToBeginComparison<=0))
            return false;
        else
            return true;
    };
    
    public boolean includes(Time T)
    {
        boolean result=false;
        
        if (((this.getBeginT().compareTo(T.getBeginT())<=0)&&(this.getEndT().compareTo(T.getEndT())>=0))
            ||((this.getBeginT()==null)&&(this.getEndT().compareTo(T.getEndT())>=0))
                    ||((this.getBeginT().compareTo(T.getBeginT())<=0)&&(this.getEndT()==null)))
        result=true;
        
        return result;
    };
}
