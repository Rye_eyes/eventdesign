/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

import DataAccessLayer.EventMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует общую информацию о ренте.
Расширяется классами Placement (место проведения мероприятия) и Facility
(техническое устройство). Каждая рента имеет список ассоциированных событий, 
каждое событие - список ассоциированных рент (реализуется связь многие ко 
многим). Реализуется не как абстрактный класс в связи с 
удобством применения на уровне DataAccessLayer/ Database паттерна Single
Inheritance Table для хранения информации. Загрузка информации о списке
событий, как не требующаяся по умолчанию, реализуется с применением паттерна
отложенной инициализации Lazy Load.*/

public class Rent extends DomainObject 
{
    private Money PriceOf;
    private Firm Owner;
    private ArrayList<Event> Events=null;
    
    public Rent(Integer Id, Money PriceOf, Firm Owner)
    {
        super(Id);
        
        this.PriceOf=PriceOf;
        this.Owner=Owner;
    };
    
    public Money getPriceOf()
    {
      return this.PriceOf;  
    };
    
    public void setPrice(Money PriceOf)
    {
        this.PriceOf=PriceOf;
    };
    
    public Firm getOwner()
    {
        return this.Owner;
    };
    
    public void setOwner(Firm Owner)
    {
        this.Owner=Owner;
    };
    
    public ArrayList<Event> getEvents() throws SQLException, ClassNotFoundException
    {
        if (this.Events==null)
        {
            this.Events=new ArrayList<>();
            
            EventMapper EM=EventMapper.getInstance();
            this.Events=EM.getEventsForRent(this);
        };
        
        return this.Events;
    };

    public Boolean isFree(Time T) throws SQLException, ClassNotFoundException
    {
        if (T==null) throw new NullPointerException();
        
        for (Event e: this.getEvents())
        {
            if (e.getShedule().isCrossedTime(T)) return false;
        };
        
        return true;
    };
    
    public Money getCost(Time T)
    {
        return new Money(this.PriceOf.getAmount()*T.getRoundedHourPeriod());
    };
    
}
