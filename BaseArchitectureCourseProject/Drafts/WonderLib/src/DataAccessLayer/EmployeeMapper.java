/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Man;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует специфическую логику загрузки для экземпляра
класса работника (по отношению к классу человека). Каждый Mapper реализуется
с применением паттерна Singleton.*/

public class EmployeeMapper extends ManMapper
{
    private static EmployeeMapper Instance=null;
    
    private EmployeeMapper() throws SQLException
    {
        super();
    };
    
    public static EmployeeMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new EmployeeMapper();
        };

            return Instance;
    };

    public Employee getUserByLogin(String Username, String Password) throws SQLException, ClassNotFoundException
    {
        ArrayList<DomainObject> Result=null;
        
        Result=this.DomainObjectsFind("SELECT * FROM MANSIT WHERE Username='"+Username+"' AND Password='"+Password+"'");
        
        if (!Result.isEmpty()) return (Employee)Result.get(0);
        else
            return null;
    };
    
    public Employee getEmployeeByEventId(Event Wonder) throws SQLException, ClassNotFoundException
    {
        String Ask="SELECT * FROM MANSIT INNER JOIN ManEventAT ON MANSIT.PK"
                + "=ManEventAT.PK_ManSIT WHERE ManEventAT.PK_Event=";
        
        ArrayList<DomainObject> Result=null;
        
        
        Result=this.DomainObjectsFind(Ask+Wonder.getId().toString());
        
        if (!Result.isEmpty()) return (Employee)Result.get(0);
        else
            return null;
        
    };

    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        
        Employee Result=null;
        
        if (rs.getInt("Classcode")!=1) return null;


        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Man Base=(Man)super.load(rs);

            Employee.Appointment App;
            String appChar= rs.getString("Appointment");

            switch (appChar)
            {
                case "a":
                        App=Employee.Appointment.Administrator;
                        break;
                case "m":
                        App=Employee.Appointment.Manager;
                        break;

                default:
                        throw new IllegalStateException();
            }

            Result=new Employee(Base.getId(),Base.getSurname(), Base.getName(), Base.getPatronym(),App);
        }
        else
            Result=(Employee) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };
 
     @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException
    {
        Employee SubjectIns=(Employee)Subject;
        
        super.doInsert(Subject, insertStatement);
        
        String Appointment="";
        
        switch(SubjectIns.getRank())
        {
            case Manager:
            {
                Appointment="Менеджер";
                break;
            }
            case Administrator:
            {
                Appointment="Администратор";                 
                break;
            }
            default:
            {
                throw new NullPointerException();
            }          
        };

        insertStatement.setString(6, Appointment);
        insertStatement.setString(7, null);     // TODO (хранение в явном виде в ОП?)
        insertStatement.setString(8, null);   
    };
}
