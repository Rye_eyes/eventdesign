/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.DomainObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует информацию о первичных ключах.
Реализуется паттерн уникального простого первичного ключа, 
что позволяет использовать паттерн общей коллекции объектов.
Данный класс реализуется не как синглтон в связи с тем, что
обладает параметризуемым конструктором (параметр - величина
кэша выборки ключей из соответствующей таблицы БД). В случае
развития системы это позволит производить за раз выборку 
нескольких ключей и уменьшить число обращений к БД.*/

public class KeyGenerator // Неиспользуемый AbstractMapper. TODO: Отделение общего абстр. класса (иначе Stack Overflow)
{
    protected static String Host = "jdbc:derby://localhost:1527/Wonders";
    protected static String Name = "Inga";
    protected static String Pass= "4059042";
    
    protected Connection con;
    protected Statement stmt;
    
    private Integer nextId;
    private Integer maxId;
    private Integer incrementBy;
    
    public KeyGenerator(Integer incrementBy) throws SQLException
    {
      this.con = DriverManager.getConnection(this.Host, this.Name, this.Pass);
      this.stmt = con.createStatement();
        
      this.nextId=0;
      this.maxId=0;
      this.incrementBy=incrementBy;
    };
    
    public Integer nextKey() throws SQLException, ClassNotFoundException
    {
      if (this.nextId==this.maxId)
          reserveIds();
      
      return this.nextId++;
    };
    
    private void reserveIds() throws SQLException, ClassNotFoundException
    {
        ArrayList<DomainObject> Result=DomainObjectsFind("SELECT NextId FROM KEYS FOR UPDATE");
       
        this.nextId=Result.get(0).getId();
        this.maxId=nextId+incrementBy;
        
        stmt.executeUpdate("UPDATE Keys SET NextId="+this.maxId);
        con.commit();
    };
    
    public ArrayList<DomainObject> DomainObjectsFind(String Ask) throws SQLException, ClassNotFoundException
    {
        ArrayList<DomainObject> Result=new ArrayList<>();
        
        ResultSet rs=stmt.executeQuery(Ask);
        
        Result=loadMany(rs);
        
        return Result;
    };
    
    protected ArrayList<DomainObject> loadMany(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        List<DomainObject> Result=new ArrayList<>();
        
        if (!rs.isClosed())
        {
       
            while (rs.next())
            {
                if (load(rs)!=null)
                Result.add(load(rs));
            };
        }
        
        return (ArrayList<DomainObject>) Result;
    };
    
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException 
    {
        return new DomainObject(rs.getInt("NextId"));
    };

}
