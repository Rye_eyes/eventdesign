/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/


package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Man;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует логику доступа к общей информации о людях
в системе (пользователях двух типов и клиентах). Данная информация в базе
данных организована с применением паттерна Single Inheritance Table (таблица
MANSIT).
За тип класса строки отвечает поле ClassCode. В случае, если Classcode==1,
это - служащий, иначе (Classcode==2) - клиент.
Для отображения связи человек-список событий (многие-ко-многим) используется
паттерн ORM Association Table (ManEventAT).*/

public abstract class ManMapper extends AbstractMapper
{    
    protected ManMapper() throws SQLException
    {
        super();
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        Man Result=null;
        
        Result=new Man(rs.getInt("PK"), rs.getString("Surname"), rs.getString("Name"), rs.getString("Patronym"));
        
        return (DomainObject)Result;
    };
    
    @Override
    protected String insertStatement()
    {
        return "INSERT INTO ManSIT (PK, ClassCode, Surname, Name, Patronym, Appointment,"
                + "Username, Password) VALUES (?,?,?,?,?,?,?,?)";
    };

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException
    {
        Man SubjectIns=(Man)Subject;
        
        Integer ClassCode;
        
        if (SubjectIns instanceof Client)
        {
            ClassCode=2;
        }
        else
            if (SubjectIns instanceof Employee)
            {
                ClassCode=1;
            }
            else
            {
                throw new NullPointerException();
            };
        
        insertStatement.setInt(2, ClassCode);
        insertStatement.setString(3, SubjectIns.getSurname());
        insertStatement.setString(4, SubjectIns.getName());
        insertStatement.setString(5, SubjectIns.getPatronym());
        
        
        ArrayList<DomainObject> ManEventATInsertList=new ArrayList<>();
        String insertATStatement="INSERT INTO ManEventAT (PK_Event, PK_ManSIT)"
                + " VALUES (?,?)";
        
        
        ArrayList<Event> ManEvents=new ArrayList<>();
        ManEvents=SubjectIns.getEvents();
        
        EventMapper EM= EventMapper.getInstance();
        
        for (Event ev: ManEvents)
        {
            ManEventATInsertList.clear();
            EM.Insert(ev);
            ManEventATInsertList.add(ev);
            ManEventATInsertList.add(SubjectIns);
            InsertAT(ManEventATInsertList,insertATStatement);
        };
    };
    
}
