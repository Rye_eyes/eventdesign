/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/


package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Firm;
import BusinessLayer.Man;
import BusinessLayer.Money;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует логику доступа к общей информации о рентах
в системе (технических устройствах и доступных местах
проведения мероприятий). Данная информация в базе
данных организована с применением паттерна Single Inheritance Table (таблица
RentSIT).
За тип класса строки отвечает поле ClassCode. В случае, если Classcode==1,
это - место, иначе (Classcode==2) - техника.
Для отображения связи событие-список рент (многие-ко-многим) используется
паттерн ORM Association Table (EventRentAT).
Реализует для иерархии рент часть паттерна Inheritance Mappers, отвечающую
за операции над конкретными рентами (может использоваться только конечными
преобразователями: FacilityMapper и PlacementMapper).*/

public abstract class AbstractRentMapper extends AbstractMapper
{
    protected AbstractRentMapper() throws SQLException
    {
        super();
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        Rent Result=null;
        
        Result=new Rent(rs.getInt("PK"), new Money(rs.getLong("Price")), null);
        
        Firm Owner;
        
        FirmMapper FM=FirmMapper.getInstance();
        Owner=FM.getFirmForRent(Result);              // TODO: if null?

        Result.setOwner(Owner);
        
        return (DomainObject)Result;
    };
    
     
    @Override
    protected String insertStatement()
    {
        return "INSERT INTO RentSIT (PK, FK_Firm, Price, ClassCode, Address,"
                + "Capacity, Type) VALUES (?,?,?,?,?,?,?,?)";
    };

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException
    {
        Rent SubjectIns=(Rent)Subject;
        
        Integer ClassCode;
        
        if (SubjectIns instanceof Facility)
        {
            ClassCode=2;
        }
        else
            if (SubjectIns instanceof Placement)
            {
                ClassCode=1;
            }
            else
            {
                throw new NullPointerException();
            };
        
        
        if (SubjectIns.getOwner().getId()==null)
        {
            FirmMapper FM=FirmMapper.getInstance();
            FM.Insert(SubjectIns.getOwner());
        };
            
            
        insertStatement.setInt(2, SubjectIns.getOwner().getId());
        
        insertStatement.setInt(3, SubjectIns.getPriceOf().getAmount().intValue());
        insertStatement.setInt(4, ClassCode);   
        
        ArrayList<DomainObject> EventRentATInsertList=new ArrayList<>();
        String insertATStatement="INSERT INTO EventRentAT (FK_RentSIT, FK_Event)"
                + " VALUES (?,?)";
        
        
        ArrayList<Event> RentEvents=new ArrayList<>();
        RentEvents=SubjectIns.getEvents();
        
        EventMapper EM= EventMapper.getInstance();
        
        for (Event ev: RentEvents)
        {
            EventRentATInsertList.clear();
            EventRentATInsertList.add(SubjectIns);
            EM.Insert(ev);
            EventRentATInsertList.add(ev);
            InsertAT(EventRentATInsertList,insertATStatement);
        };
        
    };
    
}
