/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Man;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует специфическую логику загрузки для экземпляра
класса клиента (по отношению к классу человека). Каждый Mapper реализуется
с применением паттерна Singleton.*/

public class ClientMapper extends ManMapper
{
    private static ClientMapper Instance=null;
    
    private ClientMapper() throws SQLException
    {
        super();
    };
    
    public static ClientMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new ClientMapper();
        };

            return Instance;
    };
    
    public Client getClientByEventId(Event Wonder) throws SQLException, ClassNotFoundException
    {
        String Ask="SELECT * FROM MANSIT INNER JOIN ManEventAT ON MANSIT.PK"
                + "=ManEventAT.PK_Mansit WHERE ManEventAT.PK_Event=";
        
        ArrayList<DomainObject> Result=null;
        
        
        Result=this.DomainObjectsFind(Ask+Wonder.getId().toString());
        
        if (!Result.isEmpty()) return (Client)Result.get(0);
        else
            return null;
        
    };
    
    public Client getClientById(Integer Id) throws SQLException, ClassNotFoundException
    {
        ArrayList<DomainObject> Result=DomainObjectsFindById("SELECT * FROM MANSIT "
                + "WHERE PK=", Id);
        
        Client Cust=null;
        
        if (!Result.isEmpty())
            Cust=(Client) Result.get(0);
        
        return Cust;
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        
        Client Result=null;
        
        if (rs.getInt("Classcode")!=2) return null;


        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Man Base=(Man)super.load(rs);
            Result=new Client(Base.getId(),Base.getSurname(), Base.getName(), Base.getPatronym());
        }
        else
            Result=(Client) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };
    
     @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException
    {
        Client SubjectIns=(Client)Subject;
        
        super.doInsert(Subject, insertStatement);
        
        insertStatement.setString(6, null);
        insertStatement.setString(7, null);
        insertStatement.setString(8, null);   
    };

}
