package externalsevice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class ExternalSevice {


    public static void main(String[] args) throws IOException, ParseException 
    {
        System.out.println("Введите команду:");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
        String Command = "";
        String Username="";
        String Password="";
        
        Command=in.readLine();
        
        String encodedCommand="";
        String encodedResult="";
        
        switch (Command)
        {
            case "login":
            {
                System.out.println("Введите логин:");
                Username=in.readLine();
                System.out.println("Введите пароль:");
                Password=in.readLine();
                encodedCommand=makeLoginAsk(Username, Password);
                
                System.out.println("Закодированная команда= "+encodedCommand);
 
                break;
            }
            case "getClients":
            {
                encodedCommand=makegetClientsAsk();
         
                System.out.println("Закодированная команда= "+encodedCommand);
 
                break;
            }
            default:
            {
                System.out.println("Команда не найдена");
                return;
            }    
        };
        
        encodedResult=parseAsk(encodedCommand);
        System.out.println("Закодированный результат= "+encodedResult);
 
    }

    private static String makeLoginAsk(String Username, String Password) 
    {
         JSONObject obj=new JSONObject();
         JSONObject arg=new JSONObject();
         
         arg.put("name", Username);
         arg.put("password", Password);
         
         obj.put("command","login");
         obj.put("arg",arg);
         
         return obj.toJSONString();
    }

    private static String makegetClientsAsk() 
    {
        JSONObject obj=new JSONObject();
        obj.put("command","getClients");
         
        return obj.toJSONString();
    }
    
    private static String parseAsk(String Ask) throws ParseException
    {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject)jsonParser.parse(Ask);
        
        String result="";
        
        switch((String)jsonObject.get("command"))
        {
            case "login":
            {
                String Name=jsonObject.get("arg").toString();
                String Password=jsonObject.get("arg").toString();
                
                // Dump обработки
                boolean switcher=true;
                
                result=makeLoginResult(switcher);
                
                parseLoginResult(result);
                break;
            }
            case "getClients":
            {
                // Dump обработки
                ArrayList<Client> Clients=new ArrayList<>();
                Client C1=new Client("A", "A", "A");
                Client C2=new Client("B", "B", "B");
                Client C3=new Client("C", "C", "C");
                
                Clients.add(C1);
                Clients.add(C2);
                Clients.add(C3);
                
                
                result=makegetClientsResult(Clients);
                parsegetClientsResult(result);
                break;
            }
            
        }
        
        return result;
    };

    private static String makeLoginResult(boolean switcher) 
    {
         JSONObject obj=new JSONObject();
         
         obj.put("result",switcher);
         
         return obj.toJSONString();
    }

    private static String makegetClientsResult(ArrayList<Client> Clients) 
    {
        
        JSONObject mainobj=new JSONObject();
         
        
         JSONArray list = new JSONArray();
         
         for (Client t:Clients)
         {
            JSONObject obj=new JSONObject();
         
            obj.put("surname", t.Surname);
            obj.put("name", t.Name);
            obj.put("patronym", t.Patronym);
            
            list.add(obj);
         }
         
        mainobj.put("clients", list);
         
         return mainobj.toJSONString();
         
    };

    private static boolean parseLoginResult(String result) throws ParseException 
    {
         JSONParser jsonParser = new JSONParser();
         JSONObject jsonObject = (JSONObject)jsonParser.parse(result);
         
         boolean res=(boolean)jsonObject.get("result");
        
         System.out.println("login="+res);
         return  res;
    };

    private static ArrayList<Client> parsegetClientsResult(String result) throws ParseException 
    {
        ArrayList<Client> resL=new ArrayList<>();
         JSONParser jsonParser = new JSONParser();
         JSONObject jsonObject = (JSONObject)jsonParser.parse(result);
         
         JSONArray nestedArr=(JSONArray)jsonObject.get("clients");
         
         for (int i = 0; i<nestedArr.size(); i++)
         {
             resL.add(new Client(((JSONObject)(nestedArr.get(i))).get("surname").toString(), 
                     ((JSONObject)(nestedArr.get(i))).get("name").toString(), 
                     ((JSONObject)(nestedArr.get(i))).get("patronym").toString()));
             
             System.out.println("Resl="+resL.get(i).Surname);
         }
        
         return  resL;
    };
    
}
