/*Реализация слоя предстваления.Окно общего списка событий.*/

package Frames;

import BusinessLayer.Event;
import BusinessLayer.Time;
import ServiceLayer.BussinessAPI;
import java.awt.Color;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Inga
 */
public class WondersRegistryFrame extends JFrameWithAddFunc{

    /**
     * Creates new form WondersRegistryFrame
     */
    
    // Выделенное мероприятие
   private static Event selectedWonderRow=null;
    
    
    
    // Функция отрисовки таблицы списка мероприятий по заданным значениям из
    // базы данных
    private void WondersTablePainter(final ArrayList<Event> EventList)
    {
        DefaultTableModel WondersTModel=(DefaultTableModel) this.WondersTable.getModel();
        
        TableClearer(this.WondersTable);
        
        TableParametrizer(this.WondersTable, ListSelectionModel.SINGLE_SELECTION, 
                new  ListSelectionListener()
                {
                    @Override
                    public void valueChanged(ListSelectionEvent e) 
                    {
                         if (WondersTable.getSelectedRow()!=-1)
                             selectedWonderRow=EventList.get(WondersTable.getSelectedRow());
                         else
                             selectedWonderRow=null;
                    };
                });
        
        SimpleDateFormat dateFormat=new SimpleDateFormat();
        
        int i=1;
        for(Event ev: EventList)
        {
                       
            Object [] Tmp;
            
            Tmp=new Object[6];
            
            Tmp[0]=i;
            Tmp[1]=dateFormat.format(new Date(ev.getShedule().getBeginT().getTime()));
            Tmp[2]=dateFormat.format(new Date(ev.getShedule().getEndT().getTime()));
            
            String Type=null;
            
            switch (ev.getType())
            {
                case banquet:
                {
                    Type="Банкет";
                    break;
                }
                case presentation:
                {
                    Type="Презентация";
                    break;
                }
                case party:
                {
                    Type="Вечеринка";
                    break;
                }
                case show:
                {
                    Type="Шоу";
                    break;
                }
                default:
                {
                    try 
                    {
                        throw new ClassNotFoundException();
                    } 
                    catch (ClassNotFoundException ex) 
                    {
                         JOptionPane.showMessageDialog(null, "Ошибка работы с базой данных.");  // TODO: return-dispose?
                        return;
                    }
                }
            };
            Tmp[3]=Type;
            Tmp[4]=ev.getCustomer().getSurname()+" "+ev.getCustomer().getName().charAt(0)+". "+ev.getCustomer().getPatronym().charAt(0)+".";
            Tmp[5]=ev.getCreator().getSurname()+" "+ev.getCreator().getName().charAt(0)+". "+ev.getCreator().getPatronym().charAt(0)+".";
            
            if (i<=WondersTModel.getRowCount())
            {
                for (int k=0; k<WondersTModel.getColumnCount(); k++)
                    WondersTModel.setValueAt(Tmp[k], i-1, k);
            }
            else
                WondersTModel.addRow(Tmp);
            i++;
        };
    };
    
    
    // Таблица инициализируется списком уже утверждённых событий, доступных
    // для просмотра заданному пользователю. Администратору доступны все события,
    // менеджеру - созданные им. TODO: ограничения время/размер
    
    public WondersRegistryFrame(String TitleAdd, JFrameWithAddFunc previousFrame)
    {
        super(TitleAdd,previousFrame);
        initComponents();
       
       
        BussinessAPI BA=BussinessAPI.getInstance();
        
        ArrayList<Event> EventList=null;
         
        try 
        {
            EventList=BA.getEventsList();
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "Ошибка работы с базой данных.");
            return;
        } 
        catch (ClassNotFoundException ex) 
        {
            JOptionPane.showMessageDialog(null, "Внутренняя ошибка программы.");
            return;
        };
        
        WondersTablePainter(EventList);
       
    };

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        WondersTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        logoutButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        filterStartField = new javax.swing.JTextField();
        filterEndField = new javax.swing.JTextField();
        filterClientIdField = new javax.swing.JTextField();
        filterEventsButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        getEventButton = new javax.swing.JButton();
        removeEventButton = new javax.swing.JButton();
        addEventButton = new javax.swing.JButton();

        jRadioButtonMenuItem1.setSelected(true);
        jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        WondersTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        WondersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "№", "Начало", "Конец", "Тип", "Клиент", "Менеджер"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        WondersTable.setGridColor(new java.awt.Color(0, 0, 0));
        WondersTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(WondersTable);

        jLabel1.setText("Список утверждённых мероприятий");

        logoutButton.setText("Выйти");
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });

        jLabel3.setText("Фильтры");

        filterEndField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterEndFieldActionPerformed(evt);
            }
        });

        filterEventsButton.setText("Фильтровать");
        filterEventsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterEventsButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Начало:");

        jLabel4.setText("Конец:");

        jLabel5.setText("Id клиента:");

        getEventButton.setText("Просмотреть");
        getEventButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getEventButtonActionPerformed(evt);
            }
        });

        removeEventButton.setText("Удалить");
        removeEventButton.setPreferredSize(new java.awt.Dimension(101, 23));

        addEventButton.setText("Добавить");
        addEventButton.setPreferredSize(new java.awt.Dimension(101, 23));
        addEventButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addEventButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(294, 294, 294)
                        .addComponent(logoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(getEventButton)
                        .addGap(51, 51, 51)
                        .addComponent(removeEventButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58)
                        .addComponent(addEventButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 647, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(filterEventsButton)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(filterStartField)
                        .addComponent(filterEndField)
                        .addComponent(filterClientIdField, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(241, 241, 241)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(41, 41, 41))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(filterStartField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(filterEndField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(filterClientIdField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(20, 20, 20)
                        .addComponent(filterEventsButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(getEventButton)
                    .addComponent(removeEventButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addEventButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                .addComponent(logoutButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Нажатие кнопки "Выйти"
    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        LogoutAndStartAgain();
        
    }//GEN-LAST:event_logoutButtonActionPerformed

    private void filterEndFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterEndFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filterEndFieldActionPerformed

    // Нажатие кнопки "Фильтровать"
    private void filterEventsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterEventsButtonActionPerformed
        // TODO add your handling code here:
        BussinessAPI BA=BussinessAPI.getInstance();
        
        SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
        
        Date Begin=null;
        Date End=null;
        
        
        if (!"".equals(filterStartField.getText()))
        {
            
            try 
            {
                Begin=Formatter.parse(filterStartField.getText());
            } 
            catch (ParseException ex) 
            {
                JOptionPane.showMessageDialog(null, "Некорректное значение фильтра (начало).");
                return;
            }
        };
        
        if (!"".equals(filterEndField.getText()))
        {
            
            try 
            {
                End=Formatter.parse(filterEndField.getText());
            } 
            catch (ParseException ex) 
            {
                JOptionPane.showMessageDialog(null, "Некорректное значение фильтра (конец).");
                return;
            }
        };
        
        Integer ClientId=null;
        
        if (!"".equals(filterClientIdField.getText()))
        {
            try
            {
                ClientId=Integer.parseInt(filterClientIdField.getText());
            }
            catch (NumberFormatException e)
            {
                JOptionPane.showMessageDialog(null, "Некорректное значение фильтра (Id клиента).");
                return;
            };
        };
       
        
        ArrayList<Event> Result=null;
        
        try 
        {
            Result = BA.getFilteredEventsList(Begin, End, ClientId);
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "Ошибка работы с базой данных.");
            return;
        } 
        catch (ClassNotFoundException ex) 
        {
            JOptionPane.showMessageDialog(null, "Внутренняя ошибка программы.");
            return;
        };
        
        WondersTablePainter(Result);
            
    }//GEN-LAST:event_filterEventsButtonActionPerformed

    // Нажатие кнопки "Просмотреть". Текущая форма закрывается, вызывается
    // форма с отображением информации о выбранном (selection) событии
    // Передача события? Сохранение состояния фильтрации для последующего 
    // возврата?
    private void getEventButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getEventButtonActionPerformed
      
       try 
       {
           if (selectedWonderRow!=null)
           {
             this.dispose();  
             new WonderCardFrame("Карта мероприятия.",this, selectedWonderRow).setVisible(true);
           }
           else
             JOptionPane.showMessageDialog(null, "Выберите мероприятие.");  
       }
       catch (ClassNotFoundException e)
       {
           
           JOptionPane.showMessageDialog(null, "Внутренняя ошибка программы.");
           return;  
       }
       catch (SQLException e)
       {
           JOptionPane.showMessageDialog(null, "Ошибка работы с базой данных.");
           return;
       };
    }//GEN-LAST:event_getEventButtonActionPerformed

    // Нажатие кнопки "Добавить" (событие)
    private void addEventButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addEventButtonActionPerformed
        try 
       {
           
            selectedWonderRow=null;
            this.dispose();  
            new WonderCardFrame("Карта мероприятия.",this, selectedWonderRow).setVisible(true);
           
       }
       catch (ClassNotFoundException e)
       {
           
           JOptionPane.showMessageDialog(null, "Внутренняя ошибка программы.");
           return;  
       }
       catch (SQLException e)
       {
           JOptionPane.showMessageDialog(null, "Ошибка работы с базой данных.");
           return;
       };
    }//GEN-LAST:event_addEventButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable WondersTable;
    private javax.swing.JButton addEventButton;
    private javax.swing.JTextField filterClientIdField;
    private javax.swing.JTextField filterEndField;
    private javax.swing.JButton filterEventsButton;
    private javax.swing.JTextField filterStartField;
    private javax.swing.JButton getEventButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton logoutButton;
    private javax.swing.JButton removeEventButton;
    // End of variables declaration//GEN-END:variables
}
