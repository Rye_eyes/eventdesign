/*Реализация слоя предстваления.Реестр разделяемых данных.*/

package Frames;

import ServiceLayer.BussinessAPI;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует служебные данные и функции,разделяемые между всеми 
фреймами приложения. Фреймы будут наследоваться именно от него*/

public abstract class JFrameWithAddFunc extends JFrame
{
    private static String Title="Дизайнер чудес. ";
    private static String IconPath="E:\\logoWonder.jpg";
    private JFrameWithAddFunc previousFrame=null;
    private static JFrameWithAddFunc startFrame=null;
    
    public JFrameWithAddFunc(String TitleAdd, JFrameWithAddFunc previousFrame)
    {
        super();
        setFrIcon();
        setFrTitle(TitleAdd);
        this.previousFrame=previousFrame;
    };
    
    private void setFrIcon()
    {
        ImageIcon img = new ImageIcon(IconPath);
        this.setIconImage(img.getImage());
    };
    
    private void setFrTitle(String TitleAdd)
    {
        this.setTitle(Title+TitleAdd);
    };
    
    protected void TableParametrizer(JTable Table, int SelectionModel, ListSelectionListener x)
    {
        Table.setSelectionMode(SelectionModel);
        
        Table.getSelectionModel().addListSelectionListener(x);
        
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        Table.setDefaultRenderer(Object.class, centerRenderer);
        Table.getTableHeader().setDefaultRenderer(centerRenderer);
        Table.setGridColor(Color.BLACK);
        Table.setShowGrid(true);
         
    };
    
    // Функция очищения строк таблицы
    protected void TableClearer(JTable Table)
    {
        DefaultTableModel TModel=(DefaultTableModel)Table.getModel();
        
        for (int i=0; i< TModel.getRowCount(); i++)
        {
            for (int k=0; k<TModel.getColumnCount(); k++)
                 TModel.setValueAt(null, i, k); 
        };
    };
    
    // Функция возврата к предыдущему фрейму
    protected void goToPreviousFrame()
    {
        if (this.previousFrame==null) throw new NullPointerException();
        
        this.dispose();
        this.previousFrame.setVisible(true);
    };
    
    // Функция возврата к начальному фрейму
    private void goToStartFrame()
    {
        if (startFrame==null) throw new NullPointerException();
        
        this.dispose();
        startFrame.setVisible(true);
    };
    
    // Функция возврата к начальному фрейму с логаутом (реально используемая, 
    // конкретная)
    protected void LogoutAndStartAgain()
    {
        BussinessAPI BA=BussinessAPI.getInstance();
        BA.Logout();
        goToStartFrame();
    };
    
    
    // Функция установки начального фрейма
    protected void setStartFrame(JFrameWithAddFunc startFrameN)
    {
        startFrame=startFrameN;  
        System.out.println("Name="+this.getName());
    };
}
