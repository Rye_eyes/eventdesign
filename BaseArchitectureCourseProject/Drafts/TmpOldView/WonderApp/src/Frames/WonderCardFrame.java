/*Реализация слоя предстваления.Окно регистрационной карточки мероприятия.*/



package Frames;

import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import ServiceLayer.BussinessAPI;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Inga
 */
public class WonderCardFrame extends JFrameWithAddFunc {

    private Event eWonder=null;
    
    
    
    // Функция отрисовки таблицы списка рент мероприятия
    private void EventRentsTablePainter() throws SQLException, ClassNotFoundException
    {
        DefaultTableModel RentTModel=(DefaultTableModel) this.EventRentsTable.getModel();
        
        TableClearer(this.EventRentsTable);
        
        TableParametrizer(this.EventRentsTable, ListSelectionModel.SINGLE_SELECTION, 
                null);
        
        int i=1;
        for(Rent r: eWonder.getRents())
        {
                       
            Object [] Tmp;
            
            Tmp=new Object[5];
            
            Tmp[0]=i;
            Tmp[1]=r.getId();
            
            String Type="";
            
            if (r instanceof Placement)
            {
                Type="Размещение";
            }
            else
                if (r instanceof Facility)
                {
                    switch(((Facility)r).getType())
                    {
                        case Audiosystem:
                        {
                            Type="Аудиосистема";
                            break;
                        }
                        case Microphone:
                        {
                            Type="Микрофон";
                            break;
                        }
                        case Videosystem:
                        {
                            Type="Видеосистема";
                            break;
                        }
                    }
                    
                }
                else
                {
                    throw new ClassNotFoundException();
                };
            Tmp[2]=Type;
            Tmp[3]=r.getOwner().getName();
            Tmp[4]=r.getPriceOf().getIntFactoredAmount(100);
            
            if (i<=RentTModel.getRowCount())
            {
                for (int k=0; k<RentTModel.getColumnCount(); k++)
                    RentTModel.setValueAt(Tmp[k], i-1, k);
            }
            else
                RentTModel.addRow(Tmp);
            i++;
        };
    };
    
    
    /*Функция установки активности в зависимости от режима: просмотр/создание
    события*/
    
    private void setActive(boolean isActive)
    {
            ClientIdField.setEditable(isActive);
            SurnameField.setEditable(isActive);
            NameField.setEditable(isActive);
            PatronymField.setEditable(isActive);
            EventTypeCombo.setEnabled(isActive);
            BeginTimeField.setEditable(isActive);
            EndTimeField.setEditable(isActive);
            EventCostField.setEditable(isActive);
            saveEventButton.setEnabled(isActive);
    };
    
    
    /**
     * Creates new form WonderCardFrame
     */
    //TODO: обработчик для ClassNotFound
    public WonderCardFrame(String TitleAdd, JFrameWithAddFunc previousFrame, Event Wonder) throws ClassNotFoundException, SQLException 
    {
        super(TitleAdd,previousFrame);
        eWonder=Wonder;
        initComponents();
        
        // Если редакция, то заполняем значения полей для переданного события
        if (eWonder!=null)
        {
            setActive(false);
            ClientIdField.setText(eWonder.getCustomer().getId().toString());
            SurnameField.setText(eWonder.getCustomer().getSurname().toString());
            NameField.setText(eWonder.getCustomer().getName().toString());
            PatronymField.setText(eWonder.getCustomer().getPatronym().toString());
            
            SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
            
            BeginTimeField.setText(Formatter.format(eWonder.getShedule().getBeginT()));
            EndTimeField.setText(Formatter.format(eWonder.getShedule().getEndT()));
            
            String Combovalue=null;
            
            // TODO: рефакторинг в класс события (конвертер, деконвертер)
            switch (eWonder.getType())
            {
                case banquet:
                {
                    Combovalue="Банкет";
                    break;
                }
                case presentation:
                {
                    Combovalue="Презентация";
                    break;
                }
                case party:
                {
                    Combovalue="Вечеринка";
                    break;
                }
                case show:
                {
                    Combovalue="Шоу";
                    break;
                }
                default:
                {
                    throw new ClassNotFoundException();
                }
                    
            }
            EventTypeCombo.setSelectedItem(Combovalue);
            
            EventRentsTablePainter();
            EventCostField.setText(Wonder.getCost().getIntFactoredAmount(100).toString());
            
            editOrViewRentsButton.setText("Просмотреть");
        }
        else
        {
            setActive(true);
            editOrViewRentsButton.setText("Редактировать");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        ClientIdField = new javax.swing.JTextField();
        SurnameField = new javax.swing.JTextField();
        NameField = new javax.swing.JTextField();
        PatronymField = new javax.swing.JTextField();
        BeginTimeField = new javax.swing.JTextField();
        EndTimeField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        EventRentsTable = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        toWondersButton = new javax.swing.JButton();
        logoutButton = new javax.swing.JButton();
        EventTypeCombo = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        EventCostField = new javax.swing.JTextField();
        editOrViewRentsButton = new javax.swing.JButton();
        saveEventButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(900, 450));
        setResizable(false);

        jLabel1.setText("Регистрационная карточка мероприятия");

        jLabel2.setText("Id клиента:");

        jLabel3.setText("Фамилия:");

        jLabel4.setText("Имя:");

        jLabel5.setText("Отчество:");

        jLabel6.setText("Тип мероприятия:");

        jLabel7.setText("Дата начала:");

        jLabel8.setText("Дата конца:");

        NameField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NameFieldActionPerformed(evt);
            }
        });

        EndTimeField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EndTimeFieldActionPerformed(evt);
            }
        });

        EventRentsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "№", "Id", "Тип", "Фирма", "Цена "
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(EventRentsTable);
        if (EventRentsTable.getColumnModel().getColumnCount() > 0) {
            EventRentsTable.getColumnModel().getColumn(0).setResizable(false);
            EventRentsTable.getColumnModel().getColumn(1).setResizable(false);
            EventRentsTable.getColumnModel().getColumn(2).setResizable(false);
            EventRentsTable.getColumnModel().getColumn(3).setResizable(false);
            EventRentsTable.getColumnModel().getColumn(4).setResizable(false);
        }

        jLabel10.setText("Аренда:");

        toWondersButton.setText("К списку ");
        toWondersButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toWondersButtonActionPerformed(evt);
            }
        });

        logoutButton.setText("Выйти");
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });

        EventTypeCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Банкет", "Презентация", "Вечеринка", "Шоу" }));

        jLabel11.setText("Стоимость:");

        EventCostField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EventCostFieldActionPerformed(evt);
            }
        });

        editOrViewRentsButton.setText("Редактировать");
        editOrViewRentsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editOrViewRentsButtonActionPerformed(evt);
            }
        });

        saveEventButton.setText("Сохранить");
        saveEventButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveEventButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(52, 52, 52)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SurnameField)
                            .addComponent(NameField)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ClientIdField, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(PatronymField)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BeginTimeField)
                            .addComponent(EventTypeCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(EndTimeField, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 145, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(EventCostField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(77, 77, 77))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(editOrViewRentsButton)
                        .addGap(175, 175, 175))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(saveEventButton)
                        .addGap(186, 186, 186))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(toWondersButton)
                        .addGap(18, 18, 18)
                        .addComponent(logoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(142, 142, 142))))
            .addGroup(layout.createSequentialGroup()
                .addGap(242, 242, 242)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel1)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(ClientIdField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(SurnameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(NameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(PatronymField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(EventTypeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(BeginTimeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(EventCostField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(EndTimeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(saveEventButton))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(toWondersButton)
                            .addComponent(logoutButton)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(editOrViewRentsButton)))
                .addContainerGap(91, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void NameFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NameFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NameFieldActionPerformed

    private void EndTimeFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EndTimeFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EndTimeFieldActionPerformed

    private void EventCostFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EventCostFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EventCostFieldActionPerformed

    // Нажатие на кнопку выхода
    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        LogoutAndStartAgain();
    }//GEN-LAST:event_logoutButtonActionPerformed

    // Возвращение к списку
    // TODO: сохранение состояния фильтрации(?), перенос инициализации пользователя
    // в title при переходе к списку в конструктор списка
    private void toWondersButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toWondersButtonActionPerformed
        goToPreviousFrame();
    }//GEN-LAST:event_toWondersButtonActionPerformed

    // Нажатие на кнопку "Редактировать"
    private void editOrViewRentsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editOrViewRentsButtonActionPerformed

        if (eWonder==null)
        {
          
            SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");

            Date Begin=null;
            Date End=null;


            if (!"".equals(BeginTimeField.getText()))
            {
                try 
                {
                    Begin=Formatter.parse(BeginTimeField.getText());
                } 
                catch (ParseException ex) 
                {
                    JOptionPane.showMessageDialog(null, "Некорректное значение фильтра поиска (начало).");
                    return;
                }
            };

            if (!"".equals(EndTimeField.getText()))
            {

                try 
                {
                    End=Formatter.parse(EndTimeField.getText());
                } 
                catch (ParseException ex) 
                {
                    JOptionPane.showMessageDialog(null, "Некорректное значение фильтра поиска (конец).");
                    return;
                }
            };    

            if (!((Begin==null)&&(End==null)))
            {
                this.dispose();
                new RentChoice("Редактор рент.",this, null, new Time(Begin, End)).setVisible(true);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Для выбора рент необходимо задать время проведения мероприятия.");
                return;
            };  
        }
        else
        {
                this.dispose();
                new RentChoice("Редактор рент.",this, this.eWonder, null).setVisible(true);
        };
        
    }//GEN-LAST:event_editOrViewRentsButtonActionPerformed

    private void saveEventButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveEventButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_saveEventButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField BeginTimeField;
    private javax.swing.JTextField ClientIdField;
    private javax.swing.JTextField EndTimeField;
    private javax.swing.JTextField EventCostField;
    private javax.swing.JTable EventRentsTable;
    private javax.swing.JComboBox EventTypeCombo;
    private javax.swing.JTextField NameField;
    private javax.swing.JTextField PatronymField;
    private javax.swing.JTextField SurnameField;
    private javax.swing.JButton editOrViewRentsButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton logoutButton;
    private javax.swing.JButton saveEventButton;
    private javax.swing.JButton toWondersButton;
    // End of variables declaration//GEN-END:variables
}
