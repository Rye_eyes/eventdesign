/*Реализация слоя представления.Окно выбора рент.*/


package Frames;

import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import ServiceLayer.BussinessAPI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Inga
 */

public class RentChoice extends JFrameWithAddFunc
{

    private Event eWonder=null;
    private Time Shedule;
    
    // Выделенные ренты(размещения)
    private ArrayList<Placement> Placements=new ArrayList<Placement>();
    
    // Выделенные ренты(технические устройства)
    private ArrayList<Facility> Facilities=new ArrayList<Facility>();
    
    
     // Функция отрисовки таблицы списка рент мероприятия (места проведения)
    
    private void EventPlacementTablePainter() throws SQLException, ClassNotFoundException
    {
        final BussinessAPI BA=BussinessAPI.getInstance();
        
        DefaultTableModel PlacementTModel=(DefaultTableModel) this.placementTable.getModel();
        
        TableClearer(this.placementTable);
        
        if (this.eWonder==null)
        TableParametrizer(this.placementTable, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, 
                new  ListSelectionListener()
                {
                    @Override
                    public void valueChanged(ListSelectionEvent e) 
                    {
                        Placements.clear();
                             
                        if (placementTable.getSelectedRows().length!=0)
                        {
                            
                            for (int it: placementTable.getSelectedRows())
                            {
                                try 
                                {
                                    Placements.add((BA.getFreePlacementsByShedule(Shedule)).get(it));
                                } 
                                catch (SQLException ex) 
                                {
                                    Logger.getLogger(RentChoice.class.getName()).log(Level.SEVERE, null, ex);
                                } 
                                catch (ClassNotFoundException ex) 
                                {
                                    Logger.getLogger(RentChoice.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            };
                        };
               
                    };
                });
        else
        {
             TableParametrizer(this.placementTable, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, null); 
        };
        int i=1;
        
        ArrayList<Placement> showPlacements=new ArrayList<>();
        
        if (eWonder==null)
        {
            showPlacements=BA.getFreePlacementsByShedule(Shedule);
        }
        else
        {
            showPlacements=this.eWonder.getPlacements();
        };
        
        for(Placement p: showPlacements)
        {
            
            Object [] Tmp;
            
            Tmp=new Object[6];
            
            Tmp[0]=i;
            Tmp[1]=p.getId();
            
            Tmp[2]=p.getOwner().getName();
            Tmp[3]=p.getPriceOf().getIntFactoredAmount(100);
            
            Tmp[4]=p.getAddress();
            Tmp[5]=p.getCapacity();
            
            if (i<=PlacementTModel.getRowCount())
            {
                for (int k=0; k<PlacementTModel.getColumnCount(); k++)
                    PlacementTModel.setValueAt(Tmp[k], i-1, k);
            }
            else
                PlacementTModel.addRow(Tmp);
            i++;
        };
    };
    
    
     // Функция отрисовки таблицы списка рент мероприятия (технические устройства)
    
    private void EventFacilityTablePainter() throws SQLException, ClassNotFoundException
    {
        final BussinessAPI BA=BussinessAPI.getInstance();
        
        DefaultTableModel FacilityTModel=(DefaultTableModel) this.facilityTable.getModel();
        
        TableClearer(this.facilityTable);
        
        if (this.eWonder==null)
        TableParametrizer(this.facilityTable, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, 
                new  ListSelectionListener()
                {
                    @Override
                    public void valueChanged(ListSelectionEvent e) 
                    {
                        
                        Facilities.clear();
                             
                        if (facilityTable.getSelectedRows().length!=0)
                        {
                            for (int it: facilityTable.getSelectedRows())
                            {
                                try 
                                {
                                    Facilities.add((BA.getFreeFacilitiesByShedule(Shedule)).get(it));
                                } 
                                catch (SQLException ex) 
                                {
                                    Logger.getLogger(RentChoice.class.getName()).log(Level.SEVERE, null, ex);
                                } 
                                catch (ClassNotFoundException ex) 
                                {
                                    Logger.getLogger(RentChoice.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            };
                        };
               
                    };
                });
        else
        {
             TableParametrizer(this.facilityTable, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, null);
        };
        ArrayList<Facility> showFacilities=new ArrayList<>();
        
        if (eWonder==null)
        {
            showFacilities=BA.getFreeFacilitiesByShedule(Shedule);
        }
        else
        {
            showFacilities=this.eWonder.getFacilities();
        };
        
        
        int i=1;
        for(Facility f: showFacilities)
        {
            
            Object [] Tmp;
            
            Tmp=new Object[5];
            
            Tmp[0]=i;
            Tmp[1]=f.getId();
            
            String Type="";
            
            
            switch(f.getType())
            {
                case Audiosystem:
                {
                    Type="Аудиосистема";
                    break;
                }
                case Microphone:
                {
                    Type="Микрофон";
                    break;
                }
                case Videosystem:
                {
                    Type="Видеосистема";
                    break;
                }
            }
                    
              
            Tmp[4]=Type;
            Tmp[2]=f.getOwner().getName();
            Tmp[3]=f.getPriceOf().getIntFactoredAmount(100);
            
            if (i<=FacilityTModel.getRowCount())
            {
                for (int k=0; k<FacilityTModel.getColumnCount(); k++)
                    FacilityTModel.setValueAt(Tmp[k], i-1, k);
            }
            else
                FacilityTModel.addRow(Tmp);
            i++;
        };
    };
    /**
     * Creates new form RentChoice
     */
    public RentChoice(String TitleAdd, JFrameWithAddFunc previousFrame, Event eWonder, Time Shedule) 
    {
        super(TitleAdd, previousFrame);
        this.Shedule=Shedule;
        this.eWonder=eWonder;
        initComponents();
       
        try 
        {
            EventFacilityTablePainter();
            EventPlacementTablePainter();
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(RentChoice.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) 
        {
            Logger.getLogger(RentChoice.class.getName()).log(Level.SEVERE, null, ex);
        };
        
        if (this.eWonder!=null)
        {
            saveRentsButton.setEnabled(false);
        };
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        placementTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        facilityTable = new javax.swing.JTable();
        saveRentsButton = new javax.swing.JButton();
        toWonderCardButton = new javax.swing.JButton();
        logoutButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(900, 450));
        setResizable(false);

        jLabel1.setText("Доступные арендодатели:");

        placementTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "№", "Id", "Фирма", "Цена", "Адрес", "Вместимость"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(placementTable);
        if (placementTable.getColumnModel().getColumnCount() > 0) {
            placementTable.getColumnModel().getColumn(0).setResizable(false);
            placementTable.getColumnModel().getColumn(1).setResizable(false);
            placementTable.getColumnModel().getColumn(2).setResizable(false);
            placementTable.getColumnModel().getColumn(3).setResizable(false);
        }

        facilityTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "№", "Id", "Фирма", "Цена", "Тип"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(facilityTable);
        if (facilityTable.getColumnModel().getColumnCount() > 0) {
            facilityTable.getColumnModel().getColumn(0).setResizable(false);
            facilityTable.getColumnModel().getColumn(1).setResizable(false);
            facilityTable.getColumnModel().getColumn(2).setResizable(false);
            facilityTable.getColumnModel().getColumn(3).setResizable(false);
            facilityTable.getColumnModel().getColumn(4).setResizable(false);
        }

        saveRentsButton.setText("Сохранить");
        saveRentsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveRentsButtonActionPerformed(evt);
            }
        });

        toWonderCardButton.setText("К форме");
        toWonderCardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toWonderCardButtonActionPerformed(evt);
            }
        });

        logoutButton.setText("Выйти");
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Место проведения:");

        jLabel3.setText("Технические устройства:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(310, 310, 310)
                        .addComponent(saveRentsButton)
                        .addGap(40, 40, 40)
                        .addComponent(toWonderCardButton)
                        .addGap(45, 45, 45)
                        .addComponent(logoutButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(393, 393, 393)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(422, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel1)
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toWonderCardButton)
                    .addComponent(saveRentsButton)
                    .addComponent(logoutButton))
                .addContainerGap(127, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Нажатие на кнопку "Выход".
    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        
        LogoutAndStartAgain();
        
    }//GEN-LAST:event_logoutButtonActionPerformed

    // Нажатие на кнопку "К форме". Список при этом обратно не передаётся.
    private void toWonderCardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toWonderCardButtonActionPerformed
        goToPreviousFrame();
                 
    }//GEN-LAST:event_toWonderCardButtonActionPerformed

    // Нажатие на кнопку "Сохранить".
    private void saveRentsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveRentsButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_saveRentsButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable facilityTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton logoutButton;
    private javax.swing.JTable placementTable;
    private javax.swing.JButton saveRentsButton;
    private javax.swing.JButton toWonderCardButton;
    // End of variables declaration//GEN-END:variables
}
