/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/


package DataAccessLayer;

import BusinessLayer.DomainObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует базовые операции взаимодействия с сущностями 
любого типа из базы данных. Реализует паттерны Layer Supertype, Identity Map
(универсальная коллекция).*/

public abstract class AbstractMapper 
{
    protected static String Host = "jdbc:derby://localhost:1527/Wonders";
    protected static String Name = "Inga";
    protected static String Pass= "4059042";
    
    protected Connection con;
    protected Statement stmt;
  
    protected static KeyGenerator KeyMaker;
    protected static Map<Integer,DomainObject> AllIdentitiesMap=new HashMap();
    
    protected AbstractMapper() throws SQLException
    {
        this.con = DriverManager.getConnection(this.Host, this.Name, this.Pass);
        this.stmt = con.createStatement();
        KeyMaker= new KeyGenerator(new Integer(1));
    };
    
    @SuppressWarnings("empty-statement")
    public ArrayList<DomainObject> DomainObjectsFindById(String AskForFetchById,
            Integer Id) throws SQLException, ClassNotFoundException
    {
        return DomainObjectsFind(AskForFetchById+Id.toString());
    };
    
    public ArrayList<DomainObject> DomainObjectsFind(String Ask) throws SQLException, ClassNotFoundException
    {
        ArrayList<DomainObject> Result=new ArrayList<>();
        
        ResultSet rs=stmt.executeQuery(Ask);
        
        Result=loadMany(rs);
        
        return Result;
    };
    
    protected abstract DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException;
    
    protected ArrayList<DomainObject> loadMany(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        List<DomainObject> Result=new ArrayList<>();
        
        if (!rs.isClosed())
        {
       
            while (rs.next())
            {
                if (load(rs)!=null)
                Result.add(load(rs));
            };
        }
        
        return (ArrayList<DomainObject>) Result;
    };
  
    protected Integer Insert(DomainObject Subject) throws SQLException, ClassNotFoundException
    {
        if (Subject.getId()!=null) return Subject.getId();
            
        PreparedStatement insertStatement=null;
        insertStatement=con.prepareStatement(insertStatement());
        Subject.setId(KeyMaker.nextKey());
        insertStatement.setInt(1, Subject.getId());
        doInsert(Subject, insertStatement);
        insertStatement.execute();
        AllIdentitiesMap.put(Subject.getId(), Subject);
        return Subject.getId();
    };
    
    protected void InsertAT(ArrayList<DomainObject> InsertedObjects, String insertATStatement) throws SQLException
    {
        PreparedStatement insertStatement=null;
        insertStatement=con.prepareStatement(insertATStatement);
        
        int i=1;
        
        for (DomainObject DO: InsertedObjects)
        {
            if (DO.getId()==null) throw new NullPointerException();                 // В принципе, обходится универсально/централизованно AT через Mapper Registry...
            insertStatement.setInt(i, DO.getId());
            i++;
        };
        
        insertStatement.execute();
    };

    protected abstract String insertStatement();

    protected abstract void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException;

}
