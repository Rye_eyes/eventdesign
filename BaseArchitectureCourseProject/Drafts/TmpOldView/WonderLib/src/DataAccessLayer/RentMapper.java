/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/


package DataAccessLayer;

import BusinessLayer.DomainObject;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Firm;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует логику доступа к общей информации о рентах
в системе (технических устройствах и доступных местах
проведения мероприятий). Данная информация в базе
данных организована с применением паттерна Single Inheritance Table (таблица
RentSIT).
За тип класса строки отвечает поле ClassCode. В случае, если Classcode==1,
это - место, иначе (Classcode==2) - техника.
Для отображения связи событие-список рент (многие-ко-многим) используется
паттерн ORM Association Table (EventRentAT).
Реализует для иерархии рент часть паттерна Inheritance Mappers, отвечающую
за операции над всеми рентами (делегирует
задачи конкретным преобразователям: FacilityMapper и PlacementMapper).*/

public class RentMapper 
{
    private static RentMapper Instance=null;
    private FacilityMapper FM;
    private PlacementMapper PM;
    
    private RentMapper() throws SQLException
    {
        super();
        FM=FacilityMapper.getInstance();
        PM=PlacementMapper.getInstance();
    };
    
    public static RentMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new RentMapper();
        };
        
        return Instance;
    };

    public ArrayList<Rent> getRentsForEvent(Event aEvent) throws SQLException, ClassNotFoundException 
    {
        ArrayList<Rent> Result=new ArrayList<>();
        
        ArrayList<Facility> Facilities=new ArrayList<Facility>();
        ArrayList<Placement> Places=new ArrayList<Placement>();
        
        Facilities=FM.getFacilitiesForEvent(aEvent);
        
        for (Facility tmpF: Facilities)
        {
            Result.add((Rent)tmpF);
        };
        
        Places=PM.getPlacementForEvent(aEvent);
        
        for (Placement tmpP: Places)
        {
            Result.add((Rent)tmpP);
        };
        
        return Result;
    };
    
    public ArrayList<Rent> getRentsForTime(Time Shedule) throws SQLException, ClassNotFoundException
    {
        ArrayList<Rent> Result=new ArrayList<>();
        
        ArrayList<Facility> Facilities=new ArrayList<Facility>();
        ArrayList<Placement> Places=new ArrayList<Placement>();
        
        Facilities=FM.getFacilitiesForTime(Shedule);
        
        for (Facility tmpF: Facilities)
        {
            Result.add((Rent)tmpF);
        };
        
        Places=PM.getPlacementForTime(Shedule);
        
        for (Placement tmpP: Places)
        {
            Result.add((Rent)tmpP);
        };
        
        return Result;
        
    };
    
    public ArrayList<Rent> getRentsForFirm(Firm Owner) throws SQLException, ClassNotFoundException
    {
        ArrayList<Rent> Result=new ArrayList<>();
        
        ArrayList<Facility> Facilities=new ArrayList<Facility>();
        ArrayList<Placement> Places=new ArrayList<Placement>();
        
        Facilities=FM.getFacilitiesForFirm(Owner);
        
        for (Facility tmpF: Facilities)
        {
            Result.add((Rent)tmpF);
        };
        
        Places=PM.getPlacementForFirm(Owner);
        
        for (Placement tmpP: Places)
        {
            Result.add((Rent)tmpP);
        };
        
        return Result;  
    };
    
    
    
}
