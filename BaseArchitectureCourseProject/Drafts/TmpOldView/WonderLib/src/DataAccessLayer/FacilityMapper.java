/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Facility.FacilityType;
import BusinessLayer.Firm;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует специфическую логику загрузки для экземпляра
класса арендуемого технического устройства 
(по отношению к классу ренты). Каждый Mapper реализуется
с применением паттерна Singleton.*/ 

public class FacilityMapper extends AbstractRentMapper
{
    private static FacilityMapper Instance=null;
    
    private FacilityMapper() throws SQLException
    {
        super();
    };
    
    public static FacilityMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new FacilityMapper();
        };
        
        return Instance;
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
         
        Facility Result=null;
        
        if (rs.getInt("Classcode")!=2) return null;


        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Rent Base=(Rent)super.load(rs);
            
            String FacType=rs.getString("Type");
            FacilityType Ft;
            
            switch (FacType)
            {
                case "Аудиосистема":
                {
                    Ft=FacilityType.Audiosystem;
                    break;
                }
                case "Видеосистема":
                {
                    Ft=FacilityType.Videosystem;
                    break;
                }
                case "Микрофон":
                {
                    Ft=FacilityType.Microphone;
                    break;
                }
                default:
                {
                    throw new ClassNotFoundException();
                }
            };
            
            Result=new Facility(Base.getId(),Base.getPriceOf(), Base.getOwner(),Ft);
        }
        else
            Result=(Facility) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };
    
    public ArrayList<Facility> getFacilitiesForEvent(Event Wonder) throws SQLException, ClassNotFoundException
    {
        String Ask="SELECT * FROM RENTSIT INNER JOIN EventRentAT ON "
                + "EventRentAT.FK_RentSIT=RentSIT.PK"
                + " WHERE EventRentAT.FK_Event=";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFindById(Ask, Wonder.getId());
        ArrayList<Facility> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                Result.add((Facility)DO);   
            };
        };
        
        return Result;
        
    };

    public ArrayList<Facility> getFacilitiesForTime(Time Shedule) throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM RENTSIT";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFind(Ask);
        ArrayList<Facility> Result=new ArrayList<>();
        
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                if (((Facility)DO).isFree(Shedule))
                Result.add((Facility)DO);   
            };
        };
        
        return Result;
        
    };
    
    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException
    {
        Facility SubjectIns=(Facility)Subject;
        
        super.doInsert(Subject, insertStatement);
        
        String Type=null;
            
        switch (SubjectIns.getType())
        {
            case Audiosystem:
            {
                Type="Аудиосистема";
                break;
            }
            case Videosystem:
            {
                Type="Видеосистема";
                break;
            }
            case Microphone:
            {
                Type="Микрофон";
                break;
            }
            default:
            {
                throw new ClassNotFoundException();
            }
        };

        
        insertStatement.setString(5, null);
        insertStatement.setString(6, null);
        insertStatement.setString(7, Type);   
    }; 
    
    ArrayList<Facility> getFacilitiesForFirm(Firm Owner) throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM RENTSIT WHERE FK_Firm=";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFindById(Ask, Owner.getId());
        ArrayList<Facility> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                Result.add((Facility)DO);   
            };
        };
        
        return Result;
    }
}
