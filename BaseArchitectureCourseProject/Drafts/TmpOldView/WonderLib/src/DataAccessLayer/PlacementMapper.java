/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.DomainObject;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Firm;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует специфическую логику загрузки для экземпляра
класса места проведения мероприятия
(по отношению к классу ренты). Каждый Mapper реализуется
с применением паттерна Singleton.*/ 

public class PlacementMapper extends AbstractRentMapper
{
    private static PlacementMapper Instance=null;
    
    private PlacementMapper() throws SQLException
    {   
        super();
    };
    
    public static PlacementMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new PlacementMapper();
        };
        
        return Instance;
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
         
        Placement Result=null;
        
        if (rs.getInt("Classcode")!=1) return null;

        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Rent Base=(Rent)super.load(rs);
           
            Result=new Placement(Base.getId(),Base.getPriceOf(), Base.getOwner(),rs.getString("Address"), rs.getInt("Capacity"));
        }
        else
            Result=(Placement) AllIdentitiesMap.get(rs.getInt("PK"));

        return (DomainObject)Result;
    };
    
    
    public ArrayList<Placement> getPlacementForEvent(Event Wonder) throws SQLException, ClassNotFoundException
    {
    
        String Ask="SELECT * FROM RENTSIT INNER JOIN EventRentAT ON "
                + "EventRentAT.FK_RentSIT=RentSIT.PK"
                + " WHERE EventRentAT.FK_Event=";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFindById(Ask, Wonder.getId());
        ArrayList<Placement> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                Result.add((Placement)DO);   
            };
        };
        
        return Result;
    };

    public ArrayList<Placement> getPlacementForTime(Time Shedule) throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM RENTSIT";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFind(Ask);
        ArrayList<Placement> Result=new ArrayList<>();
        
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                if (((Placement)DO).isFree(Shedule))
                Result.add((Placement)DO);   
            };
        };
        
        return Result;
    };
    
    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException
    {
        Placement SubjectIns=(Placement)Subject;
        
        super.doInsert(Subject, insertStatement);
        
        insertStatement.setString(5, SubjectIns.getAddress());
        insertStatement.setInt(6, SubjectIns.getCapacity());
        insertStatement.setString(7, null);   
    }; 
    
    ArrayList<Placement> getPlacementForFirm(Firm Owner) throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM RENTSIT WHERE FK_Firm=";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFindById(Ask, Owner.getId());
        ArrayList<Placement> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                Result.add((Placement)DO);   
            };
        };
        
        return Result;
    };
}
