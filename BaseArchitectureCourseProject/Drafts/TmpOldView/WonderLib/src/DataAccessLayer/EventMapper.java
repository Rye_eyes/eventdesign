/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Event.EventType;
import BusinessLayer.Facility;
import BusinessLayer.Man;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует логику загрузки для событий. Каждый Mapper 
реализуется с применением паттерна Singleton.*/

public class EventMapper extends AbstractMapper
{
    private static EventMapper Instance=null;

    
    private EventMapper() throws SQLException
    {
        super();
    };
    
    public static EventMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new EventMapper();
        };

            return Instance;
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException
    {
        Event Result=null;
        
         if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            EventType Type=null;
            
            switch (rs.getString("Type"))
            {
                case "banquet":
                {
                    Type=EventType.banquet;
                    break;
                }
                case "presentation":
                {
                    Type=EventType.presentation;
                    break;
                }
                case "party":
                {
                    Type=EventType.party;
                    break;
                }
                case "show":
                {
                    Type=EventType.show;
                    break;
                }
                default:
                {
                    throw new ClassNotFoundException();
                }
            };
            
            Result=new Event(rs.getInt("PK"),null,null,new Time(new Date(rs.getTimestamp("BeginT").getTime()),new Date(rs.getTimestamp("EndT").getTime())), Type);
           
        }
        else
            Result=(Event) AllIdentitiesMap.get(rs.getInt("PK"));
        
        return (DomainObject)Result;
    };

    protected String AskForFetchById() 
    {
        return ("SELECT * FROM Event INNER JOIN ManEventAT ON Event.PK="
                + "ManEventAT.PK_Event WHERE ManEventAT.PK_ManSIT=");
    };
    
    public ArrayList<Event> getEventsForMan(Man CreatorOrClient) throws SQLException, ClassNotFoundException 
    {
        ArrayList<DomainObject> DomainResult=DomainObjectsFindById(AskForFetchById(),CreatorOrClient.getId());
        ArrayList<Event> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                
                if (CreatorOrClient instanceof Employee)
                {
                     ((Event)DO).setCreator((Employee)CreatorOrClient);
                     ClientMapper CM=ClientMapper.getInstance();
                     ((Event)DO).setCustomer(CM.getClientByEventId((Event)DO));    
                }
                else
                    if (CreatorOrClient instanceof Client)
                    {
                        ((Event)DO).setCustomer((Client)CreatorOrClient);
                        EmployeeMapper EM=EmployeeMapper.getInstance();
                        ((Event)DO).setCreator(EM.getEmployeeByEventId((Event)DO));                        
                    }
                    else
                        throw new ClassNotFoundException();
 
                Result.add(((Event)DO));   
            }
        }
        
        
        return Result;
    };

    public ArrayList<Event> getAllEvents() throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM EVENT";

        ArrayList<DomainObject> DomainResult=DomainObjectsFind(Ask);
        
        ArrayList<Event> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                
                 ClientMapper CM=ClientMapper.getInstance();
                 ((Event)DO).setCustomer(CM.getClientByEventId((Event)DO));    
                
                EmployeeMapper EM=EmployeeMapper.getInstance();
                ((Event)DO).setCreator(EM.getEmployeeByEventId((Event)DO));                        
                   
                Result.add(((Event)DO));   
            }
        }
        
        
        return Result;
        
    }

    public ArrayList<Event> getEventsForRent(Rent aRent) throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM EVENT INNER JOIN EventRentAT ON Event.PK="
                + "EventRentAT.FK_Event WHERE EventRentAT.FK_RentSIT=";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFind(Ask+aRent.getId().toString());
        
        ArrayList<Event> Result=new ArrayList<>();
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
            for (DomainObject DO: DomainResult)
            {
                
                 ClientMapper CM=ClientMapper.getInstance();
                 ((Event)DO).setCustomer(CM.getClientByEventId((Event)DO));    
                
                EmployeeMapper EM=EmployeeMapper.getInstance();
                ((Event)DO).setCreator(EM.getEmployeeByEventId((Event)DO));                        
                   
 
                Result.add(((Event)DO));   
            }
        }
        return Result;
        
    }

    @Override
    protected String insertStatement() 
    {
         return "INSERT INTO Event (PK, BeginT,"
                + "EndT, Type) VALUES (?,?,?,?)";
    }

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException 
    {
        Event SubjectIns=(Event)Subject;
        
        insertStatement.setTimestamp(2, new Timestamp(SubjectIns.getShedule().getBeginT().getTime()));       
        insertStatement.setTimestamp(3, new Timestamp(SubjectIns.getShedule().getBeginT().getTime()));
        
        String Type=null;
            
        switch (SubjectIns.getType())
        {
            case banquet:
            {
                Type="Банкет";
                break;
            }
            case presentation:
            {
                Type="Презентация";
                break;
            }
            case party:
            {
                Type="Вечеринка";
                break;
            }
            case show:
            {
                Type="Шоу";
                break;
            }
            default:
            {
               
                throw new ClassNotFoundException();
               
            }
        };
        
        insertStatement.setString(4, Type);  
        
        ArrayList<DomainObject> EventRentATInsertList=new ArrayList<>();
        String insertATStatement="INSERT INTO EventRentAT (FK_RentSIT, FK_Event)"
                + " VALUES (?,?)";
        
        
        ArrayList<Rent> EventRents=new ArrayList<>();
        EventRents=SubjectIns.getRents();
        
        FacilityMapper FM=FacilityMapper.getInstance();                             // TODO: делегирование через RentMapper
        PlacementMapper PM=PlacementMapper.getInstance();
        
        for (Rent r: EventRents)
        {
            EventRentATInsertList.clear();
           
            EventRentATInsertList.add(r);
            
             if (r instanceof Facility)
            {
                FM.Insert((Facility)r);
            }
            else
                if (r instanceof Placement)
                {
                    PM.Insert((Placement)r);
                }
                else
                    throw new ClassNotFoundException();
             
            EventRentATInsertList.add(SubjectIns);
            InsertAT(EventRentATInsertList,insertATStatement);
        };
        
        ArrayList<DomainObject> EventManATInsertList=new ArrayList<>();
        insertATStatement="INSERT INTO ManEventAT (PK_Event, PK_ManSIT)"
                + " VALUES (?,?)";
        
        
        ArrayList<Man> EventMans=new ArrayList<>();
        EventMans.add(SubjectIns.getCreator());
        EventMans.add(SubjectIns.getCustomer());
        
        // TODO: делегирование через ManMapper, выделение абстрактного и классического
        
        EmployeeMapper EmM=EmployeeMapper.getInstance();
        ClientMapper CM=ClientMapper.getInstance();
        
        for (Man m: EventMans)
        {
            EventManATInsertList.clear();
            
            
            if (m instanceof Employee)
            {
                EmM.Insert((Employee)m);
            }
            else
                if (m instanceof Client)
                {
                    CM.Insert((Client)m);
                }
                else
                    throw new ClassNotFoundException();
           
            EventManATInsertList.add(SubjectIns);
            EventManATInsertList.add(m);
            
            InsertAT(EventManATInsertList,insertATStatement);
        };
    }
    
}
