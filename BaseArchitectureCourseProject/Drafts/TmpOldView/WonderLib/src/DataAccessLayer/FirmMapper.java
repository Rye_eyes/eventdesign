/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/


package DataAccessLayer;

import BusinessLayer.Client;
import BusinessLayer.DomainObject;
import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Firm;
import BusinessLayer.Man;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */


/*Данный класс инкапсулирует логику загрузки для экземпляра
класса фирмы-владельца ренты. Каждый Mapper реализуется
с применением паттерна Singleton.*/ 

public class FirmMapper extends AbstractMapper
{
    
    private static FirmMapper Instance=null;
    
    private FirmMapper() throws SQLException
    {
        super();  
    };
    
    public static FirmMapper getInstance() throws SQLException
    {
        if (Instance==null)
        {
            Instance=new FirmMapper();
        };
        
        return Instance;
    };

    
    public Firm getFirmForRent(Rent aRent) throws SQLException, ClassNotFoundException 
    {
        String Ask="SELECT * FROM FIRM INNER JOIN RentSIT ON "
                + "Firm.PK=RentSIT.FK_Firm"
                + " WHERE RentSIT.PK=";
        
        ArrayList<DomainObject> DomainResult=DomainObjectsFindById(Ask, aRent.getId());
        
        Firm Result=null;
        
        if (DomainResult.isEmpty()) return Result;
        else
        {
           Result=(Firm)DomainResult.get(0);
        };
        
        return Result;
    };

    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException 
    {
           
        Firm Result=null;
        

        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Result=new Firm(rs.getInt("PK"), rs.getString("Name"));
        }
        else
            Result=(Firm) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };

    @Override
    protected String insertStatement() 
    { 
        return "INSERT INTO Firm (PK, Name) VALUES (?,?)";
    };

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException, ClassNotFoundException 
    {
        Firm SubjectIns=(Firm)Subject;
        
        insertStatement.setString(2, SubjectIns.getName());
        
        ArrayList<Rent> FirmRents=new ArrayList<>();
        
        FirmRents=SubjectIns.getRents();
        
        FacilityMapper FM=FacilityMapper.getInstance();                             // TODO: делегирование через RentMapper
        PlacementMapper PM=PlacementMapper.getInstance();
        
        for (Rent r: FirmRents)
        {
            
            if (r instanceof Facility)
            {
                FM.Insert((Facility)r);
            }
            else
                if (r instanceof Placement)
                {
                    PM.Insert((Placement)r);
                }
                else
                    throw new ClassNotFoundException();
            
        };
    };
    
}
