/*Пакет, реализующий слой доступа к данным. Паттерн Data Mapper.*/

package DataAccessLayer;

import BusinessLayer.DomainObject;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует информацию о первичных ключах.
Реализуется паттерн уникального простого первичного ключа, 
что позволяет использовать паттерн общей коллекции объектов.
Данный класс реализуется не как синглтон в связи с тем, что
обладает параметризуемым конструктором (параметр - величина
кэша выборки ключей из соответствующей таблицы БД). В случае
развития системы это позволит производить за раз выборку 
нескольких ключей и уменьшить число обращений к БД.*/

public class KeyGenerator extends AbstractMapper
{
    private Integer nextId;
    private Integer maxId;
    private Integer incrementBy;
    
    public KeyGenerator(Integer incrementBy) throws SQLException
    {
      super();  
      this.nextId=0;
      this.maxId=0;
      this.incrementBy=incrementBy;
    };
    
    public Integer nextKey() throws SQLException, ClassNotFoundException
    {
      if (this.nextId==this.maxId)
          reserveIds();
      
      return this.nextId++;
    };
    
    private void reserveIds() throws SQLException, ClassNotFoundException
    {
        ArrayList<DomainObject> Result=DomainObjectsFind("SELECT NextId FROM KEYS FOR UPDATE");
       
        this.nextId=Result.get(0).getId();
        this.maxId=nextId+incrementBy;
        
        stmt.executeUpdate("UPDATE Keys SET NextId="+this.maxId);
        con.commit();
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException, ClassNotFoundException 
    {
        return new DomainObject(rs.getInt("NextId"));
    };

    @Override
    protected String insertStatement() 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
