/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

/**
 *
 * @author Inga
 */


/*Этот класс содержит ключ сущности и методы работы с ним.
Реализация паттерна Layer supertype. Не является абстрактным,
т.к. в проекте используется паттерн генератора ключей.*/

public class DomainObject 
{
    private Integer Id;
    
    public DomainObject(Integer Id)
    {
        this.Id=Id;
    };
    
    public Integer getId()
    {
        return this.Id;
    };
    
    public void setId(Integer Id)
    {
        if (Id==null) throw new NullPointerException();
        this.Id=Id;
    };
    
}
