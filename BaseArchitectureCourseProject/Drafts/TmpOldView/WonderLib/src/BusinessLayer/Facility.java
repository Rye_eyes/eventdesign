/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

/**
 *
 * @author Inga
 */

public class Facility extends Rent
{ 
    public enum FacilityType
    {
        Audiosystem,
        Videosystem,
        Microphone
    };
 
    private FacilityType Type;
     
    public Facility(Integer Id, Money PriceOf, Firm Owner, FacilityType Type) 
    {
        super(Id, PriceOf, Owner);
        this.Type=Type;
    };
    
    public FacilityType getType()
    {
        return this.Type;
    };
    
    public void setType(FacilityType Type)
    {
        if (Type==null) throw new NullPointerException();
        
        this.Type=Type;
        
    };
        
}
