/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

import DataAccessLayer.AbstractRentMapper;
import DataAccessLayer.EventMapper;
import DataAccessLayer.RentMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует сведения о конкретном событии в системе.
Загрузка информации о списке рент, как не требующаяся по умолчанию, 
реализуется с применением паттерна отложенной инициализации Lazy Load.*/

public class Event extends DomainObject
{
    /*Это перечисление определяет допустимые типы событий в системе.*/
    public enum EventType
    {
        banquet,
        presentation,
        party,
        show
    };
    
    private Employee Creator;
    private Client Customer;
    private Time Shedule;
    private EventType Type;
    private ArrayList<Rent> Rents=null;

    public Event(Integer Id, Employee Creator, Client Customer, Time Shedule, EventType Type) 
    {
        super(Id);
        
        this.Creator=Creator;
        this.Shedule=Shedule;
        this.Customer=Customer;
        this.Type=Type;
    };
    
    public Employee getCreator()
    {
        return this.Creator;
    };
    
    public void setCreator(Employee Creator)
    {
        this.Creator=Creator;
    };
    
    public Client getCustomer()
    {
        return this.Customer;
    };
    
    public void setCustomer(Client Customer)
    {
        this.Customer=Customer;
    };
    
    public Time getShedule()
    {
        return this.Shedule;
    };
    
    public void setShedule(Time Shedule)
    {
        this.Shedule=Shedule;
    };
    
    public EventType getType()
    {
        return this.Type;
    };
    
    public void setType(EventType Type)
    {
        this.Type=Type;
    };
    
    
    public ArrayList<Rent> getRents() throws SQLException, ClassNotFoundException
    {
        if (this.Rents==null)
        {
            this.Rents=new ArrayList<>();
            
            RentMapper RM=RentMapper.getInstance();
            this.Rents=RM.getRentsForEvent(this);
        };
        
        return this.Rents;
    };
    
    public ArrayList<Facility> getFacilities() throws SQLException, ClassNotFoundException
    {
        ArrayList<Facility> Result=new ArrayList<>();
        
        for (Rent r: getRents())
        {
            if (r instanceof Facility)
                Result.add((Facility)r);
        }
        
        return Result;
    };
    
    public ArrayList<Placement> getPlacements() throws SQLException, ClassNotFoundException
    {
        ArrayList<Placement> Result=new ArrayList<>();
        
        for (Rent r: getRents())
        {
            if (r instanceof Placement)
                Result.add((Placement)r);
        }
        
        return Result;
    };
    
    public boolean filter(Date BeginT, Date EndT, Integer ClientId)
    {
        boolean Result=true;
        
        if ((ClientId!=null)&&(!Objects.equals(this.getCustomer().getId(), ClientId)))
            Result=false;
            
        if ((BeginT!=null)&&(this.getShedule().getBeginT().compareTo(BeginT)<=0))
            Result=false;
                
            
        if ((EndT!=null)&&(this.getShedule().getEndT().compareTo(EndT)>=0))
            Result=false;
        
        return Result;
    };
    
    private Money getRentSummaryCost()
    {
        Money Result=new Money(new Long(0));
        
        for (Rent r: this.Rents)
        {
            Result=new Money(Result.getAmount()+r.getCost(Shedule).getAmount());
        };
        
        return Result;
    };
    
    private Money getFee()
    {
        Money Result = null;
        Money RentCost=getRentSummaryCost();
        
        if (RentCost.getAmount()!=0)
        {
            Result=new Money(RentCost.getAmount()/10);
        }
        else
        {
            Result=new Money(new Long(10000));
        };
        
        return Result;
    };
    
    public Money getCost()
    {
        return new Money(getRentSummaryCost().getAmount()+getFee().getAmount());
    };
}
