/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

import java.util.Objects;

/**
 *
 * @author Inga
 */

/*Данный класс инкапсулирует информацию о стоимости аренды/ совокупной стоимости
мероприятия в наименьших единицах стоимости (использовано типовое решение 
"Деньги"). Паттерн Object Value (в инфраструктурном слое соответствует паттерну
Embedded Value для таблицы аренд).*/

public class Money 
{
    private final Long Amount;
    
    public Money(Long Amount)
    {
        if (Amount==null) throw new NullPointerException();
        if (Amount<0) throw new IllegalArgumentException();
        
        this.Amount=Amount;
    };
    
    @Override
    public boolean equals(Object Other)
    {
        return((Other instanceof Money)&&equals((Money)Other));
    };
    
    public Boolean equals(Money Other)
    {
        return (Objects.equals(this.Amount, Other.Amount));
    };
    
    @Override
    public int hashCode() 
    {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.Amount);
        return hash;
    };
    
    public Money getCost(Time T)
    {
        if (T==null) throw new NullPointerException();
        Long Hours;
        Hours=T.getHourIntFactoredPeriod();
        if(T.getMinuteIntFactoredPeriod()>0) Hours++;
        return new Money(Hours*this.Amount);
    };
    
    public Long getIntFactoredAmount(Integer Base)
    {
        if (Base==null) throw new NullPointerException();
        if (Base<=0) throw new IllegalArgumentException();
        
        return (this.Amount/Base);
    };
    
    public Long getRemFactoredAmount(Integer Base)
    {
        if (Base==null) throw new NullPointerException();
        if (Base<=0) throw new IllegalArgumentException();
        
        return (this.Amount%Base);
    };
    
    public Long getAmount()
    {
        return this.Amount;
    };
}
