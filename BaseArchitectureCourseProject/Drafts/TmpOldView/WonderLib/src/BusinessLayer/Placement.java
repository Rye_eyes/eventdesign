/*Пакет, реализующий слой бизнес-логики. Паттерн Domain Model.*/

package BusinessLayer;

/**
 *
 * @author Inga
 */

public class Placement extends Rent
{
    private String Address;
    private Integer Capacity;

    public Placement(Integer Id, Money PriceOf, Firm Owner, String Address, Integer Capacity) 
    {
        super(Id, PriceOf, Owner);
        
        if (Capacity<=0) throw new IllegalArgumentException();
        
        this.Address=Address;
        this.Capacity=Capacity;
    };
    
    public String getAddress()
    {
        return this.Address;
    };
    
    public void setAddress(String Address)
    {
        this.Address=Address;
    };
    
    public Integer getCapacity()
    {
        return this.Capacity;
    };
    
    public void setCapacity(Integer Capacity)
    {
       if (Capacity<=0) throw new IllegalArgumentException();
       
       this.Capacity=Capacity;
    };
    
}
