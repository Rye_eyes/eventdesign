/*Пакет, реализующий сервисный слой. Паттерн Domain Facade.*/

package ServiceLayer;

import BusinessLayer.Employee;
import BusinessLayer.Event;
import BusinessLayer.Facility;
import BusinessLayer.Placement;
import BusinessLayer.Rent;
import BusinessLayer.Time;
import DataAccessLayer.EmployeeMapper;
import DataAccessLayer.EventMapper;
import DataAccessLayer.FacilityMapper;
import DataAccessLayer.PlacementMapper;
import DataAccessLayer.RentMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Inga
 */

/*Данный класс реализует API бизнес-логики.
Паттерны Registry, Singleton.*/

public class BussinessAPI 
{
    private static BussinessAPI Instance=null;
    private static Employee CurrentUser=null;
   
    private BussinessAPI()
    {         
    };
    
    public static BussinessAPI getInstance()
    {
        if (Instance==null)
        {
            Instance=new BussinessAPI();
        };
        
        return Instance;
    };
    
    
    public Employee getCurrentUser()
    {
      return CurrentUser;  
    };
    
    // Функция входа. Т.к. выбран "тонкий фасад", реализуется в виде
    // функции реестра
    public boolean Login(String Username, String Password) throws SQLException, ClassNotFoundException
    {
        if ((Username==null)||(Password==null)) throw new NullPointerException();
        
        EmployeeMapper EM=EmployeeMapper.getInstance();
        CurrentUser=EM.getUserByLogin(Username, Password);
       
        return  (CurrentUser != null);
    };
    
    // Функция выхода
    
    public void Logout()
    {
       CurrentUser=null;  
    };
    
    // Функция получения списка событий пользователя (менеджер получает только
    // события, которые создал он сам, администратор - полный список событий)
    
    public ArrayList<Event> getEventsList() throws SQLException, ClassNotFoundException
    {
        if (CurrentUser==null) throw new NullPointerException();
        
        EventMapper EM=EventMapper.getInstance();
        
        ArrayList<Event> Result= new ArrayList<>();
        
        if (CurrentUser.getRank()==Employee.Appointment.Manager)
            Result=CurrentUser.getEvents();
        else
            Result=EM.getAllEvents();
            
        return Result;
    };
    
    // Функция получения списка событий с последующей фильтрацией. Данный
    // вариант алгоритма связан со спецификой использования библиотеки (порядок
    // загрузки GUI-клиента). TODO: рефакторинг, загрузка сразу через Event: filter
    // с установленными null
    
    public ArrayList<Event> getFilteredEventsList(Date BeginT, Date EndT, Integer ClientId) throws SQLException, ClassNotFoundException
    {   
       ArrayList<Event> tmpResult= getEventsList();
       ArrayList<Event> Result=new ArrayList<>();
       
       if ((BeginT==null)&&(EndT==null)&&(ClientId==null))
           Result=tmpResult;
       else
       {
            for (Event ev: tmpResult)
            {
                if (ev.filter(BeginT, EndT, ClientId))
                Result.add(ev);
            };
       }
     
       return Result;
    };
    
    // Функция получения списка свободных рент
    
    public ArrayList<Rent> getFreeRentsByShedule(Time Shedule) throws SQLException, ClassNotFoundException
    {
        ArrayList<Rent> Result=new ArrayList<>();
    
        RentMapper RM=RentMapper.getInstance();
        Result=RM.getRentsForTime(Shedule);
        
        return Result;
    };
    
    // Функция получения списка свободных рент (места проведения мероприятия)
    
    public ArrayList<Placement> getFreePlacementsByShedule(Time Shedule) throws SQLException, ClassNotFoundException
    {
        ArrayList<Placement> Result=new ArrayList<>();
    
        PlacementMapper PM=PlacementMapper.getInstance();
        Result=PM.getPlacementForTime(Shedule);
        
        return Result;
    };
    
      
    // Функция получения списка свободных рент (технических устройств)
    
    public ArrayList<Facility> getFreeFacilitiesByShedule(Time Shedule) throws SQLException, ClassNotFoundException
    {
        ArrayList<Facility> Result=new ArrayList<>();
    
        FacilityMapper FM=FacilityMapper.getInstance();
        Result=FM.getFacilitiesForTime(Shedule);
        
        return Result;
       
    };
    
}
