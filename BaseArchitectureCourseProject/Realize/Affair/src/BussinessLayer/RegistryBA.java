package BussinessLayer;

import static BussinessLayer.Quantum.QuantumType.аудиосистема;
import static BussinessLayer.Quantum.QuantumType.видеосистема;
import static BussinessLayer.Quantum.QuantumType.микрофон;
import static BussinessLayer.Quantum.QuantumType.проектор;
import static BussinessLayer.Quantum.QuantumType.шарики;
import java.util.ArrayList;
import java.util.Date;

public class RegistryBA 
{
    private static RegistryBA Instance=null;
    
    public static RegistryBA getInstance()
    {
        if (Instance==null)
        {
            Instance=new RegistryBA();
        };
        
        return Instance;
    };
    
    public ArrayList<Happening> getAllHappenings()
    {
        ArrayList<Happening> dumpHappenings=new ArrayList<>();
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        ArrayList<Quantum> QuantumList2= new ArrayList<>();
        ArrayList<Quantum> QuantumList3= new ArrayList<>();
        
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        
        QuantumList2.add(new Quantum(3, "C", 30, проектор) 
        );
        QuantumList2.add(new Quantum(4, "D", 40, шарики) 
        );
        
        
        QuantumList3.add(new Quantum(5, "E", 50, микрофон) 
        );
        QuantumList3.add(new Quantum(6, "F", 60, аудиосистема) 
        );
        
        
        dumpHappenings.add(new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "D", "E", "F"),
                          new Date(2014,9,11), new Date(2014,9,12), QuantumList1));
        dumpHappenings.add(new Happening(2,"hh",new Fellow(2, "G", "H", "K", Fellow.FellowType.менеджер),
                          new Celebrity(2, "L", "M", "N"),
                          new Date(2014,9,11), new Date(2014,9,13), QuantumList2));
        dumpHappenings.add(new Happening(3,"hhh", new Fellow(3, "O", "P", "R", Fellow.FellowType.менеджер),
                          new Celebrity(2, "S", "T", "Q"),
                          new Date(2014,9,17), new Date(2014,9,21), QuantumList3));
        
        return dumpHappenings;
    };
    
}
