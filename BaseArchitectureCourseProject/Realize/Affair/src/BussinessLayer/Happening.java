package BussinessLayer;

import java.util.ArrayList;
import java.util.Date;

public class Happening extends DomainObject
{
    private String Name;
    private Fellow Worker;
    private Celebrity Orderer;
    private Date Start;
    private Date End;
    private ArrayList<Quantum> Items=new ArrayList<>();
    

    public Happening(Integer Id, String Name, Fellow Worker, Celebrity Orderer, Date Start, 
            Date End, ArrayList<Quantum> Items) 
    {
        super(Id);
        
        this.Name=Name;
        
        if (isCorrectDates(Start, End))
        {
            this.Start=Start;
            this.End=End;
        };
        
        this.Worker=Worker;
        this.Orderer=Orderer;
        
        if(Items!=null)
        this.Items.addAll(Items);
    };
    
    public String getName()
    {
        return this.Name;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
    
    public Fellow getWorker()
    {
        return this.Worker;
    };
    
    public void setWorker(Fellow Worker)
    {
        this.Worker=Worker;
    };
    
    public Celebrity getOrderer()
    {
        return this.Orderer;
    };
    
    public void setOrderer(Celebrity Orderer)
    {
        this.Orderer=Orderer;
    };
    
    public Date getStart()
    {
        return this.Start;
    };
    
    public void setStart(Date Start)
    {
        if ((this.End==null)||(isCorrectDates(Start, this.End)))
            this.Start=Start;
    };
    
    public Date getEnd()
    {
        return this.End;
    };
    
    public void setEnd(Date End)
    {
        if ((this.Start==null)||(isCorrectDates(this.Start, End)))
            this.End=End;        
    };
    
    public ArrayList<Quantum> getItems()
    {
        return this.Items;
    };
    
    public static boolean isCorrectDates(Date potentialStart, Date potentialEnd)
    {
        return(potentialEnd.after(potentialStart));
    };
    
    public Long getHappeningCost()
    {
        Long S=0L;
        
        for (Quantum q: this.getItems())
        {
            S+=q.GetHourFee()*(this.getEnd().getTime()-this.getStart().getTime())/(1000*60*60);
        };
        
        return S;
    };
    
}
