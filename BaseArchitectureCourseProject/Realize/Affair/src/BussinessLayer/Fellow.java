package BussinessLayer;

public class Fellow extends Man
{
    public enum FellowType
    {
        администратор,
        менеджер
    };

    private FellowType Type;
    
    public Fellow(Integer Id, String Surname, String Name, String Patronym, 
            FellowType Type) 
    {
        super(Id, Surname, Name, Patronym);
        this.Type=Type;
    };
    
    public FellowType getType()
    {
        return this.Type;
    };
    
    public void setType(FellowType Type)
    {
        this.Type=Type;
    };
    
}
