package BussinessLayer;

import DataAccessLayer.HappeningMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class Quantum extends DomainObject
{
    public enum QuantumType
    {
        микрофон,
        шарики,
        аудиосистема,
        видеосистема,
        проектор
    };
    
    private String Owner=null;
    private Integer HourFee=null;
    private QuantumType Type=null;

    public Quantum(Integer Id, String Owner, Integer HourFee, QuantumType Type) 
    {
        super(Id);
        this.Owner=Owner;
        
        if (isCorrectFee(HourFee))
            this.HourFee=HourFee;
        
        this.Type=Type;
    };
    
    public String getOwner()
    {
        return this.Owner;
    };
    
    public void setOwner(String Owner)
    {
        this.Owner=Owner;
    };
    
    public Integer GetHourFee()
    {
        return this.HourFee;
    };
    
    public void setHourFee(Integer HourFee)
    {
        if (isCorrectFee(HourFee))
        this.HourFee=HourFee;
    };
    
    public QuantumType getType()
    {
        return this.Type;
    };
    
    public void setType(QuantumType Type)
    {
        this.Type=Type;
    };
    
    public static boolean isCorrectFee(Integer potentialHourFee)
    {
        return (potentialHourFee>0);
    };
    
    public boolean isFree(Date Start, Date End) throws SQLException
    {
        boolean Result=true;
        
        HappeningMapper HM=new HappeningMapper();
        
        ArrayList<Happening> AllHappenigs=HM.findAllHappenings();
        
        for (Happening H: AllHappenigs)
        {
            for(Quantum q: H.getItems())
            {
                if (this.getId()==q.getId())
                {
                    Long As=Start.getTime();
                    Long Ae=End.getTime();
                    
                    Long Bs=H.getStart().getTime();
                    Long Be=H.getEnd().getTime();
                    
                    if (((Bs<=As)&&(Be<=Ae)&&(Be>As))||
                            ((Bs<=As)&&(Be>=Ae))||
                            ((As<=Bs)&&(Ae<=Be)&&(Ae>Bs))||
                            ((As<=Bs)&&(Ae>=Be)))
                    Result=false;
                };
                
            };
        };
        
        return Result;
        
    };
}
