package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import DataAccessLayer.CelebrityMapper;
import DataAccessLayer.FellowMapper;
import DataAccessLayer.HappeningMapper;
import DataAccessLayer.QuantumMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import org.apache.derby.impl.sql.compile.HalfOuterJoinNode;

public class API 
{
  
    private Fellow CurrentUser=null;
    
    public Fellow getCurrentUser()
    {
        return CurrentUser;
    };
    
    
    public boolean Login(String Name, String Password) throws SQLException
    {
        FellowMapper FM=new FellowMapper();
        CurrentUser=FM.findFellowByLoginPass(Name, Password);
        return (CurrentUser!=null);
    };
    
    public void logout()
    {
        CurrentUser=null;
    }
    
    public ArrayList<Happening> getHappenings() throws SQLException
    {
        if (CurrentUser!=null)
        {
            HappeningMapper HM=new HappeningMapper();
            ArrayList<Happening> tmpResults=HM.findAllHappenings();
            if (tmpResults==null) return null;
            
            ArrayList<Happening> Results=new ArrayList<>();
            
            for (Happening h: tmpResults)
            {
                if (CurrentUser.getType()==Fellow.FellowType.менеджер)
                {
                    if (h.getWorker()!=null)
                    if (Objects.equals(h.getWorker().getId(), CurrentUser.getId()))
                        Results.add(h);
                }
                else
                {
                   Results.add(h);
                };
            };
            
            if (Results.isEmpty()) return null;
            else
                return Results;
        }
        else
            return null;    
    };  
    
    public Happening getHappeningById(Integer Id) throws SQLException
    {
        ArrayList<Happening> ResultTmp=getHappenings();
        if (ResultTmp==null)
        {
            return null;
        }
        else
            for (Happening h: ResultTmp)
            {
                if (Objects.equals(h.getId(), Id))
                {
                    return h;
                }
            };
            
        return null;
    };
    
    public void deleteHappeningById(Integer Id) throws SQLException
    {
        Happening deletedHappening=getHappeningById(Id);
       
        if (deletedHappening==null)
        {
            return;
        }
        else
        {
            HappeningMapper HM=new HappeningMapper();
            HM.Delete(deletedHappening);
        };
    };
    
    public Integer createHappening(Integer CelebrityId, Date Start, Date End,
                                        String Name, ArrayList<Quantum> Items) throws SQLException, ClassNotFoundException
    {
        if ((CurrentUser==null)||(!Happening.isCorrectDates(Start, End)))
        {
            return null;
        }
        else
        {
            CelebrityMapper CM=new CelebrityMapper();
            Celebrity tmpCelebrity=(Celebrity)CM.findManById(CelebrityId);
            
            if (tmpCelebrity==null) return null;
            
            HappeningMapper HM=new HappeningMapper();
            Integer res=HM.Insert(new Happening(null, Name, CurrentUser, tmpCelebrity, Start, End, Items));
            return res;
        }
        
    };
    
    public ArrayList<Quantum> getFreeQuantum(Date Start, Date End) throws SQLException
    {
        if (CurrentUser==null)
        {
            return null;
        }
        else
        {
            QuantumMapper QM=new QuantumMapper();
            
            ArrayList<Quantum> tmpRes= QM.findAllQuantum();
            
            if (tmpRes.isEmpty())
            {
                return null;
            };
            ArrayList<Quantum> reall=new ArrayList<>();
            
            for (Quantum q: tmpRes)
            {
                if (q.isFree(Start, End))
                {
                    reall.add(q);
                };
            };
            
            return  reall;
        }
        
    };
    
    public ArrayList<Celebrity> getCelebrities() throws SQLException
    {
        if (CurrentUser==null)
        {
            return null;
        }
        else
        {
            if (CurrentUser.getType()!=Fellow.FellowType.администратор)
                return null;
            
            CelebrityMapper CM=new CelebrityMapper();
            return CM.findAllCelebrities();
        }
    };
    
    public void deleteCelebrityById(Integer Id) throws SQLException
    {
        if ((CurrentUser==null)||(CurrentUser.getType()!=Fellow.FellowType.администратор))
        {
            return;
        }
        else
        {
            CelebrityMapper CM=new CelebrityMapper();
            Celebrity deletedCelebrity=(Celebrity)CM.findManById(Id);
            CM.Delete(deletedCelebrity);
            return;
        }
        
    };
    
    public Integer createCelebrity(String Surname, String Name, String Patronym) throws SQLException, ClassNotFoundException
    {
    
        if ((CurrentUser==null)||(CurrentUser.getType()!=Fellow.FellowType.администратор))
        {
            return null;
        }
        else
        {
            CelebrityMapper CM=new CelebrityMapper();
            Integer res=CM.Insert(new Celebrity(null, Surname, Name, Patronym));
            return res;
        }
    }
}
