package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;


public class HappeningMapper extends AbstractMapper
{
    
    public HappeningMapper() throws SQLException
    {
        super();
    };

    @Override
    protected DomainObject load(ResultSet rs) throws SQLException 
    {
        Happening Result=null;
        
        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Result=new Happening(rs.getInt("PK"),rs.getString("Name"), null, null, new Date(rs.getTimestamp("Start").getTime()), new Date(rs.getTimestamp("Finish").getTime()), null);
            
            
            String Ask="SELECT * FROM MHAT WHERE FK_H="+Result.getId().toString();
            
            Connection e1conn = DriverManager.getConnection(Host, Name, Pass);
            e1conn.setAutoCommit(false);
            
            Statement e1stmt = e1conn.createStatement();
            ResultSet rsn=e1stmt.executeQuery(Ask);
            
            ArrayList<Integer> ManAssociatedPKs=new ArrayList<>();
            
            while(rsn.next())
            {
                ManAssociatedPKs.add(rsn.getInt("FK_M"));
            };
            
            FellowMapper FM=new FellowMapper();
            CelebrityMapper CM=new CelebrityMapper();
            
            for (Integer Id: ManAssociatedPKs)
            {
                if (FM.findManById(Id)!=null) Result.setWorker((Fellow)FM.findManById(Id));
                if (CM.findManById(Id)!=null) Result.setOrderer((Celebrity)CM.findManById(Id));
                
            };
           
            
            Ask="SELECT * FROM HQAT WHERE FK_H="+Result.getId().toString();
            
            Connection e2conn = DriverManager.getConnection(Host, Name, Pass);
            e2conn.setAutoCommit(false);
            
            Statement e2stmt = e2conn.createStatement();
            ResultSet rsm=e2stmt.executeQuery(Ask);
            
            ArrayList<Integer> QuantumAssociatedPKs=new ArrayList<>();
            
            while(rsm.next())
            {
                QuantumAssociatedPKs.add(rsm.getInt("FK_Q"));
            };
            
            QuantumMapper QM=new QuantumMapper();
            
            for (Integer Id: QuantumAssociatedPKs)
            {
                Result.getItems().add(QM.findQuantumById(Id));
            };
            
        }
        else
            Result=(Happening) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };
    
    public ArrayList<Happening> findAllHappenings() throws SQLException
    {
        ArrayList<DomainObject> Result= DomainObjectsFind("SELECT * FROM HAPPENING");
        ArrayList<Happening> HappeningQ=new ArrayList<>();
        
        for (DomainObject DO:Result)
        {
            HappeningQ.add((Happening)DO);
        }
        
        return HappeningQ;
    };

    @Override
    protected String insertStatement() {
         return "INSERT INTO Happening (PK, Name, Start, Finish"
                + ") VALUES (?,?,?,?)";
    }

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException {
        Happening SubjectIns=(Happening)Subject;
        
        insertStatement.setString(2, SubjectIns.getName());
        insertStatement.setTimestamp(3, new Timestamp(SubjectIns.getStart().getTime()));
        insertStatement.setTimestamp(4, new Timestamp(SubjectIns.getEnd().getTime()));
        
    };
    
    @Override
    public Integer Insert(DomainObject Subject) throws SQLException, ClassNotFoundException
    {
        Integer res=super.Insert(Subject);
        stmt.execute("INSERT INTO MHAT (FK_M, FK_H) VALUES ("+((Happening)Subject).getWorker().getId()+","+((Happening)Subject).getId()+")");
        stmt.execute("INSERT INTO MHAT (FK_M, FK_H) VALUES ("+((Happening)Subject).getOrderer().getId()+","+((Happening)Subject).getId()+")");
        
        for (Quantum q: ((Happening)Subject).getItems())
        {
           stmt.execute("INSERT INTO HQAT (FK_Q, FK_H) VALUES ("+q.getId()+","+((Happening)Subject).getId()+")");
        };
     
        con.commit();
        return res;
    };
    
    @Override
    public void Delete(DomainObject Subject) throws SQLException
    {
        stmt.execute("DELETE FROM MHAT WHERE FK_H="+Subject.getId());
        stmt.execute("DELETE FROM HQAT WHERE FK_H="+Subject.getId());
        
        super.Delete(Subject);
        con.commit();
    };

    @Override
    protected String deleteStatement() 
    {
         return "DELETE FROM Happening WHERE PK=(?)";    
    };
    
    
}
