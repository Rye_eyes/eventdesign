package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Man;
import BussinessLayer.Quantum;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class CelebrityMapper extends ManMapper
{
    
    public CelebrityMapper() throws SQLException
    {
        super();
    };

    @Override
    protected DomainObject load(ResultSet rs) throws SQLException 
    {
        Celebrity Result=null;
        
        if (rs.getInt("Class")!=2) return null;


        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Man Base=(Man)super.load(rs);

            Result=new Celebrity(Base.getId(),Base.getSurname(), Base.getName(), Base.getPatronym());
        }
        else
            Result=(Celebrity) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };

     
    public ArrayList<Celebrity> findAllCelebrities() throws SQLException
    {
        ArrayList<DomainObject> Result= DomainObjectsFind("SELECT * FROM ManSIT");
        ArrayList<Celebrity> ResultQ=new ArrayList<>();
        
        for (DomainObject DO:Result)
        {
            ResultQ.add((Celebrity)DO);
        }
        
        return ResultQ;
    };
     
    @Override
    protected String insertStatement()
    {
        return "INSERT INTO ManSIT (PK, Surname, Name, Patronym, Class, Type,"
                + "Login, Password) VALUES (?,?,?,?,?,?,?,?)";
    };

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException 
    {
        Celebrity SubjectIns=(Celebrity)Subject;
        
        insertStatement.setString(2, SubjectIns.getSurname());
        insertStatement.setString(3, SubjectIns.getName());
        insertStatement.setString(4, SubjectIns.getPatronym());
        insertStatement.setInt(5, 2);
        insertStatement.setString(6, null);
        insertStatement.setString(7, null);
        insertStatement.setString(8, null);
    };

    @Override
    protected String deleteStatement() 
    {
        return "DELETE FROM ManSIT WHERE PK=(?)";    
    };
    
    @SuppressWarnings("empty-statement")
    public void Delete(DomainObject Subject) throws SQLException
    {
        HappeningMapper HM=new HappeningMapper();
        ArrayList<Happening> HL=new ArrayList<>();
     
        HL=HM.findAllHappenings();
        
        if (!HL.isEmpty())
        for(Happening H:HL)
        {
            if (H.getOrderer()==null) continue;
            if(Objects.equals(H.getOrderer().getId(), Subject.getId()))
                HM.Delete(H);
        };
        
        super.Delete(Subject);
    };

    
}
