package DataAccessLayer;

import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import BussinessLayer.Man;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FellowMapper extends ManMapper 
{
    
    public FellowMapper() throws SQLException
    {
        super();
    };
    
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException
    {
      
        Fellow Result=null;
        
        if (rs.getInt("Class")!=1) return null;


        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Man Base=(Man)super.load(rs);

            Fellow.FellowType Appointment;
            Appointment= Fellow.FellowType.valueOf(rs.getString("Type"));

            Result=new Fellow(Base.getId(),Base.getSurname(), Base.getName(), Base.getPatronym(),Appointment);
        }
        else
            Result=(Fellow) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
    };

    public Fellow findFellowByLoginPass(String Login, String Password) throws SQLException
    {
        ArrayList<DomainObject> Result= DomainObjectsFind("SELECT * FROM MANSIT "
                + "WHERE Login='"+Login+"' AND PASSWORD='"+Password+"'");
        
        Fellow Cust=null;
        
        if (!Result.isEmpty())
            Cust=(Fellow) Result.get(0);
        
        return Cust;
    };
    
    @Override
    protected String insertStatement() 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String deleteStatement() 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
