package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import BussinessLayer.Fellow.FellowType;
import BussinessLayer.Man;
import BussinessLayer.Quantum;
import static DataAccessLayer.AbstractMapper.AllIdentitiesMap;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class QuantumMapper extends AbstractMapper
{

    public QuantumMapper() throws SQLException
    {
        super();
    };
        
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException 
    {
        Quantum Result=null;
       
        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
        {
            Result=new Quantum(rs.getInt("PK"),rs.getString("Owner"), rs.getInt("Fee"), Quantum.QuantumType.valueOf(rs.getString("Type")));
        }
        else
            Result=(Quantum) AllIdentitiesMap.get(rs.getInt("PK"));


        return (DomainObject)Result;
        
    };
    
    public ArrayList<Quantum> findAllQuantum() throws SQLException
    {
        ArrayList<DomainObject> Result= DomainObjectsFind("SELECT * FROM QUANTUM");
        ArrayList<Quantum> ResultQ=new ArrayList<>();
        
        for (DomainObject DO:Result)
        {
            ResultQ.add((Quantum)DO);
        }
        
        return ResultQ;
    };
    
    public Quantum findQuantumById(Integer Id) throws SQLException
    {
        ArrayList<DomainObject> Result= DomainObjectsFind("SELECT * FROM QUANTUM "
                + "WHERE PK="+Id.toString());
        
        Quantum Cust=null;
        
        if (!Result.isEmpty())
            Cust=(Quantum) Result.get(0);
        
        return Cust;
    };

    @Override
    protected String insertStatement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doInsert(DomainObject Subject, PreparedStatement insertStatement) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String deleteStatement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
