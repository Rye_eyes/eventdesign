package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import junit.framework.TestCase;

public class APITest extends TestCase 
{
    
    public APITest(String testName) 
    {
        super(testName);
    }

    public void testGetCurrentUser() throws SQLException 
    {
        System.out.println("getCurrentUser");
        API instance = new API();
        instance.Login("happy", "Goal");
        String expResult = "Ступин";
        Fellow result = instance.getCurrentUser();
        assertEquals(expResult, result.getSurname());
    }

    public void testLogin() throws Exception 
    {
        System.out.println("Login");
        String Name = "happy";
        String Password = "Goal";
        API instance = new API();
        boolean expResult = true;
        boolean result = instance.Login(Name, Password);
        assertEquals(expResult, result);
      
    }

    public void testLogout() throws SQLException 
    {
        System.out.println("logout");
        API instance = new API();
        instance.Login("happy", "Goal");
        instance.logout();
        assertEquals(instance.getCurrentUser(), null);
    }

    public void testGetHappenings() throws Exception 
    {
        System.out.println("getHappenings");
        API instance = new API();
        instance.Login("happy", "Goal");
        
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(16);
        expResultIds.add(17);
        expResultIds.add(18);
        expResultIds.add(19);
        expResultIds.add(20);
        
        ArrayList<Happening> resultl = instance.getHappenings();
        
        boolean result=true;
        
        for (int i=0; i<expResultIds.size(); i++)
        {
            if (!Objects.equals(resultl.get(i).getId(), expResultIds.get(i)))
                result=false;
        };
        
        assertEquals(result, true);
    };

    public void testGetHappeningById() throws Exception 
    {
        System.out.println("getHappeningById");
        Integer Id = 18;
        API instance = new API();
        instance.Login("456", "Snake");
        Happening expResult = null;
        Happening result = instance.getHappeningById(Id);
        assertEquals(expResult, result);
       
    };

    public void testDeleteHappeningById() throws Exception 
    {
        System.out.println("deleteHappeningById");
        
        Integer CelebrityId = 6;
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014, 10, 9, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014, 10, 10, 0, 0);
        
        Date Start = ins1.getTime();
        Date End = ins2.getTime();
        
        String Name = "Трамонтана!";
        
        API instance = new API();
        instance.Login("happy", "Goal");
        
        ArrayList<Quantum> Items = instance.getFreeQuantum(Start, End);
        Integer newH=instance.createHappening(CelebrityId, Start, End, Name, Items);
        
     
        instance.deleteHappeningById(newH);
        Happening res=instance.getHappeningById(newH);
        assertNull(res);
    };

    public void testCreateHappening() throws Exception 
    {
        System.out.println("createHappening");
        Integer CelebrityId = 6;
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014, 10, 9, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014, 10, 10, 0, 0);
        
        Date Start = ins1.getTime();
        Date End = ins2.getTime();
        
        String Name = "Трамонтана!";
        
        API instance = new API();
        instance.Login("happy", "Goal");
        ArrayList<Quantum> Items = instance.getFreeQuantum(Start, End);
        Integer newH=instance.createHappening(CelebrityId, Start, End, Name, Items);
        
        assertNotNull(instance.getHappeningById(newH));
        instance.deleteHappeningById(newH);
        
    }

    public void testGetFreeQuantum() throws Exception 
    {
        System.out.println("getFreeQuantum");
       
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014,7,7,21,0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014,7,8,2,0);
        
        Date Start = ins1.getTime();
        Date End = ins2.getTime();
        
        API instance = new API();
        instance.Login("happy", "Goal");
         
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(13);
        expResultIds.add(14);
        
        ArrayList<Quantum> result = instance.getFreeQuantum(Start, End);
        
        boolean real=true;
        
        for (int i=0; i<expResultIds.size(); i++)
        {
            if (!Objects.equals(result.get(i).getId(), expResultIds.get(i))) real=false;
        };
        assertEquals(real, true);
    }

    public void testGetCelebrities() throws Exception 
    {
        System.out.println("getCelebrities");
        
        API instance = new API();
        instance.Login("happy", "Goal");
        
        ArrayList<Integer> expResultIds = new ArrayList<>();
        
        expResultIds.add(6);
        expResultIds.add(7);
        expResultIds.add(8);
        expResultIds.add(9);
        expResultIds.add(10);
        
        boolean tmpres=true;
        
        ArrayList<Celebrity> result = instance.getCelebrities();
        
        for (int i=0;i<5;i++)
        {
            if (!Objects.equals(expResultIds.get(i), result.get(i).getId()))
                tmpres=false;
        };
        
        assertEquals(tmpres, true);
    };

    @SuppressWarnings("empty-statement")
    public void testDeleteCelebrityById() throws Exception 
    {
        System.out.println("deleteCelebrityById");
        
        String Surname = "Ono";
        String Name = "L";
        String Patronym = "Yoko";
        
        API instance = new API();
        instance.Login("happy", "Goal");
        Integer Id=instance.createCelebrity(Surname, Name, Patronym);
        
        ArrayList<Celebrity> tmpList=instance.getCelebrities();
        
        boolean exist=false;
        for (Celebrity c:tmpList)
        {
            if(Objects.equals(c.getId(), Id)) 
                exist=true;
        };
        
        assertEquals(exist, true);
        
        instance.deleteCelebrityById(Id);
        
        tmpList=instance.getCelebrities();
        
        exist=false;
        for (Celebrity c:tmpList)
        {
            if(Objects.equals(c.getId(), Id)) 
                exist=true;
        };
        
        assertEquals(exist, false);
        
    };

    public void testCreateCelebrity() throws Exception 
    {
        System.out.println("createCelebrity");
        String Surname = "Ono";
        String Name = "L";
        String Patronym = "Yoko";
        API instance = new API();
        instance.Login("happy", "Goal");
        Integer Id=instance.createCelebrity(Surname, Name, Patronym);
        
        ArrayList<Celebrity> tmpList=instance.getCelebrities();
        boolean exist=false;
        for (Celebrity c:tmpList)
        {
            if(Objects.equals(c.getId(), Id)) 
                exist=true;
        };
        
        assertEquals(exist, true);
        instance.deleteCelebrityById(Id);
    }
    
}
