package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Objects;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

public class CelebrityMapperTest extends TestCase 
{
    
    public CelebrityMapperTest(String testName) 
    {
        super(testName);
    }

    public void testFindCelebrityById() throws Exception 
    {
        System.out.println("findManById(Celebrity)");
        Integer Id = 6;
        CelebrityMapper CM=new CelebrityMapper();
        String Surname = "Смородина";
        Celebrity result = (Celebrity) CM.findManById(Id);
        assertEquals(Surname, result.getSurname());
    };
    
    public void testInsert() throws Exception 
    {
        System.out.println("Insert(Celebrity)");
        CelebrityMapper CM=new CelebrityMapper();
        Celebrity newCelebrity=new Celebrity(null, "Ицыксон", "Владимир", "Михайлович");
        Integer Id=CM.Insert(newCelebrity);
        Celebrity result=(Celebrity)CM.findManById(Id);
        assertEquals("Ицыксон", result.getSurname());
        CM.Delete(result);
    };
    
    
    public void testDelete() throws Exception 
    {
        System.out.println("Delete(Celebrity)");           
        CelebrityMapper CM=new CelebrityMapper();
        Celebrity newCelebrity=new Celebrity(null, "Ицыксон", "Владимир", "Михайлович");
        Integer Id=CM.Insert(newCelebrity);
        Celebrity result=(Celebrity)CM.findManById(Id);
        CM.Delete(result);
        
        assertEquals(null, (Celebrity)CM.findManById(Id));
    };

    public void testFindAllCelebrities() throws Exception 
    {
        System.out.println("findAllCelebrities");
        CelebrityMapper instance = new CelebrityMapper();
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(6);
        expResultIds.add(7);
        expResultIds.add(8);
        expResultIds.add(9);
        expResultIds.add(10);
        
        ArrayList<Celebrity> result = instance.findAllCelebrities();
        boolean real=true;
        
        for (int i=0;i<5;i++)
        {
           if(!Objects.equals(expResultIds.get(i), result.get(i).getId())) real=false;  
        };
        
        
        assertEquals(real, true);
        
    }

}
