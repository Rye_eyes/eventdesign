/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataAccessLayer;

import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import BussinessLayer.Fellow.FellowType;
import BussinessLayer.Man;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author Inga
 */
public class FellowMapperTest extends TestCase {
    
    public FellowMapperTest(String testName) {
        super(testName);
    }

    public void testFindFellowById() throws Exception 
    {
        System.out.println("findManById(Fellow)");
        Integer Id = 4;
        FellowMapper FM=new FellowMapper();
        FellowType expResult = FellowType.менеджер;
        Fellow result = (Fellow) FM.findManById(4);
        assertEquals(expResult, result.getType());
    };
    
    public void testFindFellowByLoginPass() throws Exception
    {
        System.out.println("findFellowByLoginPass");
        FellowMapper FM=new FellowMapper();
        String expResult = "Ступин";
        Fellow result = (Fellow) FM.findFellowByLoginPass("happy", "Goal");
        assertEquals(expResult, result.getSurname());
    }
}
