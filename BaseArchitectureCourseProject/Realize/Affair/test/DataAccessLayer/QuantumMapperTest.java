package DataAccessLayer;

import BussinessLayer.DomainObject;
import BussinessLayer.Quantum;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Objects;
import junit.framework.TestCase;


public class QuantumMapperTest extends TestCase 
{
    
    public QuantumMapperTest(String testName) 
    {
        super(testName);
    }

    public void testFindAllQuantum() throws Exception {
        System.out.println("findAllQuantum");
        QuantumMapper instance = new QuantumMapper();
        ArrayList<Integer> expResult = new ArrayList<>();
        expResult.add(11);
        expResult.add(12);
        expResult.add(13);
        expResult.add(14);
        expResult.add(15);
        ArrayList<Quantum> result = instance.findAllQuantum();
        
        boolean rqesult=true;

        for (int i=0; i<5; i++)
        {
            if (!Objects.equals(result.get(i).getId(), expResult.get(i))) rqesult=false;
        };
        assertEquals(rqesult, true);
    };

    public void testFindQuantumById() throws Exception 
    {
        System.out.println("findQuantumById");
        Integer Id = 15;
        QuantumMapper instance = new QuantumMapper();
        Quantum.QuantumType expResult = Quantum.QuantumType.шарики;
        Quantum result = instance.findQuantumById(Id);
        assertEquals(expResult, result.getType());
    };

}
