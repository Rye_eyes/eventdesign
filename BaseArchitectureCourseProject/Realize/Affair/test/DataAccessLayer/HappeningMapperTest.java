package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.DomainObject;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import static BussinessLayer.Quantum.QuantumType.аудиосистема;
import static BussinessLayer.Quantum.QuantumType.видеосистема;
import static BussinessLayer.Quantum.QuantumType.микрофон;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import junit.framework.TestCase;

public class HappeningMapperTest extends TestCase {
    
    public HappeningMapperTest(String testName) 
    {
        super(testName);
    }

    
    public void testFindAllHappenings() throws Exception {
        System.out.println("findAllHappenings");
        HappeningMapper instance = new HappeningMapper();
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(16);
        expResultIds.add(17);
        expResultIds.add(18);
        expResultIds.add(19);
        expResultIds.add(20);
        
        ArrayList<Happening> result = instance.findAllHappenings();
        
        boolean ist=true;
        
        for(int i=0; i<5; i++)
        {
            if (expResultIds.get(i)!=result.get(i).getId())ist=false;
        };
        
        
        assertEquals(ist, true);
    };

    
    public void testInsert() throws Exception 
    {
        System.out.println("Insert");
       
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
       
        
        QuantumList1.add(new Quantum(11, "СуперМегаАудио", 50, аудиосистема) 
        );
        QuantumList1.add(new Quantum(12, "СуперМегаАудио", 75, микрофон) 
        );
        
        DomainObject Subject = new Happening(null,"h",new Fellow(3, "A", "B", "C", Fellow.FellowType.менеджер),
                               new Celebrity(2, "D", "E", "F"),
                               new Date(2014,9,11), new Date(2014,9,12), QuantumList1);
        HappeningMapper instance = new HappeningMapper();
        Integer result=instance.Insert(Subject);
        
        ArrayList<Happening> resultl = instance.findAllHappenings();
        ArrayList<Integer> resultlIds=new ArrayList<>();
        
        for (Happening f:resultl)
        {
            resultlIds.add(f.getId());
        };
        
        assertEquals(resultlIds.contains(result), true);
        
        instance.Delete(Subject);
    };

    
    public void testDelete() throws Exception 
    {
        System.out.println("Delete");
        
          ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(11, "СуперМегаАудио", 50, аудиосистема) 
        );
        QuantumList1.add(new Quantum(12, "СуперМегаАудио", 75, микрофон) 
        );
        
        DomainObject Subject = new Happening(null,"h",new Fellow(3, "A", "B", "C", Fellow.FellowType.менеджер),
                               new Celebrity(2, "D", "E", "F"),
                               new Date(2014,9,11), new Date(2014,9,12), QuantumList1);
        HappeningMapper instance = new HappeningMapper();
        Integer result=instance.Insert(Subject);
        
        instance.Delete(Subject);
        
        ArrayList<Happening> resultl = instance.findAllHappenings();
        ArrayList<Integer> resultlIds=new ArrayList<>();
        
        for (Happening f:resultl)
        {
            resultlIds.add(f.getId());
        };
        
        assertEquals(!resultlIds.contains(result), true);
        
    }

}
