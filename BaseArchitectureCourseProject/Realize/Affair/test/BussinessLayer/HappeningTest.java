package BussinessLayer;

import static BussinessLayer.Quantum.QuantumType.аудиосистема;
import static BussinessLayer.Quantum.QuantumType.видеосистема;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import junit.framework.TestCase;

public class HappeningTest extends TestCase 
{
    
    public HappeningTest(String testName) 
    {
        super(testName);
    };
    
    public void testGetName()
    {
        System.out.println("getName");
        Fellow F = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        
        Happening instance;
        instance = new Happening(1,"h",F,
                new Celebrity(1, "D", "E", "F"),
                ins1.getTime(), ins2.getTime(), QuantumList1);
        
        String expResult="h";
        String result = instance.getName();
        assertEquals(expResult, result);
    };
    
    public void testSetName()
    {
        System.out.println("setName");
        Fellow F = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",F,
                          new Celebrity(1, "D", "E", "F"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        
        String expResult="hh";
        
        instance.setName(expResult);
        
        String result = instance.getName();
        assertEquals(expResult, result);
        
    };
    
    public void testSetName(String Name)
    {
        System.out.println("setName");
    };

    public void testGetWorker() 
    {
        System.out.println("getWorker");
        Fellow expResult = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",expResult,
                          new Celebrity(1, "D", "E", "F"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        
        Fellow result = instance.getWorker();
        assertEquals(expResult, result);
    };

    public void testSetWorker() 
    {
        System.out.println("setWorker");
        Fellow expResult = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(3, "J", "K", "L", Fellow.FellowType.менеджер),
                          new Celebrity(1, "D", "E", "F"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        
        instance.setWorker(expResult);
        
        assertEquals(expResult, instance.getWorker());
    };

    public void testGetOrderer() 
    {
        System.out.println("getOrderer");
        Celebrity expResult = new Celebrity(1, "D", "E", "F");

        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          expResult,
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        
        Celebrity result = instance.getOrderer();
        assertEquals(expResult, result);
    };

    public void testSetOrderer() 
    {
        System.out.println("setOrderer");
        Celebrity expResult = new Celebrity(1, "D", "E", "F");

        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        instance.setOrderer(expResult);
        assertEquals(expResult, instance.getOrderer());
    };

    public void testGetStart() 
    {
        System.out.println("getStart");
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        
        Date result = instance.getStart();
        assertEquals(ins1.getTime(), result);
    };

    public void testSetStart() 
    {
        System.out.println("setStart");
        Date Start = new Date(2014,9,11,12,00);
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
      
        instance.setStart(Start);
        assertEquals(Start, instance.getStart());
      
    };

    public void testGetEnd() 
    {
        System.out.println("getEnd");
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        Date expResult = new Date(2014,9,12);
        Date result = instance.getEnd();
        assertEquals(ins2.getTime(), result);
    };

    public void testSetEnd() 
    {
        System.out.println("setEnd");
        Date End = new Date(2014,9,25);
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        instance.setEnd(End);
        
        assertEquals(End, instance.getEnd());
    }

    public void testGetItems() 
    {
        System.out.println("getItems");
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        ArrayList<Quantum> expResult = QuantumList1;
        ArrayList<Quantum> result = instance.getItems();
        assertEquals(expResult, result);
    };

    public void testIsCorrectDates() 
    {
        System.out.println("isCorrectDates");
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Date potentialStart = ins2.getTime();
        Date potentialEnd = ins1.getTime();
        boolean expResult = false;
        boolean result = Happening.isCorrectDates(potentialStart, potentialEnd);
        assertEquals(expResult, result);
    };

    public void testGetHappeningCost() 
    {
        System.out.println("getHappeningCost");
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          ins1.getTime(), ins2.getTime(), QuantumList1);
        Long expResult = 720L;
        Long result = instance.getHappeningCost();
        assertEquals(expResult, result);
    };
    
}
