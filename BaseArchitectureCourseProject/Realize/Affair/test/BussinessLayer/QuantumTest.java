package BussinessLayer;

import static BussinessLayer.Quantum.QuantumType.аудиосистема;
import static BussinessLayer.Quantum.QuantumType.шарики;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import junit.framework.TestCase;

public class QuantumTest extends TestCase 
{
    
    public QuantumTest(String testName) 
    {
        super(testName);
    }

    public void testGetOwner() 
    {
        System.out.println("getOwner");
        Quantum instance = new Quantum(1, "A", 10, аудиосистема) ;
        String expResult = "A";
        String result = instance.getOwner();
        assertEquals(expResult, result);
    };

    public void testSetOwner() 
    {
        System.out.println("setOwner");
        String Owner = "B";
        Quantum instance = new Quantum(1, "A", 10, аудиосистема) ;
        instance.setOwner(Owner);
        assertEquals(Owner, instance.getOwner());
    };

    public void testGetHourFee() 
    {
        System.out.println("GetHourFee");
        Quantum instance = new Quantum(1, "A", 10, аудиосистема) ;
        Integer expResult = 10;
        Integer result = instance.GetHourFee();
        assertEquals(expResult, result);
        
    };

    public void testSetHourFee() 
    {
        System.out.println("setHourFee");
        Integer HourFee = 15;
        Quantum instance = new Quantum(1, "A", 10, аудиосистема) ;
        instance.setHourFee(HourFee);
        assertEquals(HourFee, instance.GetHourFee());
    };

    public void testGetType() 
    {
        System.out.println("getType");
        Quantum instance = new Quantum(1, "A", 10, аудиосистема) ;
        Quantum.QuantumType expResult = аудиосистема;
        Quantum.QuantumType result = instance.getType();
        assertEquals(expResult, result);
    };

    public void testSetType() 
    {
        System.out.println("setType");
        Quantum.QuantumType Type = шарики;
        Quantum instance = new Quantum(1, "A", 10, аудиосистема) ;
        instance.setType(Type);
        assertEquals(Type, instance.getType());
    };

    public void testIsCorrectFee() 
    {
        System.out.println("isCorrectFee");
        Integer potentialHourFee = -5;
        boolean expResult = false;
        boolean result = Quantum.isCorrectFee(potentialHourFee);
        assertEquals(expResult, result);
    };

    public void testIsFree() throws SQLException 
    {
        System.out.println("isFree");
        Date Start;
        Date End;
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014,7,7,11,00);
        Start=ins1.getTime();
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014,7,8,00,00);
        
        End=ins2.getTime();
        
        
        Quantum instance = new Quantum(11, "СуперМегаАудио", 50, аудиосистема) ;
        boolean expResult = false;
        boolean result = instance.isFree(Start, End);
        assertEquals(expResult, result);
    };
    
}
