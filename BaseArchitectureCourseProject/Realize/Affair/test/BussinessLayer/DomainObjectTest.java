package BussinessLayer;

import junit.framework.TestCase;

public class DomainObjectTest extends TestCase 
{
    
    public DomainObjectTest(String testName) 
    {
        super(testName);
    }

    public void testGetId() 
    {
        System.out.println("getId");
        DomainObject instance = new DomainObject(1);
        Integer expResult = 1;
    
        Integer result = instance.getId();
        
        assertEquals(expResult, result);
        
    }

    public void testSetId() 
    {
        System.out.println("setId");
        Integer Id = 1;
        DomainObject instance = new DomainObject(2);
        instance.setId(Id);
        assertEquals(Id, instance.getId());
    }
    
}
