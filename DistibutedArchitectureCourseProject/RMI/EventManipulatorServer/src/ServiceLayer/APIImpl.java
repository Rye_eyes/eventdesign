package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.DBCommonOperationsRegistry;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import static BussinessLayer.Happening.DateFromString;
import BussinessLayer.Quantum;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

public class APIImpl extends UnicastRemoteObject implements API
{ 
    private Fellow CurrentUser=null;
    private DBCommonOperationsRegistry Registry=new DBCommonOperationsRegistry();
    
    public APIImpl () throws RemoteException
    {
                
    };
    
    @Override
    public Fellow getCurrentUser() throws RemoteException
    {
        return CurrentUser;
    };

    @Override
    public Boolean login(String Name, String Password) throws SQLException, RemoteException
    {
        System.out.println("Login accepted.");
        this.CurrentUser=Registry.Login(Name, Password);
        return (this.CurrentUser!=null);
    };
    
    @Override
    public void logout() throws RemoteException
    {
        CurrentUser=null;
    }
    
    @Override
    public ArrayList<Happening> getHappenings() throws SQLException, RemoteException
    {
        ArrayList<Happening> Res=new ArrayList<>();
        
        Iterator it=CurrentUser.getHappenings();
        
        while(it.hasNext())
        {
            Res.add((Happening)it.next());
        }
        
        return Res;
    };  
    
    @Override
    public Happening getHappeningById(Integer Id) throws SQLException, RemoteException
    {            
        return CurrentUser.getHappeningById(Id);
    };
    
    @Override
    public void deleteHappeningById(Integer Id) throws SQLException, RemoteException
    {
        CurrentUser.deleteHappeningById(Id);
    };
    
    @Override
    public Integer createHappening(Integer CelebrityId, String Start, String End,
                                   String Name, ArrayList<Quantum> QuantumsAL) throws SQLException, ParseException, RemoteException
    {
        return CurrentUser.createHappening(CelebrityId, DateFromString(Start), DateFromString(End), Name, QuantumsAL);        
    };
    
    @Override
    public ArrayList<Quantum> getFreeQuantum(String Start, String End) throws SQLException, ParseException, RemoteException
    {    
        ArrayList<Quantum> Res=new ArrayList<>();
        
        Iterator it=Registry.getFreeQuantum(DateFromString(Start), DateFromString(End), this.CurrentUser);   
        
        while(it.hasNext())
        {
            Res.add((Quantum)it.next());
        }
        
        return  Res;
    };
    
    @Override
    public ArrayList<Celebrity> getCelebrities() throws SQLException, CloneNotSupportedException, RemoteException
    {
        ArrayList<Celebrity> Res=new ArrayList<>();
        
        Iterator it=Registry.getCelebrities(this.CurrentUser);
        
        while(it.hasNext())
        {
            Res.add((Celebrity)it.next());
        }
        
        return  Res;
    };
    
    @Override
    public void deleteCelebrityById(Integer Id) throws SQLException, CloneNotSupportedException, RemoteException
    {
        Registry.deleteCelebrityById(Id, this.CurrentUser);
    };
    
    @Override
    public Integer createCelebrity(String Surname, String Name, String Patronym) throws SQLException, RemoteException
    {   return Registry.createCelebrity(Surname, Name, Patronym, this.CurrentUser);
    };
}
