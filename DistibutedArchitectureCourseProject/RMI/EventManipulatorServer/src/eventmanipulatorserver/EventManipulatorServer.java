package eventmanipulatorserver;

import ServiceLayer.APIImpl;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;

public class EventManipulatorServer {
    public static void main(String[] args) 
    {
        try
        {
            System.setSecurityManager(new RMISecurityManager());

            APIImpl eventapi = new APIImpl();

            Naming.rebind("//127.0.0.1/APIImpl", eventapi);

            System.out.println("Ready to serve");
        }
        catch (RemoteException | MalformedURLException e)
        {
            e.printStackTrace(System.err);
        }
    }
    
}
