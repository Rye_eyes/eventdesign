package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import junit.framework.TestCase;

public class APIImplTest extends TestCase 
{
    
    public APIImplTest(String testName) 
    {
        super(testName);
    }

    public void testGetCurrentUser() throws SQLException, RemoteException
    {
        System.out.println("SL. APIImpl. getCurrentUser.");
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        String expResult = "Ступин";
        Fellow result = instance.getCurrentUser();
        assertEquals(expResult, result.getSurname());
    }

    public void testLogin() throws Exception 
    {
        System.out.println("SL. APIImpl. Login.");
        String Name = "happy";
        String Password = "Goal";
        APIImpl instance = new APIImpl();
        boolean expResult = true;
        boolean result = instance.login(Name, Password);
        assertEquals(expResult, result);
      
    }

    public void testLogout() throws SQLException, RemoteException 
    {
        System.out.println("SL. APIImpl. logout.");
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        instance.logout();
        assertEquals(instance.getCurrentUser(), null);
    }

    public void testGetHappenings() throws Exception 
    {
        System.out.println("SL. APIImpl. getHappenings.");
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(16);
        expResultIds.add(17);
        expResultIds.add(18);
        expResultIds.add(19);
        expResultIds.add(20);
        
        ArrayList<Happening> resultl = new ArrayList<>();
        resultl=instance.getHappenings();
        
        
        boolean result=true;
        
        for (int i=0; i<expResultIds.size(); i++)
        {
            if (!Objects.equals(resultl.get(i).getId(), expResultIds.get(i)))
                result=false;
        }
        
        assertEquals(result, true);
    };

    public void testGetHappeningById() throws Exception 
    {
        System.out.println("SL. APIImpl. getHappeningById.");
        Integer Id = 18;
        APIImpl instance = new APIImpl();
        instance.login("456", "Snake");
        Happening expResult = null;
        Happening result = instance.getHappeningById(Id);
        assertEquals(expResult, result);
       
    };

    public void testDeleteHappeningById() throws Exception 
    {
        System.out.println("SL. APIImpl. deleteHappeningById.");
        
        Integer CelebrityId = 6;
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014, 10, 9, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014, 10, 10, 0, 0);
        
        java.sql.Date Start = new java.sql.Date(ins1.getTime().getTime());
        java.sql.Date End = new java.sql.Date(ins2.getTime().getTime());
        
        
        String Name = "Трамонтана!";
        
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        
        
        ArrayList<Quantum> qList=instance.getFreeQuantum( Happening.DateToString(Start), Happening.DateToString(End));
        
        Integer newH=instance.createHappening(CelebrityId, Happening.DateToString(Start), Happening.DateToString(End), Name, qList);
        
     
        instance.deleteHappeningById(newH);
        Happening res=instance.getHappeningById(newH);
        assertNull(res);
    };

    public void testCreateHappening() throws Exception 
    {
        System.out.println("SL. APIImpl. createHappening.");
        Integer CelebrityId = 6;
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014, 10, 9, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014, 10, 10, 0, 0);
        
        java.sql.Date Start = new java.sql.Date(ins1.getTime().getTime());
        java.sql.Date End = new java.sql.Date(ins2.getTime().getTime());
        
        String Name = "Трамонтана!";
        
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        ArrayList<Quantum> qList = instance.getFreeQuantum(Happening.DateToString(Start), Happening.DateToString(End));
        Integer newH=instance.createHappening(CelebrityId,  Happening.DateToString(Start), Happening.DateToString(End), Name, qList);
        
        assertNotNull(instance.getHappeningById(newH));
        instance.deleteHappeningById(newH);
        
    }

    public void testGetFreeQuantum() throws Exception 
    {
        System.out.println("SL. APIImpl. getFreeQuantum.");
       
        Calendar ins1=Calendar.getInstance();
        ins1.set(2014,7,7,21,0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(2014,7,8,2,0);
        
        java.sql.Date Start = new java.sql.Date(ins1.getTime().getTime());
        java.sql.Date End = new java.sql.Date(ins2.getTime().getTime());
        
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
         
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(13);
        expResultIds.add(14);
        
        ArrayList<Quantum> result = new ArrayList<>();
        
       result=instance.getFreeQuantum( Happening.DateToString(Start), Happening.DateToString(End));
        
        
        boolean real=true;
        
        for (int i=0; i<expResultIds.size(); i++)
        {
            if (!Objects.equals(result.get(i).getId(), expResultIds.get(i))) real=false;
        }
        assertEquals(real, true);
    }

    public void testGetCelebrities() throws Exception 
    {
        System.out.println("SL. APIImpl. getCelebrities.");
        
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        
        ArrayList<Integer> expResultIds = new ArrayList<>();
        
        expResultIds.add(6);
        expResultIds.add(7);
        expResultIds.add(8);
        expResultIds.add(9);
        expResultIds.add(10);
        
        boolean tmpres=true;
        
        ArrayList<Celebrity> result = new ArrayList<>();
        result= instance.getCelebrities();
        
        
        for (int i=0;i<5;i++)
        {
            if (!Objects.equals(expResultIds.get(i), result.get(i).getId()))
                tmpres=false;
        }
        
        assertEquals(tmpres, true);
    };

    public void testDeleteCelebrityById() throws Exception 
    {
        System.out.println("SL. APIImpl. deleteCelebrityById.");
        
        String Surname = "Ono";
        String Name = "L";
        String Patronym = "Yoko";
        
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        Integer Id=instance.createCelebrity(Surname, Name, Patronym);
        
        ArrayList<Celebrity> tmpList= new ArrayList<>();
        tmpList=instance.getCelebrities();
        
        
        boolean exist=false;
        for (Celebrity c:tmpList)
        {
            if(Objects.equals(c.getId(), Id)) 
                exist=true;
        }
        
        assertEquals(exist, true);
        
        instance.deleteCelebrityById(Id);
        
        
        tmpList.clear();
        
        tmpList=instance.getCelebrities();
        
        exist=false;
        for (Celebrity c:tmpList)
        {
            if(Objects.equals(c.getId(), Id)) 
                exist=true;
        }
        
        assertEquals(exist, false);
        
    };

    public void testCreateCelebrity() throws Exception 
    {
        System.out.println("SL. APIImpl. createCelebrity.");
        String Surname = "Ono";
        String Name = "L";
        String Patronym = "Yoko";
        APIImpl instance = new APIImpl();
        instance.login("happy", "Goal");
        Integer Id=instance.createCelebrity(Surname, Name, Patronym);
        
        ArrayList<Celebrity> tmpList=new ArrayList<>();
        tmpList=instance.getCelebrities();
        
        
        boolean exist=false;
        for (Celebrity c:tmpList)
        {
            if(Objects.equals(c.getId(), Id)) 
                exist=true;
        }
        
        assertEquals(exist, true);
        instance.deleteCelebrityById(Id);
    }
}
