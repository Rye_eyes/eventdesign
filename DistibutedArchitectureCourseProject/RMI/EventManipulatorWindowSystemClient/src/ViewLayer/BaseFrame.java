package ViewLayer;

import ServiceLayer.API;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JFrame;


public abstract class BaseFrame extends JFrame
{
    private static String Title="Дизайнер чудес. ";
    private static String IconPath="E:\\logoWonder.jpg";
    
    protected static BaseFrame startFrame=null;
    protected BaseFrame previousFrame;
    protected static API sessionAPI=null;
    
    public API makeAPI(String server, Integer port) throws NotBoundException, MalformedURLException, RemoteException
    {
      
        try
        {
            

            sessionAPI = (API)Naming.lookup("//" + server +":"+port.toString()+
                         "/APIImpl");
            
        }
        catch (NotBoundException | MalformedURLException | RemoteException e)
        {
            throw e;
        };
        
        return sessionAPI;
    };
    
    public BaseFrame(String TitleAdd, BaseFrame previousFrame)
    {
        super();
        setFrIcon();
        setFrTitle(TitleAdd);
        this.previousFrame=previousFrame;   
    };
    
    private void setFrIcon()
    {
        ImageIcon img = new ImageIcon(IconPath);
        this.setIconImage(img.getImage());
    };
    
    private void setFrTitle(String TitleAdd)
    {
        this.setTitle(Title+TitleAdd);
    };
    
    protected void goToFrame(BaseFrame nextFrame) throws SQLException
    {
        if (nextFrame==null) throw new NullPointerException();
        
        this.dispose();
        
        try
        {
            nextFrame.repaintTable();
        }
        catch (UnsupportedOperationException e)
        {
        }
        
        nextFrame.setVisible(true);
        
    }
    
    protected void goToStartFrame() throws SQLException
    {
        if (startFrame==null) throw new NullPointerException();
        
        this.dispose();
        try
        {
            startFrame.repaintTable();
        }
        catch (UnsupportedOperationException e)
        {
        }
        
        startFrame.setVisible(true);
    };
    
    protected void goToPreviousFrame() throws SQLException
    {
        if (this.previousFrame==null) throw new NullPointerException();
        
        this.dispose();
        try
        {
            previousFrame.repaintTable();
        }
        catch (UnsupportedOperationException e)
        {
        }
        this.previousFrame.setVisible(true);
    };
    
    public static String DateToString(Date T)
    {
        SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
        String S=Formatter.format(T);
        
        return S;
    };
    
    public static Date DateFromString(String S) throws ParseException
    {
         SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
         Date T=Formatter.parse(S);
         
         return T;
    };
    
    public abstract void repaintTable() throws SQLException;
            
}
