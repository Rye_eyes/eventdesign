package dumpserver;

import com.sun.org.apache.xerces.internal.util.URI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import javax.swing.JOptionPane;

public class DumpServer 
{

    public static void main(String[] args) throws IOException 
    {
   
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
        String potentialPort;
        
        String RMIServer="127.0.0.1";
        int RMIPortNumber=1999;
        
        
        try
        {
            
            System.out.println("Введите адрес сервера реестра RMI:");
            RMIServer=in.readLine();
            
            System.out.println("Введите номер порта реестра RMI:");
            potentialPort=in.readLine();
            RMIPortNumber = Integer.parseInt(potentialPort);
      
            //TODO: рефакторинг; унификация в static функцию по клиентам
            Protocol.makeAPI(RMIServer,RMIPortNumber);
            
            //TODO: рефакторинг, убрать из клиентов
            if (Protocol.sessionAPI.equals(null))
            {
                return;
            };
            
        }
        catch (NumberFormatException|NotBoundException|MalformedURLException|RemoteException e)
        {
            System.out.println(e.getLocalizedMessage());
            System.out.println("Соединение с RMI не установлено.");
            return;
        }
        
        
        
        
        int portNumber = 9999;
    
        System.out.println("Введите номер порта, прослушиваемого сетевым сервером:");
        potentialPort=in.readLine();
       
        try
        {
            portNumber = Integer.parseInt(potentialPort);
            System.out.println("Прослушиваетсяя порт: "+portNumber);
        }
        catch (NumberFormatException e)
        {
            System.out.println("Некорректный формат. Установлен порт по умолчанию: "+portNumber);
        }
        
        boolean listening = true;
         
        try (ServerSocket serverSocket = new ServerSocket(portNumber)) 
        {
            while (listening) 
            {
                new ThreadForClient(serverSocket.accept()).start();
            }
        }
        catch (IOException e) 
        {
            System.out.println("Прослушивание на заданном порту невозможно: " + portNumber);
        }
    }
    
}
