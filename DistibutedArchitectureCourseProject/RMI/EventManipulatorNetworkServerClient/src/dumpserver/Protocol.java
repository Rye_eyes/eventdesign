package dumpserver;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import ServiceLayer.API;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Protocol 
{
    private static boolean state=false;
    
    public static  API sessionAPI=null;
            
    //TODO: рефакторинг относительно WindowSystemCLient/ RMIServer: повтор. Вынос в общий интерфейс?
    public static API makeAPI(String server, Integer port) throws NotBoundException, MalformedURLException, RemoteException
    {
      
        try
        {
            

            sessionAPI = (API)Naming.lookup("//" + server +":"+port.toString()+
                         "/APIImpl");
            
        }
        catch (NotBoundException | MalformedURLException | RemoteException e)
        {
            throw e;
        }
        
        return sessionAPI;
    };
    
    
    
    public String Answer(String Ask) throws ParseException, SQLException, CloneNotSupportedException, RemoteException
    {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject)jsonParser.parse(Ask);
        
        String result="";
        
        switch((String)jsonObject.get("command"))
        {
            case "login":
            {
                String Name=((JSONObject)jsonObject.get("arg")).get("name").toString();
                String Password=((JSONObject)jsonObject.get("arg")).get("password").toString();
                
                this.state=sessionAPI.login(Name, Password);
                
                
                if((sessionAPI.getCurrentUser()!=null)&&
                   (sessionAPI.getCurrentUser().getType()!=Fellow.FellowType.администратор))
                {
                    this.state=false;
                    sessionAPI.logout();
                }
                
                result=makeLoginResult(this.state);
                break;
            }
            case "getClients":
            {
                
                ArrayList<Celebrity> Clients=new ArrayList<>();
                
                Clients=sessionAPI.getCelebrities();
                
                
                if (!this.state)
                    Clients.clear();
                
                result=makegetClientsResult(Clients);
                break;
            }
            case "logout":
            {
                
                this.state=false;
                result=makelogout();
                break;
            }
            
        }
        
        return result;
    };

    private String makeLoginResult(boolean switcher) 
    {
         JSONObject obj=new JSONObject();
         
         obj.put("result",switcher);
         
         return obj.toJSONString();
    }

    private String makegetClientsResult(ArrayList<Celebrity> Clients) 
    {
         JSONObject mainobj=new JSONObject();
         JSONArray list = new JSONArray();
         
         for (Celebrity t:Clients)
         {
            JSONObject obj=new JSONObject();
         
            obj.put("id", t.getId());
            obj.put("surname", t.getSurname());
            obj.put("name", t.getName());
            obj.put("patronym", t.getPatronym());
            
            list.add(obj);
         }
         
         mainobj.put("clients", list);
         
         return mainobj.toJSONString();
    }

    private String makelogout() 
    {
         JSONObject obj=new JSONObject();
         
         obj.put("result",true);
         
         return obj.toJSONString();
    }
    
}
