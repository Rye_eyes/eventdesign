package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

public interface API extends Remote
{ 
    public Fellow getCurrentUser() throws RemoteException;
  
    public Boolean login(String Name, String Password) throws SQLException, RemoteException;
    
    public void logout() throws RemoteException;
    
    public ArrayList<Happening> getHappenings() throws SQLException, RemoteException;
    
    public Happening getHappeningById(Integer Id) throws SQLException, RemoteException;
    
    public void deleteHappeningById(Integer Id) throws SQLException, RemoteException;
    
    public Integer createHappening(Integer CelebrityId, String Start, String End,
                                   String Name, ArrayList<Quantum> QuantumsAL) throws SQLException, ParseException, RemoteException;
    
    public ArrayList<Quantum> getFreeQuantum(String Start, String End) throws SQLException, ParseException, RemoteException;
    
    public ArrayList<Celebrity> getCelebrities() throws SQLException, CloneNotSupportedException, RemoteException;
    
    public void deleteCelebrityById(Integer Id) throws SQLException, CloneNotSupportedException, RemoteException;
    
    public Integer createCelebrity(String Surname, String Name, String Patronym) throws SQLException, RemoteException;
  
}
