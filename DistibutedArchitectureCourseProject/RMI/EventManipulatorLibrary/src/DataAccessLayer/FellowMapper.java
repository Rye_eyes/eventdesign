package DataAccessLayer;

import BussinessLayer.Fellow;
import BussinessLayer.Man;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FellowMapper 
{
    private Fellow fellowParser(ResultSet DbSet) throws SQLException
    {
        return new Fellow(DbSet.getInt("PK"), DbSet.getString("Surname"), 
                          DbSet.getString("Name"), DbSet.getString("Patronym"),
                          Fellow.FellowType.valueOf(DbSet.getString("Type")));
    };
    
    public Fellow findFellow(String Request) throws SQLException
    {
        Fellow Result=null;
        
        ResultSet ResponseSet=DbConnection.getInstance().getConnection().
                              createStatement().executeQuery(Request); 

        while (ResponseSet.next())
        {
            Result=fellowParser(ResponseSet);
        };
        
        return Result;
    };
    
    public Fellow findFellowByLoginPass(String Login, String Password) throws SQLException
    {
        
        String Request="SELECT * FROM MANSIT "
                + "WHERE Login='"+Login+"' AND PASSWORD='"+Password+"'";
        
        
        return findFellow(Request);
    };
    
    public Man findManById(Integer Id) throws SQLException
    {
        String Request="SELECT * FROM MANSIT "
                + "WHERE PK="+Id.toString();
        
        return (Man)findFellow(Request);
    };
    
    public Fellow findFellowByHappeningId(Integer Id) throws SQLException
    {
        String Request="SELECT * FROM MANSIT "
                + "INNER JOIN MHAT ON ManSIT.PK=MHAT.FK_M WHERE CLASS=1 AND"
                + " MHAT.FK_H="+Id.toString();
        
        return findFellow(Request);
    };
}
