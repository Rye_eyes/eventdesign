package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Iterator;
import java.util.List;

public class HappeningMapper 
{
    private Happening happeningParser(ResultSet DbSet) throws SQLException
    {
        return new Happening(DbSet.getInt("PK"),DbSet.getString("Name"), 
                             null, null, 
                             new Date(DbSet.getTimestamp("Start").getTime()), 
                             new Date(DbSet.getTimestamp("Finish").getTime()), 
                             null);
            
    };
    
    private Celebrity happeningHelperMHATOrdererFinder(Happening findCriteriaHappening) throws SQLException
    {
        return new CelebrityMapper().findCelebrityByHappeningId(findCriteriaHappening.getId());
    };
    
    private Fellow happeningHelperMHATWorkerFinder(Happening findCriteriaHappening) throws SQLException
    {
        return new FellowMapper().findFellowByHappeningId(findCriteriaHappening.getId());
    };
     
    private Iterator happeningHelperHQATFinder(Happening findCriteriaHappening) throws SQLException
    {
        return new QuantumMapper().findAllQuantumByHappeningId(findCriteriaHappening.getId());
    };
     
    private void happeningFiller(Happening baseHappening, 
                                 Celebrity Orderer, 
                                 Fellow Worker, 
                                 Iterator Quantums)
    {
        baseHappening.setOrderer(Orderer);
        baseHappening.setWorker(Worker);
        
        while(Quantums.hasNext())
        {
            Quantum q=(Quantum)Quantums.next();
            baseHappening.AddItem(q.getId(),q.getOwner(),
                                  q.GetHourFee(), q.getType());
        }
        
    };
    
    private PreparedStatement happeningMaker(DbConnection DbConn,
                                             String Request,
                                             Happening insertedHappening) throws SQLException
    {
        PreparedStatement insertStatement=DbConn.getConnection().
                                          prepareStatement(Request);
        
        insertStatement.setInt(1, insertedHappening.getId());
        insertStatement.setString(2, insertedHappening.getName());
        insertStatement.setTimestamp(3, new Timestamp(insertedHappening.getStart().getTime()));
        insertStatement.setTimestamp(4, new Timestamp(insertedHappening.getEnd().getTime()));
        
        return insertStatement;
    };
    
    private PreparedStatement happeningDeleteMaker(DbConnection DbConn,
                                                   String Request,
                                                   Happening deletedHappening) throws SQLException
    {
        PreparedStatement deleteStatement=DbConn.getConnection().
                                          prepareStatement(Request);
        
        deleteStatement.setInt(1, deletedHappening.getId());
        
        return deleteStatement;
    };
    
    private PreparedStatement happeningHelperMHATOrdererMaker(DbConnection DbConn,
                                                              String Request,
                                                              Happening insertedHappening) throws SQLException
    {
        PreparedStatement insertStatement=DbConn.getConnection().
                                          prepareStatement(Request);
        
        insertStatement.setInt(1, insertedHappening.getOrderer().getId());
        insertStatement.setInt(2, insertedHappening.getId());
        
        return insertStatement;
    };

       
    private PreparedStatement happeningHelperMHATWorkerMaker(DbConnection DbConn,
                                                             String Request,
                                                             Happening insertedHappening) throws SQLException
    {
        PreparedStatement insertStatement=DbConn.getConnection().
                                          prepareStatement(Request);
        
        insertStatement.setInt(1, insertedHappening.getWorker().getId());
        insertStatement.setInt(2, insertedHappening.getId());
        
        return insertStatement;
    };
        
        
    private Iterator happeningHelperHQATMaker(DbConnection DbConn,
                                              String Request,
                                              Happening insertedHappening) throws SQLException
    {
        List<PreparedStatement> insertStatementList= new ArrayList<>();

        Iterator itr=insertedHappening.getIterator();
        
        while (itr.hasNext())
        {
            Quantum q=(Quantum)itr.next();
            
            PreparedStatement insertStatement=DbConn.getConnection().prepareStatement(Request);
            
            insertStatement.setInt(1, q.getId());
            insertStatement.setInt(2, insertedHappening.getId());
        
            insertStatementList.add(insertStatement);
            
        }
        
        
        return insertStatementList.iterator();
    };
    
    private PreparedStatement happeningATDeleteMaker(DbConnection DbConn,
                                                     String Request,
                                                     Happening deletedHappening) throws SQLException
    {
        return happeningDeleteMaker(DbConn, Request, deletedHappening);
    };

       
    private Iterator findHappenings(String Request) throws SQLException
    {   
        List<Happening> Result=new ArrayList<>();
        
        ResultSet ResponseSet=DbConnection.getInstance().getConnection().createStatement().executeQuery(Request); 

        while (ResponseSet.next())
        {
            Happening tmp=happeningParser(ResponseSet);

            Fellow Worker=happeningHelperMHATWorkerFinder(tmp);
            Celebrity Orderer=happeningHelperMHATOrdererFinder(tmp);
            Iterator Quantums= happeningHelperHQATFinder(tmp);
            
            happeningFiller(tmp, Orderer, Worker, Quantums);
            
            Result.add(tmp);
        }
        
        return Result.iterator();
    };
    
    public Iterator findAllHappenings() throws SQLException
    {
        String Request="SELECT * FROM HAPPENING";
        
        return findHappenings(Request);
    };
    
    public synchronized Integer Insert(Happening insertedHappening) throws SQLException
    {
        String Request="INSERT INTO Happening (PK, Name, Start, Finish"
                        + ") VALUES (?,?,?,?)";

        insertedHappening.setId(DbConnection.getInstance().nextKey());

        happeningMaker(DbConnection.getInstance(), Request, insertedHappening).execute();
        
        
        Request="INSERT INTO MHAT (FK_M, FK_H) VALUES (?,?)";
        
        happeningHelperMHATOrdererMaker(DbConnection.getInstance(), Request, insertedHappening).execute();
        happeningHelperMHATWorkerMaker(DbConnection.getInstance(), Request, insertedHappening).execute();
       
        
        Request="INSERT INTO HQAT (FK_Q, FK_H) VALUES (?,?)";
        Iterator itr=happeningHelperHQATMaker(DbConnection.getInstance(), Request, insertedHappening);
        
        while (itr.hasNext())
        {
           ((PreparedStatement)itr.next()).execute();
        }
        
        DbConnection.getInstance().getConnection().commit();
        
        return insertedHappening.getId();
        
    };

    
    public synchronized void Delete(Happening deletedHappening) throws SQLException
    {
        
        String Request="DELETE FROM MHAT WHERE FK_H=?";
        
        happeningATDeleteMaker(DbConnection.getInstance(), Request, deletedHappening).execute();
        
        
        Request="DELETE FROM HQAT WHERE FK_H=?";
        
        happeningATDeleteMaker(DbConnection.getInstance(), Request, deletedHappening).execute();
        
        
        Request="DELETE FROM Happening WHERE PK=(?)";
        
        happeningDeleteMaker(DbConnection.getInstance(), Request, deletedHappening).execute();
        
        
        DbConnection.getInstance().getConnection().commit();
        
    };
    
    
}
