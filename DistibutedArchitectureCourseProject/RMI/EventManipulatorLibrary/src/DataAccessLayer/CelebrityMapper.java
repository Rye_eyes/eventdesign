package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Happening;
import BussinessLayer.Man;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class CelebrityMapper 
{
    private Celebrity celebrityParser(ResultSet DbSet) throws SQLException
    {
        return new Celebrity(DbSet.getInt("PK"), DbSet.getString("Surname"), 
                             DbSet.getString("Name"), DbSet.getString("Patronym"));
    };
    
    private PreparedStatement celebrityMaker(DbConnection DbConn,
                                             String Request,
                                             Celebrity insertedCelebrity) throws SQLException
    {
        PreparedStatement insertStatement=DbConn.getConnection().
                                          prepareStatement(Request);
        
        insertStatement.setInt(1, insertedCelebrity.getId());
        insertStatement.setString(2, insertedCelebrity.getSurname());
        insertStatement.setString(3, insertedCelebrity.getName());
        insertStatement.setString(4, insertedCelebrity.getPatronym());
        insertStatement.setInt(5, 2);
        insertStatement.setString(6, null);
        insertStatement.setString(7, null);
        insertStatement.setString(8, null);
        
        return insertStatement;
    };
    
    private PreparedStatement celebrityDeleteMaker(DbConnection DbConn,
                                                   String Request,
                                                   Celebrity deletedCelebrity) throws SQLException
    {
        PreparedStatement deleteStatement=DbConn.getConnection().
                                          prepareStatement(Request);
        
        deleteStatement.setInt(1, deletedCelebrity.getId());
        
        return deleteStatement;
    };
    
    private synchronized void celebrityHelperHappeningsDeleter(Celebrity deletedCelebrity) throws SQLException
    {
        HappeningMapper HM=new HappeningMapper();
        
        Iterator itr=HM.findAllHappenings();
        
        while(itr.hasNext())
        {
            Happening h=(Happening)itr.next();
           
            if(Objects.equals(h.getOrderer().getId(), deletedCelebrity.getId()))
                HM.Delete(h);
        }
    };
    
    private Celebrity findCelebrity(String Request) throws SQLException
    {
        Celebrity Result=null;
        
        ResultSet ResponseSet=DbConnection.getInstance().getConnection().
                              createStatement().executeQuery(Request); 

        while (ResponseSet.next())
        {
            Result=celebrityParser(ResponseSet);
        }
        
        return Result;
    };
    
    private Iterator findCelebrities(String Request) throws SQLException
    {
        List<Celebrity> Result=new ArrayList<>();
        
        ResultSet ResponseSet=DbConnection.getInstance().getConnection().
                              createStatement().executeQuery(Request); 

        while (ResponseSet.next())
        {
            Result.add(celebrityParser(ResponseSet));
        }
        
        return Result.iterator();
    };
    
    public Iterator findAllCelebrities() throws SQLException
    {
        String Request="SELECT * FROM MANSIT WHERE CLASS=2";
        
        return findCelebrities(Request);
    };
    
    public Man findManById(Integer Id) throws SQLException
    {
        String Request="SELECT * FROM MANSIT "
                + "WHERE PK="+Id.toString();
        
        return (Man)findCelebrity(Request);
    };
    
    public Celebrity findCelebrityByHappeningId(Integer Id) throws SQLException
    {
        String Request="SELECT * FROM MANSIT "
                + "INNER JOIN MHAT ON ManSIT.PK=MHAT.FK_M WHERE CLASS=2 AND"
                + " MHAT.FK_H="+Id.toString();
        
        return findCelebrity(Request);
    };
    
    public synchronized Integer Insert(Celebrity insertedCelebrity) throws SQLException
    {
        String Request="INSERT INTO ManSIT (PK, Surname, Name, Patronym, Class, Type,"
                + "Login, Password) VALUES (?,?,?,?,?,?,?,?)";

        insertedCelebrity.setId(DbConnection.getInstance().nextKey());

        celebrityMaker(DbConnection.getInstance(), Request, insertedCelebrity).execute();
        DbConnection.getInstance().getConnection().commit();
        
        return insertedCelebrity.getId();
    };
    
    public synchronized void Delete(Celebrity deletedCelebrity) throws SQLException
    {
        celebrityHelperHappeningsDeleter(deletedCelebrity);
        
        String Request="DELETE FROM ManSIT WHERE PK=(?)";
        
        celebrityDeleteMaker(DbConnection.getInstance(), Request, deletedCelebrity).execute();
        
        DbConnection.getInstance().getConnection().commit();
    };
    
}
