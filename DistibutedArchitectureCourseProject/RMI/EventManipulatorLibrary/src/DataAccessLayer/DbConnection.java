package DataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbConnection
{
    private static DbConnection DbConn;
    
    private final String Host = "jdbc:derby://localhost:1527/Happenings";
    private final String Name = "Inga";
    private final String Pass= "4069043";
    
    private final Connection Conn;
    
    private Integer nextId;
    
    
    private DbConnection() throws SQLException
    {
        this.Conn = DriverManager.getConnection(Host, Name, Pass);
        this.Conn.setAutoCommit(false);
    };
    
    public static DbConnection getInstance() throws SQLException 
    {  
        if ((DbConn==null)||(DbConn.getConnection().isClosed()))
        {
            DbConn=new DbConnection();
        }
        
        return DbConn;
    };

    public Connection getConnection()
    {
        return this.Conn;
    };
    
    public Integer nextKey() throws SQLException
    {
        String Request="SELECT PK FROM KEYS FOR UPDATE";
    
        ResultSet ResponseSet=this.Conn.createStatement().executeQuery(Request);
        
        while (ResponseSet.next())
        {
            this.nextId= ResponseSet.getInt("PK");
        }
        
        this.Conn.createStatement().executeUpdate("UPDATE KEYS SET PK="+(this.nextId+1));
        this.Conn.commit();
      
        return this.nextId;
    };
    
}
