package DataAccessLayer;

import BussinessLayer.Quantum;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class QuantumMapper 
{
    private Quantum quantumParser(ResultSet DbSet) throws SQLException
    {
        return new Quantum(DbSet.getInt("PK"),DbSet.getString("Owner"), 
                           DbSet.getInt("Fee"), Quantum.QuantumType.valueOf(DbSet.getString("Type")));
    };
    
    private Quantum findQuantum(String Request) throws SQLException
    {
        Quantum Result=null;
        
        ResultSet ResponseSet=DbConnection.getInstance().getConnection().
                              createStatement().executeQuery(Request); 

        while (ResponseSet.next())
        {
            Result=quantumParser(ResponseSet);
        }
        
        return Result;
    };
    
    private Iterator findQuantums(String Request) throws SQLException
    {
        List<Quantum> Result=new ArrayList<>();
        
        ResultSet ResponseSet=DbConnection.getInstance().getConnection().
                              createStatement().executeQuery(Request); 

        while (ResponseSet.next())
        {
            Result.add(quantumParser(ResponseSet));
        }
        
        return Result.iterator();
    };
    
    public Iterator findAllQuantum() throws SQLException
    {
        String Request="SELECT * FROM QUANTUM";
        
        return findQuantums(Request);
    };
    
    public Iterator findAllQuantumByHappeningId(Integer Id) throws SQLException
    {
        String Request="SELECT * FROM Quantum "
                     + "INNER JOIN HQAT ON Quantum.PK=HQAT.FK_Q WHERE "
                     + " HQAT.FK_H="+Id.toString();
        
        return findQuantums(Request);
    };
    
    public Quantum findQuantumById(Integer Id) throws SQLException
    {
        String Request="SELECT * FROM QUANTUM "
                + "WHERE PK="+Id.toString();
        
        return findQuantum(Request);
    };
    
}
