package BussinessLayer;

import java.io.Serializable;

public class DomainObject  implements Serializable
{
    private Integer Id;
    
    public DomainObject(Integer Id)
    {
        this.Id=Id;
    };
    
    public Integer getId()
    {
        return Id;
    };
    
    public void setId(Integer Id)
    {
        this.Id=Id;  
    };
}
