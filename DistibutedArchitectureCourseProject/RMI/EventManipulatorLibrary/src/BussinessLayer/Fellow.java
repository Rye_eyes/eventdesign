package BussinessLayer;

import DataAccessLayer.CelebrityMapper;
import DataAccessLayer.FellowMapper;
import DataAccessLayer.HappeningMapper;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.sql.Date;
import java.util.Iterator;
import java.util.Objects;

public class Fellow extends Man  implements Serializable
{
    public enum FellowType implements Serializable
    {
        администратор,
        менеджер
    };

    private FellowType Type;
    private Boolean isLogged;
    
    public Fellow(Integer Id, String Surname, String Name, String Patronym, 
            FellowType Type) 
    {
        super(Id, Surname, Name, Patronym);
        this.Type=Type;
        this.isLogged=false;
    };
    
    public FellowType getType()
    {
        return this.Type;
    };
    
    public void setType(FellowType Type)
    {
        this.Type=Type;
    };
    
    public Boolean getIsLogged()
    {
        return this.isLogged;
    };
    
    private void setIsLogged(Boolean isLogged)
    {
        this.isLogged=isLogged;
    };
    
    public boolean isAdmin()
    {
        return this.getType()==Fellow.FellowType.администратор;
    }
    
    public static Fellow Login(String Name, String Password) throws SQLException
    {
        FellowMapper FM=new FellowMapper();
        Fellow CurrentUser=FM.findFellowByLoginPass(Name, Password);
       
        if (CurrentUser!=null)
        {
            CurrentUser.setIsLogged(true);
        }
                     
        return CurrentUser;    
    };
            
    
    public Iterator getHappenings() throws SQLException
    {
        if (this.getIsLogged())
        {
            HappeningMapper HM=new HappeningMapper();
            
            Iterator itr=HM.findAllHappenings();
            ArrayList<Happening> tmpResults=new ArrayList<>();
            
            while(itr.hasNext())
            {
                tmpResults.add((Happening)itr.next());
            }
            
            if (tmpResults.isEmpty()) return Collections.<Happening>emptyList().iterator(); 
            
            ArrayList<Happening> Results=new ArrayList<>();
            
            for (Happening h: tmpResults)
            {
                if (this.getType()==Fellow.FellowType.менеджер)
                {
                    if (h.getWorker()!=null)
                    if (Objects.equals(h.getWorker().getId(), this.getId()))
                        Results.add(h);
                }
                else
                {
                   Results.add(h);
                }
            }
            
            if (Results.isEmpty()) 
                return Collections.<Happening>emptyList().iterator(); 
            else
                return Results.iterator();
        }
        else
            return Collections.<Happening>emptyList().iterator();    
    };  
    
    public Happening getHappeningById(Integer Id) throws SQLException
    {
        Iterator ResultTmpIt=this.getHappenings();
        
        if (ResultTmpIt==null)
        {
            return null;
        }
        else
            while(ResultTmpIt.hasNext())
            {
                Happening h=(Happening)ResultTmpIt.next();
                if (Objects.equals(h.getId(), Id))
                {
                    return h;
                }
            }
            
        return null;
    };
    
    public synchronized void deleteHappeningById(Integer Id) throws SQLException
    {
        Happening deletedHappening=this.getHappeningById(Id);
       
        if (deletedHappening==null)
        {
        }
        else
        {
            HappeningMapper HM=new HappeningMapper();
            HM.Delete(deletedHappening);
        }
    };
    
    public synchronized Integer createHappening(Integer CelebrityId, Date Start, Date End,
                                   String Name, ArrayList<Quantum> QuantumsAL) throws SQLException
    {
        if ((!this.getIsLogged())||(!Happening.isCorrectDates(Start, End)))
        {
            return null;
        }
        else
        {
            Iterator Quantums=QuantumsAL.iterator();
            ArrayList<Quantum> Items=new ArrayList<>();
            
            while(Quantums.hasNext())
            {
                Items.add((Quantum) Quantums.next());
            }
            
            Quantums=Items.iterator();
            
            for(Quantum q: Items)
            {
                if (!q.isFree(Start, End))
                    return null;
            }
            
            CelebrityMapper CM=new CelebrityMapper();
            Celebrity tmpCelebrity=(Celebrity)CM.findManById(CelebrityId);
            if (tmpCelebrity==null) return null;
            
            HappeningMapper HM=new HappeningMapper();
            Integer res=HM.Insert(new Happening(null, Name, this, tmpCelebrity, Start, End, Quantums));
            return res;
        }
    };
}
