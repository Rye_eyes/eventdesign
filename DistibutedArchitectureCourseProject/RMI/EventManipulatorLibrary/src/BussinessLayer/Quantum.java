package BussinessLayer;

import DataAccessLayer.HappeningMapper;
import DataAccessLayer.QuantumMapper;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.sql.Date;
import java.util.Iterator;
import java.util.Objects;

public class Quantum extends DomainObject  implements Serializable
{
    public enum QuantumType implements Serializable
    {
        микрофон,
        шарики,
        аудиосистема,
        видеосистема,
        проектор
    };
    
    private String Owner=null;
    private Integer HourFee=null;
    private QuantumType Type=null;

    public Quantum(Integer Id, String Owner, Integer HourFee, QuantumType Type) 
    {
        super(Id);
        this.Owner=Owner;
        
        if (isCorrectFee(HourFee))
            this.HourFee=HourFee;
        
        this.Type=Type;
    };
    
    public String getOwner()
    {
        return this.Owner;
    };
    
    public void setOwner(String Owner)
    {
        this.Owner=Owner;
    };
    
    public Integer GetHourFee()
    {
        return this.HourFee;
    };
    
    public void setHourFee(Integer HourFee)
    {
        if (isCorrectFee(HourFee))
        this.HourFee=HourFee;
    };
    
    public QuantumType getType()
    {
        return this.Type;
    };
    
    public void setType(QuantumType Type)
    {
        this.Type=Type;
    };
    
    public static boolean isCorrectFee(Integer potentialHourFee)
    {
        return (potentialHourFee>0);
    };
    
    public boolean isFree(Date Start, Date End) throws SQLException
    {
        
        boolean Result=true;
        
        HappeningMapper HM=new HappeningMapper();
        
        Iterator itr=HM.findAllHappenings();
        ArrayList<Happening> AllHappenigs=new ArrayList<>();
        
        while (itr.hasNext())
        {
            AllHappenigs.add((Happening)itr.next());
        }
        
        for (Happening H: AllHappenigs)
        {
            Iterator HIterator=H.getIterator();
            while(HIterator.hasNext())
            {
                Quantum q=(Quantum)HIterator.next();
                if (Objects.equals(this.getId(), q.getId()))
                {
                    Long As=Start.getTime();
                    Long Ae=End.getTime();
                    
                    Long Bs=H.getStart().getTime();
                    Long Be=H.getEnd().getTime();
                    
                    if (((Bs<=As)&&(Be<=Ae)&&(Be>As))||
                            ((Bs<=As)&&(Be>=Ae))||
                            ((As<=Bs)&&(Ae<=Be)&&(Ae>Bs))||
                            ((As<=Bs)&&(Ae>=Be)))
                    Result=false;
                }
                
            }
        }
        
        return Result;
        
    };
    
    public static Iterator getFreeQuantum(Date Start, Date End, Fellow CurrentUser) throws SQLException
    {       
        if (CurrentUser==null) return Collections.<Quantum>emptyList().iterator(); 
        
        QuantumMapper QM=new QuantumMapper();
            
        Iterator itr=QM.findAllQuantum();
        ArrayList<Quantum> tmpRes=new ArrayList<>();
        
        while(itr.hasNext())
        {
            tmpRes.add(((Quantum)itr.next()));
        }

        
        if (tmpRes.isEmpty())
        {
            return Collections.<Happening>emptyList().iterator(); 
        }
        
        ArrayList<Quantum> reall=new ArrayList<>();

        int itq=0;
        
        for (Quantum q: tmpRes)
        {
            itq++;
            if (q.isFree(Start, End))
            {
                reall.add(q);
            }
        }

        return  reall.iterator();          
    };
}
