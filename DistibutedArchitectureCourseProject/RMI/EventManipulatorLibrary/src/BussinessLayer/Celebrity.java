package BussinessLayer;

import DataAccessLayer.CelebrityMapper;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Iterator;

public class Celebrity extends Man implements Serializable
{
    public Celebrity(Integer Id, String Surname, String Name, String Patronym) 
    {
        super(Id, Surname, Name, Patronym);
    };
    
    public static Iterator getCelebrities(Fellow CurrentUser) throws SQLException
    {
        if ((CurrentUser==null)||(CurrentUser.getType()!=Fellow.FellowType.администратор))
        {
            return null;
        }
                
        CelebrityMapper CM=new CelebrityMapper();
        return CM.findAllCelebrities();
    };
    
    public synchronized static void deleteCelebrityById(Integer Id, Fellow CurrentUser) throws SQLException
    {
        if ((CurrentUser==null)||(CurrentUser.getType()!=Fellow.FellowType.администратор))
        {
            return;
        }
          
        CelebrityMapper CM=new CelebrityMapper();
        Celebrity deletedCelebrity=(Celebrity)CM.findManById(Id);
        CM.Delete(deletedCelebrity);
    };
    
    public synchronized static Integer createCelebrity(String Surname, String Name, String Patronym, Fellow CurrentUser) throws SQLException
    {
    
        if ((CurrentUser==null)||(CurrentUser.getType()!=Fellow.FellowType.администратор))
        {
            return null;
        }
        
        CelebrityMapper CM=new CelebrityMapper();
        Integer res=CM.Insert(new Celebrity(null, Surname, Name, Patronym));
        return res;
    }
}
