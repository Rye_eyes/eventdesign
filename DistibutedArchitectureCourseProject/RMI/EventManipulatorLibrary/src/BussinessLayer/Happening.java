package BussinessLayer;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Iterator;
import java.util.List;

public class Happening extends DomainObject  implements Serializable
{
    private String Name;
    private Fellow Worker;
    private Celebrity Orderer;
    private Date Start;
    private Date End;
    private ArrayList<Quantum> Items=new ArrayList<>();
    

    public Happening(Integer Id, String Name, Fellow Worker, Celebrity Orderer, Date Start, 
            Date End, Iterator Quantums) 
    {
        super(Id);
        
        this.Name=Name;
        
        if (isCorrectDates(Start, End))
        {
            this.Start=Start;
            this.End=End;
        }
        
        this.Worker=Worker;
        this.Orderer=Orderer;
        
        if(Quantums!=null)
        {
            while (Quantums.hasNext())
            {
                Quantum q=(Quantum)Quantums.next();
                this.AddItem(q.getId(), q.getOwner(), q.GetHourFee(), q.getType());
            }
            
        }
        
        
    };
    
    public String getName()
    {
        return this.Name;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
    
    public Fellow getWorker()
    {
        return this.Worker;
    };
    
    public void setWorker(Fellow Worker)
    {
        this.Worker=Worker;
    };
    
    public Celebrity getOrderer()
    {
        return this.Orderer;
    };
    
    public void setOrderer(Celebrity Orderer)
    {
        this.Orderer=Orderer;
    };
    
    public Date getStart()
    {
        return this.Start;
    };
    
    public void setStart(Date Start)
    {
        if ((this.End==null)||(isCorrectDates(Start, this.End)))
            this.Start=Start;
    };
    
    public Date getEnd()
    {
        return this.End;
    };
    
    public void setEnd(Date End)
    {
        if ((this.Start==null)||(isCorrectDates(this.Start, End)))
            this.End=End;        
    };
    
    public boolean AddItem(Integer Id, String Owner, Integer HourFee, Quantum.QuantumType Type)
    {
        Iterator IteratorQ= this.getIterator();
        
        while (IteratorQ.hasNext())
        {
            Quantum q=(Quantum)IteratorQ.next();
            if (q.getId().equals(Id))
            {
                return false;
            };
        };
        
        return this.Items.add(new Quantum(Id, Owner, HourFee, Type));
        
    };
    
    public Iterator getIterator()
    {
        return this.Items.iterator();
    };
    
    public static boolean isCorrectDates(Date potentialStart, Date potentialEnd)
    {
        return(potentialEnd.after(potentialStart));
    };
    
    public Long getHappeningCost()
    {
        Long S=0L;
        Iterator IteratorQ= this.getIterator();
        
        while (IteratorQ.hasNext())
        {
            Quantum q=(Quantum)IteratorQ.next();
            S+=q.GetHourFee()*(this.getEnd().getTime()-this.getStart().getTime())/(1000*60*60);
        }
        
        return S;
    };
    
     public static String DateToString(Date T)
    {
        SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
        String S=Formatter.format(T);
        
        return S;
    };
    
    public static Date DateFromString(String S) throws ParseException
    {
         SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
         java.util.Date T=Formatter.parse(S);
         
         return new java.sql.Date(T.getTime());
    };
    
}
