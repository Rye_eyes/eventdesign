package BussinessLayer;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Iterator;

public class DBCommonOperationsRegistry  implements Serializable
{

    public Iterator getFreeQuantum(Date Start, Date End, Fellow CurrentUser) throws SQLException
    {
        return Quantum.getFreeQuantum(Start, End, CurrentUser);
    };

    public Iterator getCelebrities(Fellow CurrentUser) throws SQLException 
    {
        return Celebrity.getCelebrities(CurrentUser);
    };

    public void deleteCelebrityById(Integer Id, Fellow CurrentUser) throws SQLException
    {
        Celebrity.deleteCelebrityById(Id, CurrentUser);
    };

    public Integer createCelebrity(String Surname, String Name, String Patronym, Fellow CurrentUser) throws SQLException
    {
        return Celebrity.createCelebrity(Surname, Name, Patronym, CurrentUser);
    };

    public Fellow Login(String Name, String Password) throws SQLException
    {
        return Fellow.Login(Name, Password);
    };
    
    
}
