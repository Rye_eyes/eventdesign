package DataAccessLayer;

import BussinessLayer.Fellow;
import BussinessLayer.Fellow.FellowType;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

public class FellowMapperTest extends TestCase {
    
    public FellowMapperTest(String testName) {
        super(testName);
    }

    public void testFindFellowById() throws Exception 
    {
        System.out.println("DAL. FellowMapper. findManById.");
        Integer Id = 4;
        FellowMapper FM=new FellowMapper();
        FellowType expResult = FellowType.менеджер;
        Fellow result = (Fellow) FM.findManById(4);
        assertEquals(expResult, result.getType());
    };
    
    public void testFindFellowByLoginPass() throws Exception
    {
        System.out.println("DAL. FellowMapper. findFellowByLoginPass.");
        FellowMapper FM=new FellowMapper();
        String expResult = "Ступин";
        Fellow result = (Fellow) FM.findFellowByLoginPass("happy", "Goal");
        assertEquals(expResult, result.getSurname());
    }

}
