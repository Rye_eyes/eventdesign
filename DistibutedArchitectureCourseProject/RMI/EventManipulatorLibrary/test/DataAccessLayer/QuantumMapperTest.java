package DataAccessLayer;

import BussinessLayer.Quantum;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import junit.framework.TestCase;


public class QuantumMapperTest extends TestCase 
{
    
    public QuantumMapperTest(String testName) 
    {
        super(testName);
    }

    public void testFindAllQuantum() throws Exception {
        System.out.println("DAL. QuantumMapper. findAllQuantum.");
        QuantumMapper instance = new QuantumMapper();
        ArrayList<Integer> expResult = new ArrayList<>();
        expResult.add(11);
        expResult.add(12);
        expResult.add(13);
        expResult.add(14);
        expResult.add(15);
        
        Iterator result = instance.findAllQuantum();
        
        boolean rqesult=true;

        int i=0;
        while(result.hasNext())
        {
            if (i==5) break;
            if (!Objects.equals(((Quantum)result.next()).getId(), expResult.get(i))) rqesult=false;
            i++;
        }
        assertEquals(rqesult, true);
    };

    public void testFindQuantumById() throws Exception 
    {
        System.out.println("DAL. QuantumMapper. findQuantumById.");
        Integer Id = 15;
        QuantumMapper instance = new QuantumMapper();
        Quantum.QuantumType expResult = Quantum.QuantumType.шарики;
        Quantum result = instance.findQuantumById(Id);
        assertEquals(expResult, result.getType());
    };

}
