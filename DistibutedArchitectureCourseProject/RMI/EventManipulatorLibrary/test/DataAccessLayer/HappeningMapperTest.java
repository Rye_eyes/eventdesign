package DataAccessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import static BussinessLayer.Quantum.QuantumType.аудиосистема;
import static BussinessLayer.Quantum.QuantumType.микрофон;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import junit.framework.TestCase;

public class HappeningMapperTest extends TestCase {
    
    public HappeningMapperTest(String testName) 
    {
        super(testName);
    };
    
    public void testFindAllHappenings() throws Exception {
        System.out.println("DAL. HappeningMapper. findAllHappenings.");
        HappeningMapper instance = new HappeningMapper();
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(16);
        expResultIds.add(17);
        expResultIds.add(18);
        expResultIds.add(19);
        expResultIds.add(20);
        
        Iterator result = instance.findAllHappenings();
        
        boolean ist=true;
        int i=0;
        
        while (result.hasNext())
        {
            if (i==5) break;
            if (!Objects.equals(expResultIds.get(i), ((Happening)result.next()).getId()))ist=false;
            i++;
        }
        
        
        assertEquals(ist, true);
    };

    
    public void testInsert() throws Exception 
    {
        System.out.println("DAL. HappeningMapper. Insert.");
       
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
       
        
        QuantumList1.add(new Quantum(11, "СуперМегаАудио", 50, аудиосистема) 
        );
        QuantumList1.add(new Quantum(12, "СуперМегаАудио", 75, микрофон) 
        );
        
        Happening Subject = new Happening(null,"h",new Fellow(3, "A", "B", "C", Fellow.FellowType.менеджер),
                               new Celebrity(2, "D", "E", "F"),
                               new java.sql.Date((new java.util.Date(2014,9,11,00,00)).getTime()), new java.sql.Date((new java.util.Date(2014,9,12,00,00)).getTime()), QuantumList1.iterator());
        HappeningMapper instance = new HappeningMapper();
        Integer result=instance.Insert(Subject);
        
        Iterator resultl = instance.findAllHappenings();
        ArrayList<Integer> resultlIds=new ArrayList<>();
        
        while (resultl.hasNext())
        {
            resultlIds.add(((Happening)resultl.next()).getId());
        }
        
        assertEquals(resultlIds.contains(result), true);
        
        instance.Delete(Subject);
    };

    
    public void testDelete() throws Exception 
    {
        System.out.println("DAL. HappeningMapper. Delete.");
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(11, "СуперМегаАудио", 50, аудиосистема) 
        );
        QuantumList1.add(new Quantum(12, "СуперМегаАудио", 75, микрофон) 
        );
        
        Happening Subject = new Happening(null,"h",new Fellow(3, "A", "B", "C", Fellow.FellowType.менеджер),
                               new Celebrity(2, "D", "E", "F"),
                              new java.sql.Date((new java.util.Date(2014,9,11,00,00)).getTime()), new java.sql.Date((new java.util.Date(2014,9,12,00,00)).getTime()), QuantumList1.iterator());
        HappeningMapper instance = new HappeningMapper();
        Integer result=instance.Insert(Subject);
        
        instance.Delete(Subject);
        
        Iterator resultl = instance.findAllHappenings();
        ArrayList<Integer> resultlIds=new ArrayList<>();
        
        while (resultl.hasNext())
        {
            resultlIds.add(((Happening)resultl.next()).getId());
        }
        
        assertEquals(!resultlIds.contains(result), true); 
    }

}
