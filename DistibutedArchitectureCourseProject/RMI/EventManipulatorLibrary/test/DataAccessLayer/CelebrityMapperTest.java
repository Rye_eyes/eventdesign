package DataAccessLayer;

import BussinessLayer.Celebrity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

public class CelebrityMapperTest extends TestCase 
{
    
    public CelebrityMapperTest(String testName) 
    {
        super(testName);
    }

    public void testFindCelebrityById() throws Exception 
    {
        System.out.println("DAL. CelebrityMapper. findManById.");
        Integer Id = 6;
        CelebrityMapper CM=new CelebrityMapper();
        String Surname = "Смородина";
        Celebrity result = (Celebrity) CM.findManById(Id);
        assertEquals(Surname, result.getSurname());
    };
    
    public void testInsert() throws Exception 
    {
        System.out.println("DAL. CelebrityMapper. Insert/Delete.");
        CelebrityMapper CM=new CelebrityMapper();
        Celebrity newCelebrity=new Celebrity(null, "Ицыксон", "Владимир", "Михайлович");
        
        Integer Id=CM.Insert(newCelebrity);
         
        Celebrity result=(Celebrity)CM.findManById(Id);
        assertEquals("Ицыксон", result.getSurname());
        
        CM.Delete(newCelebrity);
        
        result=(Celebrity)CM.findManById(Id);
        assertEquals(null, result);
        
    };
    

    public void testFindAllCelebrities() throws Exception 
    {
        System.out.println("DAL. CelebrityMapper. findAllCelebrities.");
        CelebrityMapper instance = new CelebrityMapper();
        ArrayList<Integer> expResultIds = new ArrayList<>();
        expResultIds.add(6);
        expResultIds.add(7);
        expResultIds.add(8);
        expResultIds.add(9);
        expResultIds.add(10);
        
        Iterator itr = instance.findAllCelebrities();
        
        boolean real=true;
        
        int i=0;
        while (itr.hasNext())
        {
            if (i==5) break;
            if(!Objects.equals(expResultIds.get(i), ((Celebrity)itr.next()).getId())) real=false;  
            i++;
        };
        
        assertEquals(real, true);
        
    }
}
