package BussinessLayer;

import junit.framework.TestCase;

public class FellowTest extends TestCase 
{
    
    public FellowTest(String testName) 
    {
        super(testName);
    };

    public void testGetType() 
    {
        System.out.println("BL. Fellow. getType.");
        Fellow instance = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        Fellow.FellowType expResult = Fellow.FellowType.менеджер;
        Fellow.FellowType result = instance.getType();
        assertEquals(expResult, result);
    };

    public void testSetType() 
    {
        System.out.println("BL. Fellow. setType.");
        Fellow.FellowType Type = Fellow.FellowType.администратор;
        Fellow instance = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        instance.setType(Type);
        assertEquals(Type, instance.getType());
    };
    
}
