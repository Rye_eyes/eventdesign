package BussinessLayer;

import static BussinessLayer.Quantum.QuantumType.аудиосистема;
import static BussinessLayer.Quantum.QuantumType.видеосистема;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.util.Iterator;
import junit.framework.TestCase;

public class HappeningTest extends TestCase 
{
    
    public HappeningTest(String testName) 
    {
        super(testName);
    };
    
    public void testGetName()
    {
        System.out.println("BL. Happening. getName.");
        Fellow F = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        
        Happening instance;
        
        Iterator tempIterator=QuantumList1.iterator();
        
        instance = new Happening(1,"h",F,
                new Celebrity(1, "D", "E", "F"),
                new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        
        String expResult="h";
        String result = instance.getName();
        assertEquals(expResult, result);
    };
    
    public void testSetName()
    {
        System.out.println("BL. Happening. setName.");
        Fellow F = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        
        
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",F,
                          new Celebrity(1, "D", "E", "F"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        
        String expResult="hh";
        
        instance.setName(expResult);
        
        String result = instance.getName();
        assertEquals(expResult, result);
        
    };
    
    public void testSetName(String Name)
    {
        System.out.println("BL. Happening. setName.");
    };

    public void testGetWorker() 
    {
        System.out.println("BL. Happening. getWorker");
        Fellow expResult = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",expResult,
                          new Celebrity(1, "D", "E", "F"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        
        Fellow result = instance.getWorker();
        assertEquals(expResult, result);
    };

    public void testSetWorker() 
    {
        System.out.println("BL. Happening. setWorker.");
        Fellow expResult = new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер);
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );

        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(3, "J", "K", "L", Fellow.FellowType.менеджер),
                          new Celebrity(1, "D", "E", "F"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        
        instance.setWorker(expResult);
        
        assertEquals(expResult, instance.getWorker());
    };

    public void testGetOrderer() 
    {
        System.out.println("BL. Happening. getOrderer.");
        Celebrity expResult = new Celebrity(1, "D", "E", "F");

        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          expResult,
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        
        Celebrity result = instance.getOrderer();
        assertEquals(expResult, result);
    };

    public void testSetOrderer() 
    {
        System.out.println("BL. Happening. setOrderer.");
        Celebrity expResult = new Celebrity(1, "D", "E", "F");

        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        instance.setOrderer(expResult);
        assertEquals(expResult, instance.getOrderer());
    };

    public void testGetStart() 
    {
        System.out.println("BL. Happening. getStart.");
        
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        
        Date result = instance.getStart();
        assertEquals(ins1.getTime(), result);
    };

    public void testSetStart() 
    {
        System.out.println("BL. Happening. setStart.");
        Date Start = new java.sql.Date((new java.util.Date(2014,9,11,12,00)).getTime());
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
      
        instance.setStart(Start);
        assertEquals(Start, instance.getStart());
      
    };

    public void testGetEnd() 
    {
        System.out.println("BL. Happening. getEnd.");
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        Date expResult = new Date(2014,9,12);
        Date result = instance.getEnd();
        assertEquals(ins2.getTime(), result);
    };

    public void testSetEnd() 
    {
        System.out.println("BL. Happening. setEnd.");
        Date End =new java.sql.Date((new java.util.Date(2014,9,11,25,00)).getTime());
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        instance.setEnd(End);
        
        assertEquals(End, instance.getEnd());
    }

    public void testGetItems() 
    {
        System.out.println("BL. Happening. getItems.");
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        ArrayList<Quantum> expResult = QuantumList1;
        
        ArrayList<Quantum> result = new ArrayList<>();
        
        Iterator instanceIterator=instance.getIterator();
        
        while(instanceIterator.hasNext())
        {
            result.add((Quantum)instanceIterator.next());
        }
        
        for (int i=0; i<expResult.size(); i++)
            assertEquals(expResult.get(i).getId(), result.get(i).getId());
    };

    public void testIsCorrectDates() 
    {
        System.out.println("BL. Happening. isCorrectDates.");
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Date potentialStart = new java.sql.Date(ins2.getTime().getTime());
        Date potentialEnd = new java.sql.Date(ins1.getTime().getTime());
        boolean expResult = false;
        boolean result = Happening.isCorrectDates(potentialStart, potentialEnd);
        assertEquals(expResult, result);
    };

    public void testGetHappeningCost() 
    {
        System.out.println("BL. Happening. getHappeningCost.");
        ArrayList<Quantum> QuantumList1= new ArrayList<>();
        QuantumList1.add(new Quantum(1, "A", 10, аудиосистема) 
        );
        QuantumList1.add(new Quantum(2, "B", 20, видеосистема) 
        );
        
        Iterator tempIterator=QuantumList1.iterator();
        
        Calendar ins1=Calendar.getInstance();
        ins1.set(3914, 9, 11, 0, 0);
        
        Calendar ins2=Calendar.getInstance();
        ins2.set(3914, 9, 12, 0, 0);
        
        Happening instance = new Happening(1,"h",new Fellow(1, "A", "B", "C", Fellow.FellowType.менеджер),
                          new Celebrity(1, "J", "K", "L"),
                          new java.sql.Date(ins1.getTime().getTime()), new java.sql.Date(ins2.getTime().getTime()), tempIterator);
        Long expResult = 720L;
        Long result = instance.getHappeningCost();
        assertEquals(expResult, result);
    };
    
}
