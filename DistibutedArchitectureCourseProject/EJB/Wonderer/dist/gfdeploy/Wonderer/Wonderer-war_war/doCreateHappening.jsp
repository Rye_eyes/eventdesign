<%@page contentType="text/html" pageEncoding="windows-1251"%>
<%@ include file="Helper.jsp" %>
</html>
<%
    ArrayList<Quantum> qs=new ArrayList();
    
    if(request.getParameterValues("Quantum")!=null)
    {
        String [] QS=request.getParameterValues("Quantum");
        for (String Q:QS)
        {
            if (!Q.isEmpty())
                qs.add(ejbRef1.getQuantumById(Integer.parseInt(Q)));
        }
    }
    ejbRef1.createHappening(Integer.parseInt(request.getParameter("ClientId")), request.getParameter("Begin"), request.getParameter("End"), request.getParameter("Name"), qs);
    response.sendRedirect("mainHappenings.jsp");
%>