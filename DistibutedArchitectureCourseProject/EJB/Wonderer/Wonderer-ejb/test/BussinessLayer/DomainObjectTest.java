/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLayer;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Inga
 */
public class DomainObjectTest {
    
    public DomainObjectTest() {
    }

    /**
     * Test of getId method, of class DomainObject.
     */
    @Test
    public void testGetSetId() 
    {
        System.out.println("BL. DomainObject. getId&setId");
        DomainObject instance = new DomainObject();
        Integer Id = 1;
        instance.setId(Id);
        assertEquals(Id, instance.getId());
    }

   
    
}
