/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLayer;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inga
 */
public class CelebrityTest {
    
    public CelebrityTest() {
    }

    /**
     * Test of toString method, of class Celebrity.
     */
    @Test
    public void testToString() {
        System.out.println("BL. Celebrity. toString");
        Celebrity instance = new Celebrity();
        instance.setId(5);
        String expResult = "bussinesslayer.Celebrity[ id=5 ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
