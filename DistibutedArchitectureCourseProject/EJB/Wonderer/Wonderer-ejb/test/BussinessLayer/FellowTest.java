/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLayer;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inga
 */
public class FellowTest {
    
    public FellowTest() {
    }

    /**
     * Test of getType method, of class Fellow.
     */
    @Test
    public void testGetSetType() {
        System.out.println("BL. Fellow. getType&setType");
        Fellow instance = new Fellow();
        instance.setType(Fellow.FellowType.менеджер);
        Fellow.FellowType expResult = Fellow.FellowType.менеджер;
        Fellow.FellowType result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of isAdmin method, of class Fellow.
     */
    @Test
    public void testIsAdmin() {
        System.out.println("BL. Fellow. isAdmin");
        Fellow instance = new Fellow();
        instance.setType(Fellow.FellowType.менеджер);
        boolean expResult = false;
        boolean result = instance.isAdmin();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Fellow.
     */
    @Test
    public void testToString() {
        System.out.println("BL. Fellow. toString");
        Fellow instance = new Fellow();
        instance.setId(5);
        String expResult = "bussinesslayer.Fellow[ id=5 ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
