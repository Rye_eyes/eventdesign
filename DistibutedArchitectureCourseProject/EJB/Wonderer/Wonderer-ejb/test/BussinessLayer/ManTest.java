/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLayer;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inga
 */
public class ManTest {
    
    public ManTest() {
    }

    /**
     * Test of getSurname method, of class Man.
     */
    @Test
    public void testGetSetSurname() {
        System.out.println("Bl. Man. getSurname&setSurname");
        Man instance = new Man();
        instance.setSurname("B");
        String expResult = "B";
        String result = instance.getSurname();
        assertEquals(expResult, result);
    }

    

    /**
     * Test of getPatronym method, of class Man.
     */
    @Test
    public void testGetSetPatronym() {
        System.out.println("Bl. Man. getPatronym&setPatronym");
        Man instance = new Man();
        instance.setPatronym("B");
        String expResult = "B";
        String result = instance.getPatronym();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Man.
     */
    @Test
    public void testGetSetName() {
        System.out.println("Bl. Man. getName&setName");
        Man instance = new Man();
        instance.setName("B");
        String expResult = "B";
        String result = instance.getName();
        assertEquals(expResult, result);
    }


    /**
     * Test of getClassId method, of class Man.
     */
    @Test
    public void testGetSetClassId() 
    {
        System.out.println("Bl. Man. getClassId&setClassId");
        Man instance = new Man();
        instance.setClassId(1);
        Integer expResult = 1;
        Integer result = instance.getClassId();
        assertEquals(expResult, result);

    }

}
