/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLayer;

import java.sql.Timestamp;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inga
 */
public class QuantumTest {
    
    public QuantumTest() {
    }

    /**
     * Test of getOwner method, of class Quantum.
     */
    @Test
    public void testGetSetOwner() {
        System.out.println("BL. Quantum. getOwner&setOwner");
        Quantum instance = new Quantum();
        String expResult = "Castle";
        instance.setOwner("Castle");
        String result = instance.getOwner();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHourFee method, of class Quantum.
     */
    @Test
    public void testGetSetHourFee() {
        System.out.println("BL. Quantum. getHourFee&setHourFee");
        Quantum instance = new Quantum();
        instance.setHourFee(56);
        Integer expResult = 56;
        Integer result = instance.getHourFee();
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class Quantum.
     */
    @Test
    public void testGetSetType() {
        System.out.println("BL. Quantum. getType&setType");
        Quantum instance = new Quantum();
        instance.setType(Quantum.QuantumType.шарики);
        Quantum.QuantumType expResult = Quantum.QuantumType.шарики;
        Quantum.QuantumType result = instance.getType();
        assertEquals(expResult, result);
    }


    /**
     * Test of isCorrectFee method, of class Quantum.
     */
    @Test
    public void testIsCorrectFee() {
        System.out.println("BL. Quantum. isCorrectFee");
        Integer potentialHourFee = -56;
        boolean expResult = false;
        boolean result = Quantum.isCorrectFee(potentialHourFee);
        assertEquals(expResult, result);
     }

    /**
     * Test of isFree method, of class Quantum.
     */
    @Test
    public void testIsFree() {
        System.out.println("BL. Quantum. isFree");
        
        Quantum instance=new Quantum();
        
        Timestamp Start = new Timestamp(2014, 7, 7, 21, 0, 0, 0);
        Timestamp End = new Timestamp(2014, 7, 8, 2, 0, 0, 0);
        
        ArrayList<Happening> hs = new ArrayList<>();
        
        Happening h1=new Happening();
        h1.setStart(Start);
        h1.setEnd(End);
        ArrayList<Quantum> items1=new ArrayList();
        items1.add(instance);
        h1.setItems(items1);
        
        hs.add(h1);
        
        boolean expResult = false;
        boolean result = instance.isFree(Start, End, hs);
        assertEquals(expResult, result);
        
    }
    
}
