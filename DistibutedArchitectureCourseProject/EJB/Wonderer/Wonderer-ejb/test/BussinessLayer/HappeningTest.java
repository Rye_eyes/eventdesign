/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BussinessLayer;

import java.sql.Timestamp;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Inga
 */
public class HappeningTest {
    
    public HappeningTest() {
    }

    /**
     * Test of getName method, of class Happening.
     */
    @Test
    public void testGetSetName() {
        System.out.println("BL. Happening. getName&setName");
        Happening instance = new Happening();
        String expResult = "Party";
        instance.setName("Party");
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of getWorker method, of class Happening.
     */
    @Test
    public void testGetSetWorker() {
        System.out.println("BL. Happening. getWorker&setWorker");
        Happening instance = new Happening();
        
        Fellow Worker1=new Fellow();
        Worker1.setId(1);
    
        Fellow Worker2=new Fellow();
        Worker2.setId(1);
    
        instance.setWorker(Worker2);
        Fellow expResult = Worker1;
        Fellow result = instance.getWorker();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getOrderer method, of class Happening.
     */
    @Test
    public void testGetSetOrderer() {
        System.out.println("BL. Happening. getOrderer&setOrderer");
        Happening instance = new Happening();
        
        Celebrity Orderer1=new Celebrity();
        Orderer1.setId(1);
        
        Celebrity Orderer2=new Celebrity();
        Orderer2.setId(1);
        
        instance.setOrderer(Orderer2);
        Celebrity expResult = Orderer1;
        
        Celebrity result = instance.getOrderer();
        assertEquals(expResult, result);
        
    }


    /**
     * Test of getStart method, of class Happening.
     */
    @Test
    public void testGetSetStart() {
        System.out.println("BL. Happening. getStart&setStart");
        Happening instance = new Happening();
        Timestamp expResult = new Timestamp(2014, 7, 7, 21, 0, 0, 0);
        instance.setStart(expResult);
        Timestamp result = instance.getStart();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getEnd method, of class Happening.
     */
    @Test
    public void testGetSetEnd() {
        System.out.println("BL. Happening. getEnd&setEnd");
        Happening instance = new Happening();
        Timestamp expResult = new Timestamp(2014, 7, 7, 21, 0, 0, 0);
        instance.setEnd(expResult);
        Timestamp result = instance.getEnd();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of getItems method, of class Happening.
     */
    @Test
    public void testGetSetItems() {
        System.out.println("BL. Happening. getItems&setItems");
        Happening instance = new Happening();
        ArrayList<Quantum> expResult = new ArrayList<>();
        Quantum instanceq = new Quantum();
        Quantum instancea = new Quantum();
        Quantum instancec = new Quantum();
        
        expResult.add(instanceq);
        expResult.add(instancea);
        expResult.add(instancec);
        
        instance.setItems(expResult);
        
        ArrayList<Quantum> result = instance.getItems();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of isCorrectDates method, of class Happening.
     */
    @Test
    public void testIsCorrectDates() {
        System.out.println("BL. Happening. isCorrectDates");
        Timestamp potentialStart = new Timestamp(2015, 7, 7, 21, 0, 0, 0);
        Timestamp potentialEnd = new Timestamp(2015, 7, 7, 21, 0, 0, 0);
        boolean expResult = false;
        boolean result = Happening.isCorrectDates(potentialStart, potentialEnd);
        assertEquals(expResult, result);
    }

    /**
     * Test of getHappeningCost method, of class Happening.
     */
    @Test
    public void testGetHappeningCost() {
        System.out.println("BL. Happening. getHappeningCost");
        Happening instance = new Happening();
        Timestamp Start = new Timestamp(2014, 7, 7, 21, 0, 0, 0);
        Timestamp End = new Timestamp(2014, 7, 8, 2, 0, 0, 0);
        
        instance.setStart(Start);
        instance.setEnd(End);
        
        ArrayList<Quantum> expResulta = new ArrayList<>();
        Quantum instanceq = new Quantum();
        instanceq.setHourFee(10);
        Quantum instancea = new Quantum();
        instancea.setHourFee(10);
        Quantum instancec = new Quantum();
        instancec.setHourFee(10);
        
        expResulta.add(instanceq);
        expResulta.add(instancea);
        expResulta.add(instancec);
        
        instance.setItems(expResulta);
        
        
        Long expResult = (long) 150;
        Long result = instance.getHappeningCost();
        assertEquals(expResult, result);
    }

    /**
     * Test of DateToString method, of class Happening.
     */
    @Test
    public void testDateToString() {
        System.out.println("BL. Happening. DateToString");
        Timestamp T = new Timestamp(2014, 7, 7, 21, 0, 0, 0);
        String expResult = "07.08.14 21:00";
        String result = Happening.DateToString(T);
        assertEquals(expResult, result);
    }

    /**
     * Test of DateFromString method, of class Happening.
     */
    @Test
    public void testDateFromString() throws Exception {
        System.out.println("BL. Happening. DateFromString");
        String S = "07.08.14 21:00";
        Timestamp expResult = new Timestamp(114, 7, 7, 21, 0, 0, 0);
        Timestamp result = Happening.DateFromString(S);
        assertEquals(expResult, result);
    }
    
}
