package BussinessLayer;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-28T12:06:25")
@StaticMetamodel(Man.class)
public class Man_ extends DomainObject_ {

    public static volatile SingularAttribute<Man, String> Patronym;
    public static volatile SingularAttribute<Man, Integer> ClassId;
    public static volatile SingularAttribute<Man, String> Surname;
    public static volatile SingularAttribute<Man, String> Name;

}