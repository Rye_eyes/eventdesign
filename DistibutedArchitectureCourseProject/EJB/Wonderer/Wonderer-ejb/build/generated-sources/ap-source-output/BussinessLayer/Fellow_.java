package BussinessLayer;

import BussinessLayer.Fellow.FellowType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-28T12:06:25")
@StaticMetamodel(Fellow.class)
public class Fellow_ extends Man_ {

    public static volatile SingularAttribute<Fellow, FellowType> Type;
    public static volatile SingularAttribute<Fellow, String> Login;
    public static volatile SingularAttribute<Fellow, String> Password;

}