package BussinessLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Quantum;
import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-28T12:06:25")
@StaticMetamodel(Happening.class)
public class Happening_ extends DomainObject_ {

    public static volatile SingularAttribute<Happening, Timestamp> Start;
    public static volatile SingularAttribute<Happening, Fellow> Worker;
    public static volatile SingularAttribute<Happening, Timestamp> End;
    public static volatile ListAttribute<Happening, Quantum> Items;
    public static volatile SingularAttribute<Happening, Celebrity> Orderer;
    public static volatile SingularAttribute<Happening, String> Name;

}