package BussinessLayer;

import BussinessLayer.Quantum.QuantumType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-28T12:06:25")
@StaticMetamodel(Quantum.class)
public class Quantum_ extends DomainObject_ {

    public static volatile SingularAttribute<Quantum, String> Owner;
    public static volatile SingularAttribute<Quantum, Integer> HourFee;
    public static volatile SingularAttribute<Quantum, QuantumType> Type;

}