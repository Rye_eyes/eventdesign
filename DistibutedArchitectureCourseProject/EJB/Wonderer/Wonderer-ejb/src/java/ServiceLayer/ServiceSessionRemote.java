package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 *
 * @author Inga
 */

@Remote
public interface ServiceSessionRemote 
{
    public Boolean login(String Name, String Password);
    public void logout();
    public ArrayList<Happening> getHappenings();
    public ArrayList<Celebrity> getCelebrities();
    public Happening getHappeningById(Integer Id);
    public void deleteHappeningById(Integer Id);
    public void deleteCelebrityById(Integer Id);
    public void createCelebrity(String Surname, String Name, String Patronym);
    public void createHappening(Integer CelebrityId, String Start, String End, String Name, ArrayList<Quantum> QuantumsAL);
    public ArrayList<Quantum> getFreeQuantum(String Start, String End);
    public Quantum getQuantumById(Integer Id);
    public Celebrity getCelebrityById(Integer Id);
    public String DateToString(Timestamp T);
    public Timestamp DateFromString(String T);
    public boolean isCorrectDates(Timestamp potentialStart, Timestamp potentialEnd);
    public boolean isAdmin();
}