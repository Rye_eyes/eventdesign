package ServiceLayer;

import BussinessLayer.Celebrity;
import BussinessLayer.Fellow;
import BussinessLayer.Happening;
import BussinessLayer.Quantum;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Inga
 */

@Stateful 
public class ServiceSession implements ServiceSessionLocal, ServiceSessionRemote 
{
    
    private static int bean_number=0;
    
    @PersistenceContext(unitName = "Wonderer-ejbPU")
    private EntityManager em;
    
    private Fellow User=null;
    
    /*Служебная функция-логгер*/
    private void Logger(String Log)
    {
        try (PrintWriter out = new PrintWriter(new FileWriter("C:\\Wonderer.txt",true)))
        { 
                out.println("["+System.currentTimeMillis()+"] "+Log);
                out.flush();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(ServiceSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    };
    

    @PostConstruct
    /*Логируем создание stateful-bean для клиента*/
    public void afterConstruct() 
    {
        Logger("Stateful ServiceSession Bean ["+bean_number+"] is created.");
        bean_number++;
    };
    
    @PreDestroy
    /*Логируем уничтожение stateful-bean для клиента*/
    public void beforeDestroy() 
    {
        Logger("Stateful ServiceSession Bean ["+bean_number+"] is destroyed.");
    };
    
    
    
    @Override
    public Boolean login(String Name, String Password)
    {
        Query query=em.createNamedQuery("Fellow.findFellowByLoginPass");
        query.setParameter("Login", Name);
        query.setParameter("Password", Password);
        
        List Fellows=query.getResultList();
        
        if (Fellows.size()>0)
        {
            User=(Fellow)Fellows.get(0);
            return true;
        }
        else
            return false;
    };
    
    @Override
    @Remove
    public void logout()
    {
        User=null;
    };
    
    @Override
    public ArrayList<Happening> getHappenings()
    {
        Query query;
        
        if (User==null) return null;
        else
        {
            if (User.isAdmin())
            {
                query=em.createNamedQuery("Happening.findAllHappenings");
            }
            else
            {
                query=em.createNamedQuery("Happening.findFellowHappenings");
                query.setParameter("Id", User.getId());
            }
            
            List Happenings=query.getResultList();
            ArrayList Result=new ArrayList();
            
            for (Object hObject:Happenings)
            {
                Result.add((Happening)hObject);
            }
            
            return Result;
        }
        
    };
    
    @Override
    public ArrayList<Celebrity> getCelebrities()
    {
        Query query;
        
        if (User==null) return null;
        else
        {
            if (User.isAdmin())
            {
                query=em.createNamedQuery("Celebrity.findAllCelebrities");
            }
            else
            {
                return null;
            }
            
            List Celebrities=query.getResultList();
            ArrayList Result=new ArrayList();
            
            for (Object cObject:Celebrities)
            {
                Result.add((Celebrity)cObject);
            }
            
            return Result;
        }
        
    };
    
    @Override
    public Happening getHappeningById(Integer Id)
    {
        Query query;
        
        if (User==null)
        {
            return null;
        }
        else
        {
           
            query=em.createNamedQuery("Happening.findHappeningById");
            query.setParameter("hId", Id);
           
            List Happenings=query.getResultList();
            
            if (Happenings.isEmpty())
            {
                return null;
            }
            else
            {
                if((User.isAdmin())||(Objects.equals(((Happening)Happenings.get(0)).getWorker().getId(), User.getId())))
                    return (Happening)Happenings.get(0);
                else
                    return null;
            }
        }
        
    };
    
    @Override
    public void deleteHappeningById(Integer Id)
    {
        
        if (User!=null)
        {
            Happening h=em.find(Happening.class, Id);
       
            if (h!=null)
            {
                if((User.isAdmin())||(Objects.equals(h.getWorker().getId(), User.getId())))
                    em.remove(h);
           
            }
        }
       
    };
    
    @Override
    public void deleteCelebrityById(Integer Id)
    {
        Query query;
        
        if ((User!=null)&&(User.isAdmin()))
        {
            query=em.createNamedQuery("Happening.findCelebrityHappenings");
            query.setParameter("oId", Id);
           
            List<Happening> happenings=query.getResultList();
        
            for (Happening h: happenings)
            {
                em.remove(h);
            }
        
            
            Celebrity c=em.find(Celebrity.class, Id);
            
            if(c!=null)
                em.remove(c);
        }
        
        
    };
    
    @Override
    public void createCelebrity(String Surname, String Name, String Patronym)
    {
        if ((User!=null)&&(User.isAdmin()))
        {
            Celebrity c=new Celebrity();
            c.setSurname(Surname);
            c.setName(Name);
            c.setPatronym(Patronym);

            em.persist(c);
        }
    };
    
    @Override
    public void createHappening(Integer CelebrityId, String Start, String End,
                                   String Name, ArrayList<Quantum> QuantumsAL)
    {
        if ((User!=null)&&(User.isAdmin()))
        {
            try 
            {
                Happening h=new Happening();
                h.setOrderer(em.find(Celebrity.class, CelebrityId));
                h.setWorker(User);
                h.setName(Name);
                h.setStart(Happening.DateFromString(Start));
                h.setEnd(Happening.DateFromString(End));
                h.setItems(QuantumsAL);

                em.persist(h);
            } 
            catch (ParseException ex) 
            {
                Logger.getLogger(ServiceSession.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };
    
    @Override
    public ArrayList<Quantum> getFreeQuantum(String Start, String End)
    {
        if (User==null) return null;
        
        ArrayList<Quantum> Result=new ArrayList<>();
        Query query;
        
        query=em.createNamedQuery("Quantum.getAllQuantums");
        List<Quantum> qs=query.getResultList();
        
        query=em.createNamedQuery("Happening.findAllHappenings");
        List<Happening> hs=query.getResultList();
        
        ArrayList<Happening> hsal=new ArrayList<>();
        for (Happening hss:hs)
        {
            hsal.add(hss);
        }
        
        for (Quantum q: qs)
        {
            try 
            {
                if(q.isFree(Happening.DateFromString(Start), Happening.DateFromString(End), hsal))
                    Result.add(q);
            } catch (ParseException ex) 
            {
                Logger.getLogger(ServiceSession.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        
        return Result;
    };
    
    @Override
    public Quantum getQuantumById(Integer Id)
    {
        if (User!=null)
            return em.find(Quantum.class, Id);
        else
            return null;
    }
    
    @Override
    public Celebrity getCelebrityById(Integer Id)
    {
         if (User!=null)
            return em.find(Celebrity.class, Id);
        else
            return null;
    }
    
    @Override
    public String DateToString(Timestamp T)
    {
        return Happening.DateToString(T);
    }
    
    @Override
    public Timestamp DateFromString(String T)
    {
        try 
        {
            return Happening.DateFromString(T);
        } 
        catch (ParseException ex) 
        {
            Logger.getLogger(ServiceSession.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Override
    public boolean isCorrectDates(Timestamp potentialStart, Timestamp potentialEnd)
    {
        return Happening.isCorrectDates(potentialStart, potentialEnd);
    }
    
    @Override
    public boolean isAdmin()
    {
        return User.isAdmin();
    }
    
    public void persist(Object object) 
    {
        em.persist(object);
    }
}
