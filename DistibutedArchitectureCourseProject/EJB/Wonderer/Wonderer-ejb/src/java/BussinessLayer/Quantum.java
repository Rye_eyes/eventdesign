package BussinessLayer;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="QUANTUM")
@NamedQueries({
    @NamedQuery(name="Quantum.getAllQuantums", query="SELECT q FROM Quantum q")
})

public class Quantum extends DomainObject  implements Serializable
{
    public enum QuantumType implements Serializable
    {
        микрофон,
        шарики,
        аудиосистема,
        видеосистема,
        проектор
    };
    
    private String Owner;
    
    @Column(name = "Fee")
    private Integer HourFee;
    
    private QuantumType Type;
    
    public Quantum() 
    {
    };
 
    
    public String getOwner()
    {
        return this.Owner;
    };
    
    public void setOwner(String Owner)
    {
        this.Owner=Owner;
    };
    
    
    public Integer getHourFee()
    {
        return this.HourFee;
    };
    
    
    public void setHourFee(Integer HourFee)
    {
        if (isCorrectFee(HourFee))
        this.HourFee=HourFee;
    };
    
    public QuantumType getType()
    {
        return this.Type;
    };
    
    public void setType(QuantumType Type)
    {
        this.Type=Type;
    };
    
    public static boolean isCorrectFee(Integer potentialHourFee)
    {
        return (potentialHourFee>0);
    };
    
    public boolean isFree(Timestamp Start, Timestamp End, ArrayList<Happening> hs)
    {
        
        boolean Result=true;
        
        Iterator itr=hs.iterator();
        ArrayList<Happening> AllHappenigs=new ArrayList<>();
        
        while (itr.hasNext())
        {
            AllHappenigs.add((Happening)itr.next());
        }
        
        for (Happening H: AllHappenigs)
        {
            for (Quantum q : H.getItems()) {
                if (Objects.equals(this.getId(), q.getId()))
                {
                    Long As=Start.getTime();
                    Long Ae=End.getTime();
                    
                    Long Bs=H.getStart().getTime();
                    Long Be=H.getEnd().getTime();
                    
                    if (((Bs<=As)&&(Be<=Ae)&&(Be>As))||
                            ((Bs<=As)&&(Be>=Ae))||
                            ((As<=Bs)&&(Ae<=Be)&&(Ae>Bs))||
                            ((As<=Bs)&&(Ae>=Be)))
                        Result=false;
                }
            }
        }
        
        return Result;
        
    };
    
}
