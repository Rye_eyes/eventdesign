package BussinessLayer;


import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@DiscriminatorValue(value="0")

@NamedQueries({
    @NamedQuery(name="Fellow.findFellowByLoginPass", query="SELECT f FROM Fellow f"
            + " WHERE f.Login=:Login AND f.Password=:Password")
})

public class Fellow extends Man  implements Serializable
{
    public enum FellowType implements Serializable
    {
        администратор,
        менеджер
    };

    private FellowType Type;
    
    private String Login;
    
    private String Password;
    
    public Fellow()
    {  
    };
    
    public FellowType getType()
    {
        return this.Type;
    };
    
    public void setType(FellowType Type)
    {
        this.Type=Type;
    };
    
    public boolean isAdmin()
    {
        return this.getType()==Fellow.FellowType.администратор;
    };
    
    @Override
    public String toString() 
    {
        return "bussinesslayer.Fellow[ id=" + this.getId() + " ]";
    }
    
}
