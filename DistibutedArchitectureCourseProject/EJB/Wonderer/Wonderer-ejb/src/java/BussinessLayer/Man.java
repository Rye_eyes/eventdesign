package BussinessLayer;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="MANSIT")
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name= "CLASS")

public class Man extends DomainObject  implements Serializable
{
    private String Surname;
    
    private String Name;
    
    private String Patronym;
    
    @Column(name= "CLASS")
    private Integer ClassId;
    
    public Man()
    {
        
    };
   
    public String getSurname()
    {
        return this.Surname;
    };
    
    public void setSurname(String Surname)
    {
        this.Surname=Surname;
    };
    
    public String getPatronym()
    {
        return this.Patronym;
    };
    
    public void setPatronym(String Patronym)
    {
        this.Patronym=Patronym;
    };    
    
    public String getName()
    {
        return this.Name;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
    
    public Integer getClassId()
    {
        return this.ClassId;
    };
    
    public void setClassId(Integer ClassId)
    {
        this.ClassId=ClassId;
    };
}
