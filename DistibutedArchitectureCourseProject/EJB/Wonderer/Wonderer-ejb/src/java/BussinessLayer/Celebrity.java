package BussinessLayer;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@DiscriminatorValue(value="1")

@NamedQueries({
    @NamedQuery(name="Celebrity.findAllCelebrities", query="SELECT c FROM Celebrity c")
})

public class Celebrity extends Man implements Serializable
{
    public Celebrity() 
    {
    };   
    
    @Override
    public String toString() 
    {
        return "bussinesslayer.Celebrity[ id=" + this.getId() + " ]";
    }
}
