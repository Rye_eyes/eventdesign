package BussinessLayer;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.Iterator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "HAPPENING")
@NamedQueries({
    @NamedQuery(name="Happening.findAllHappenings", query="SELECT h FROM Happening h"),
    @NamedQuery(name="Happening.findFellowHappenings", query="SELECT h FROM Happening h JOIN h.Worker w"
            + " WHERE w.Id=:Id"),
    @NamedQuery(name="Happening.findHappeningById", query="SELECT h FROM Happening h "
            + " WHERE h.Id=:hId"),
    @NamedQuery(name="Happening.findCelebrityHappenings", query="SELECT h FROM Happening h JOIN h.Orderer o"
            + " WHERE o.Id=:oId")
})

public class Happening extends DomainObject  implements Serializable
{
    private String Name;
    
    @ManyToOne
    @JoinTable(name="MHAT", 
            joinColumns=@JoinColumn(name="FK_H", referencedColumnName="PK"),
            inverseJoinColumns=@JoinColumn(name="FK_M", referencedColumnName="PK"))
    private Fellow Worker;
    
    @ManyToOne
    @JoinTable(name="MHAT", 
            joinColumns=@JoinColumn(name="FK_H", referencedColumnName="PK"),
            inverseJoinColumns=@JoinColumn(name="FK_M", referencedColumnName="PK"))
    private Celebrity Orderer;
    
    private Timestamp Start;
    
    @Column(name = "FINISH")
    private Timestamp End;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="HQAT", 
            joinColumns=@JoinColumn(name="FK_H", referencedColumnName="PK"),
            inverseJoinColumns=@JoinColumn(name="FK_Q", referencedColumnName="PK"))
    private ArrayList<Quantum> Items=new ArrayList<>();
    

    public Happening() 
    {
    };
    
    public String getName()
    {
        return this.Name;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
    
    public Fellow getWorker()
    {
        return this.Worker;
    };
    
    public void setWorker(Fellow Worker)
    {
        this.Worker=Worker;
    };
    
    public Celebrity getOrderer()
    {
        return this.Orderer;
    };
    
    public void setOrderer(Celebrity Orderer)
    {
        this.Orderer=Orderer;
    };
    
    public Timestamp getStart()
    {
        return this.Start;
    };
    
    public void setStart(Timestamp Start)
    {
        if ((this.End==null)||(isCorrectDates(Start, this.End)))
            this.Start=Start;
    };
    
    public Timestamp getEnd()
    {
        return this.End;
    };
    
    public void setEnd(Timestamp End)
    {
        if ((this.Start==null)||(isCorrectDates(this.Start, End)))
            this.End=End;        
    };
    
    public ArrayList<Quantum> getItems()
    {
        return this.Items;
    };
    
    public void setItems(ArrayList<Quantum> Items)
    {
        this.Items.clear();
        this.Items.addAll(Items);
    };
    
    public static boolean isCorrectDates(Timestamp potentialStart, Timestamp potentialEnd)
    {
        return(potentialEnd.after(potentialStart));
    };
    
    public Long getHappeningCost()
    {
        Long S=0L;
        Iterator IteratorQ= this.getItems().iterator();
        
        while (IteratorQ.hasNext())
        {
            Quantum q=(Quantum)IteratorQ.next();
            S+=q.getHourFee()*(this.getEnd().getTime()-this.getStart().getTime())/(1000*60*60);
        }
        
        return S;
    };
    
    public static String DateToString(Timestamp T)
    {
        SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
        String S=Formatter.format(T);
        
        return S;
    };
    
    public static Timestamp DateFromString(String S) throws ParseException
    {
         SimpleDateFormat Formatter=new SimpleDateFormat("dd.MM.yy HH:mm");
         java.util.Date T=Formatter.parse(S);
         
         return new java.sql.Timestamp(T.getTime());
    };
    
}
