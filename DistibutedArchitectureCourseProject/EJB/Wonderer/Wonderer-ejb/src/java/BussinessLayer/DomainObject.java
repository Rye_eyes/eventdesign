package BussinessLayer;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.TABLE;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;


@MappedSuperclass
public class DomainObject  implements Serializable
{
    @Id
    @GeneratedValue(strategy=TABLE, generator="KeyGen")
    @TableGenerator(
            name="KeyGen", 
            table="Keys", 
            pkColumnName = "PK_name",
            valueColumnName = "PK",
            allocationSize=1)
    @Basic(optional = false)
    @NotNull
    @Column(name="PK")
    private Integer Id;
    
    public DomainObject()
    {
        
    };
    
    public Integer getId()
    {
        return Id;
    };
    
    public void setId(Integer Id)
    {
        this.Id=Id;  
    };
    
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (Id != null ? Id.hashCode() : 0);
        return hash;
    };
    
    @Override
    public boolean equals(Object object) 
    {
        if (!(object instanceof DomainObject)) {
            return false;
        }
      
        DomainObject other = (DomainObject) object;
        return !((this.Id == null && other.Id != null) || (this.Id != null && !this.Id.equals(other.Id)));
    }
}
